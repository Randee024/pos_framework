<?php

	$router->map('GET', '/create_account', function() {
		$tablename="create_account";
		include linkPage("templateload");
	});


	
$router->map('POST', '/createAccount', function() {

	$accountModel = new AccountModel;

	$result = $accountModel->createAccount($_POST);
	
	$return = array();

	if (!empty($result)) {

		$return['status'] = true;

		$return['id'] = $result;

	} else {
		 $return['status'] = false;
	}
	
	echo json_encode($return);

});


    $router->map('POST', '/accountFileUpload', function() {

        $fileModel = new FileuploadModel;
        $target_dir = "upload/" . $_POST['new_path'] . "/";
		$temp = array();

        if (!file_exists($target_dir)) {

            mkdir($target_dir);

        }

        if (isset($_FILES['file']) && !empty($_FILES['file'])) {

            $temp = $_FILES['file']['tmp_name'];

        }


        foreach ($_FILES['file']['name'] as $key => $name) {

            $target_file = $target_dir . basename($name);

            $return = false;

            if (move_uploaded_file($temp[$key], $target_dir . time() . $key . $name)) {

                $fileName = time() . $key . $name;
                $return = $fileModel->accountFileUpload($_POST, $fileName, $target_dir, $name);

            }

        }

        echo json_encode($return);

    });
	

	
