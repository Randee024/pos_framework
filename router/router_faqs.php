<?php

$router->map('GET', '/faqs', function() {
    
	$pageModel = new PageModel;
	$fileModel = new FileuploadModel;
	$faqModel = new FaqModel;
	
	$pageDetails = $pageModel->getFaqPageDetails();
	$faqAdmin = $faqModel->getAllFaqAdmin();
	$faqCustomer = $faqModel->getAllFaqCustomer();
	$faqContract = $faqModel->getAllFaqContract();

	
	$tablename="faqs";
    include linkPage("templateload");
});

