<?php



$router->map('GET', '/login', function() {

    if (isset($_SESSION['authenticated']) && $_SESSION['authenticated']) {

        unset($_SESSION);



        session_destroy();

    }



    $tablename = 'login';

    include linkPage('templateload');

});



$router->map('POST', '/check_username', function() {



    if (isset($_POST)) {



        if (isset($_POST['userID'])) {



            $id = $_POST['userID'];



            $query[] = 'Select * from account Where username =? AND id <> ? ';

            $params[] = array($_POST['username'], $id);

        } else {



            $query[] = 'Select * from account Where username =? ';

            $params[] = array($_POST['username']);

        }





        if (transactStatement($query, $params)) {



            $return['checker'] = true;

        } else {



            $return['checker'] = false;

        }

    } else {



        $return['checker'] = true;

    }



    echo json_encode($return);



    exit();

});

$router->map('GET', '/registration', function() {



    $tablename = 'register';

    include linkPage('templateload');

});





$router->map('POST', '/authenticate', function() {



    if (isset($_POST)) {



        $accountModel = new AccountModel;

        $return['status'] = $accountModel->authenticate($_POST);

    } else {



        $return['status'] = false;

    }

    echo json_encode($return);



    exit();

});



if (isset($_SESSION['authenticated']) && $_SESSION['authenticated']) {



    $router->map('GET', '/backend', function() {

        $tablename = "backend";

        include linkPage("templateload_backend");

    });

    $router->map('POST', '/UpdateFixPage', function() {

        $pageModel = new PageModel();
        $result = $pageModel->UpdateFixPage($_POST);

        $return = array();

        if (!empty($result)) {

            $return['status'] = true;

            $return['id'] = $result;

        } else {
			 $return['status'] = false;
        }

        // Dev::pvx($return);

        echo json_encode($return);

    });
	
	$router->map('POST', '/addPlan', function() {

        $planModel = new PlanModel();
        $result = $planModel->addPlan($_POST);

        echo json_encode($result);

    });
	
	$router->map('POST', '/addMap', function() {

        $mapModel = new MapModel();
        $result = $mapModel->addMap($_POST);

        echo json_encode($result);

    });
	
	$router->map('POST', '/addFaq', function() {

        $faqModel = new FaqModel();
        $result = $faqModel->addFaq($_POST);

        
        $return = array();

        if (!empty($result)) {

            $return['status'] = true;

            $return['id'] = $result;

        } else {
			 $return['status'] = false;
        }

        echo json_encode($return);

    });
	
	$router->map('POST', '/addServices', function() {

        $servicesModel = new ServicesModel();
        $result = $servicesModel->addServices($_POST);

        
        $return = array();

        if (!empty($result)) {

            $return['status'] = true;

            $return['id'] = $result;

        } else {
			 $return['status'] = false;
        }

        echo json_encode($return);

    });
	
	$router->map('POST', '/addTestimony', function() {

        $testimonyModel = new TestimonyModel();
        $result = $testimonyModel->addTestimony($_POST);

        $return = array();

        if (!empty($result)) {

            $return['status'] = true;

            $return['id'] = $result;

        } else {
			 $return['status'] = false;
        }

        echo json_encode($return);

    });
	
	$router->map('POST', '/deletePlan', function() {

        $planModel = new PlanModel();
        $result = $planModel->deletePlan($_POST['id_plan']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/deleteLogo', function() {

        $logoModel = new PartnerLogoModel();
        $result = $logoModel->deleteLogo($_POST['id_logo']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/deleteMap', function() {

        $mapModel = new MapModel();
        $result = $mapModel->deleteMap($_POST['id_map']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/deleteFaq', function() {

        $faqModel = new FaqModel();
        $result = $faqModel->deleteFaq($_POST['id_faq']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/deleteService', function() {

        $servicesModel = new ServicesModel();
        $result = $servicesModel->deleteService($_POST['id_service']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/deleteFrontEndDesign', function() {

        $frontEndModel = new FrontEndModel();
        $result = $frontEndModel->deleteFrontEndDesign($_POST['id_design']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/deleteTestimony', function() {

        $testimonyModel = new TestimonyModel();
        $result = $testimonyModel->deleteTestimony($_POST['id_testimony']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/activationPlan', function() {

        $planModel = new PlanModel();
        $result = $planModel->activationPlan($_POST['id_plan']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/activationService', function() {

        $servicesModel = new ServicesModel();
        $result = $servicesModel->activationService($_POST['id_service']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/activationLogo', function() {

        $logoModel = new PartnerLogoModel();
        $result = $logoModel->activationLogo($_POST['id_logo']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/activationMap', function() {

        $mapModel = new MapModel();
        $result = $mapModel->activationMap($_POST['id_map']);

        echo json_encode($result);

    });
	
	$router->map('POST', '/getPlanDetails', function() {

        $planModel = new PlanModel();
        $result = $planModel->getPlanDetails($_POST['id']);

        echo json_encode($result);

    });

	$router->map('POST', '/getMapDetails', function() {

        $mapModel = new MapModel();
        $result = $mapModel->getMapDetails($_POST['id']);

        echo json_encode($result);

    });

	$router->map('POST', '/getFaqDetails', function() {

        $faqModel = new FaqModel();
        $result = $faqModel->getFaqDetails($_POST['id']);

        echo json_encode($result);

    });

	$router->map('POST', '/getServiceDetails', function() {

        $servicesModel = new ServicesModel();
        $result = $servicesModel->getServiceDetails($_POST['id']);

        echo json_encode($result);

    });

	$router->map('POST', '/UpdateContactUsContent', function() {

        $pageModel = new PageModel();
        $result = $pageModel->UpdateContactUsContent($_POST);

        echo json_encode($result);

    });

	$router->map('POST', '/getTestimonyDetails', function() {

        $testimonyModel = new TestimonyModel();
        $result = $testimonyModel->getTestimonyDetails($_POST['id']);

        echo json_encode($result);

    });

	$router->map('POST', '/addLogoTitle', function() {

        $pageModel = new PageModel();
        $result = $pageModel->addLogoTitle($_POST);

        echo json_encode($result);

    });

	$router->map('GET', '/getAllTestimony', function() {

        $testimonyModel = new TestimonyModel();
        $result = $testimonyModel->getAllTestimony();

        echo $result;

    });
	
	$router->map('GET', '/getAllAboutUsFileUploaded', function() {

        $logoModel = new PartnerLogoModel();
        $result = $logoModel->getAllAboutUsFileUploaded();
        echo $result;

    });

	$router->map('GET', '/getAllSampleFrontEnd', function() {

        $frontEndModel = new FrontEndModel();
        $result = $frontEndModel->getAllSampleFrontEnd();

        echo $result;

    });

	$router->map('GET', '/getAllServices', function() {

        $servicesModel = new ServicesModel();
        $result = $servicesModel->getAllServices();

        echo $result;

    });

	
	$router->map('GET', '/getAllFaqs', function() {

        $faqModel = new FaqModel();
        $result = $faqModel->getAllFaqs();

        echo $result;

    });

	
	$router->map('GET', '/getAllMap', function() {

        $mapModel = new MapModel();
        $result = $mapModel->getAllMap();

        echo $result;

    });

	
    $router->map('POST', '/fileUpload', function() {

        $fileModel = new FileuploadModel;
        $target_dir = "upload/" . $_POST['new_path'] . "/";
		$temp = array();

        if (!file_exists($target_dir)) {

            mkdir($target_dir);

        }

        if (isset($_FILES['file']) && !empty($_FILES['file'])) {

            $temp = $_FILES['file']['tmp_name'];

        }


        foreach ($_FILES['file']['name'] as $key => $name) {

            $target_file = $target_dir . basename($name);

            $return = false;

            if (move_uploaded_file($temp[$key], $target_dir . time() . $key . $name)) {

                $fileName = time() . $key . $name;
                $return = $fileModel->uploadPageFiles($_POST, $fileName, $target_dir, $name);

            }

        }

        echo json_encode($return);

    });
	
    $router->map('POST', '/uploadPartnerLogo', function() {

        $logoModel = new PartnerLogoModel;
        $target_dir = "upload/" . $_POST['new_path'] . "/";
		$temp = array();

        if (!file_exists($target_dir)) {

            mkdir($target_dir);

        }

        if (isset($_FILES['file']) && !empty($_FILES['file'])) {

            $temp = $_FILES['file']['tmp_name'];

        }


        foreach ($_FILES['file']['name'] as $key => $name) {

            $target_file = $target_dir . basename($name);

            $return = false;

            if (move_uploaded_file($temp[$key], $target_dir . time() . $key . $name)) {

                $fileName = time() . $key . $name;
                $return = $logoModel->uploadPartnerLogo($fileName, $target_dir, $name);

            }

        }

        echo json_encode($return);

    });
	
	$router->map('POST', '/uploadFrontEndDesign', function() {

        $frontEndModel = new FrontEndModel;
        $target_dir = "upload/" . $_POST['new_path'] . "/";
		$temp = array();

        if (!file_exists($target_dir)) {

            mkdir($target_dir);

        }

        if (isset($_FILES['file']) && !empty($_FILES['file'])) {

            $temp = $_FILES['file']['tmp_name'];

        }


        foreach ($_FILES['file']['name'] as $key => $name) {

            $target_file = $target_dir . basename($name);

            $return = false;

            if (move_uploaded_file($temp[$key], $target_dir . time() . $key . $name)) {

                $fileName = time() . $key . $name;
                $return = $frontEndModel->uploadFrontEndDesign($_POST, $fileName, $target_dir, $name);

            }

        }

        echo json_encode($return);

    });
	
	$router->map('POST', '/uploadFaqImages', function() {

        $faqModel = new FaqModel;
        $target_dir = "upload/" . $_POST['new_path'] . "/";
		$temp = array();

        if (!file_exists($target_dir)) {

            mkdir($target_dir);

        }

        if (isset($_FILES['file']) && !empty($_FILES['file'])) {

            $temp = $_FILES['file']['tmp_name'];

        }


        foreach ($_FILES['file']['name'] as $key => $name) {

            $target_file = $target_dir . basename($name);

            $return = false;

            if (move_uploaded_file($temp[$key], $target_dir . time() . $key . $name)) {

                $fileName = time() . $key . $name;
                $return = $faqModel->uploadFaqImages($_POST, $fileName, $target_dir, $name);

            }

        }

        echo json_encode($return);

    });
	
	$router->map('POST', '/uploadServicesIcon', function() {

        $servicesModel = new ServicesModel;
        $target_dir = "upload/" . $_POST['new_path'] . "/";
		$temp = array();

        if (!file_exists($target_dir)) {

            mkdir($target_dir);

        }

        if (isset($_FILES['file']) && !empty($_FILES['file'])) {

            $temp = $_FILES['file']['tmp_name'];

        }


        foreach ($_FILES['file']['name'] as $key => $name) {

            $target_file = $target_dir . basename($name);

            $return = false;

            if (move_uploaded_file($temp[$key], $target_dir . time() . $key . $name)) {

                $fileName = time() . $key . $name;
                $return = $servicesModel->uploadServicesIcon($_POST, $fileName, $target_dir, $name);

            }

        }

        echo json_encode($return);

    });
	
	$router->map('POST', '/clientPictureUpload', function() {

        $fileModel = new FileuploadModel;
        $target_dir = "upload/" . $_POST['new_path'] . "/";
		$temp = array();

        if (!file_exists($target_dir)) {

            mkdir($target_dir);

        }

        if (isset($_FILES['file']) && !empty($_FILES['file'])) {

            $temp = $_FILES['file']['tmp_name'];

        }


        foreach ($_FILES['file']['name'] as $key => $name) {

            $target_file = $target_dir . basename($name);

            $return = false;

            if (move_uploaded_file($temp[$key], $target_dir . time() . $key . $name)) {

                $fileName = time() . $key . $name;
                $return = $fileModel->clientPictureUpload($_POST, $fileName, $target_dir, $name);

            }

        }

        echo json_encode($return);

    });

	$router->map('GET', '/getPricingDetails', function() {
		
		$planModel = new PlanModel();
		 
		echo $planModel->getPricingDetails();
	   
	   
	});

    $router->map('GET', '/edit_home', function() {

        $pageModel = new PageModel();

        $fileUploadModel = new FileuploadModel();
        $getAllHomeEdits = $pageModel->getAllHomeEdits();
		
        $tablename = "edit_home";

        include linkPage("templateload_backend");

    });
	
	

    $router->map('GET', '/edit_services', function() {
		
		
		$pageModel = new PageModel();

        $fileUploadModel = new FileuploadModel();
        $getAllServicesEdits = $pageModel->getAllServicesEdits();
		
        $tablename = "edit_services";

        include linkPage("templateload_backend");

    });



    $router->map('GET', '/edit_about_us', function() {
		
		$pageModel = new PageModel();

        $fileUploadModel = new FileuploadModel();
        $getAllAboutUsEdits = $pageModel->getAllAboutUsEdits();
		
        $tablename = "edit_about_us";

        include linkPage("templateload_backend");

    });



    $router->map('GET', '/edit_faqs', function() {
		
		$pageModel = new PageModel();

        $fileUploadModel = new FileuploadModel();
        $getAllFaqEdits = $pageModel->getAllFaqEdits();
		
        $tablename = "edit_faqs";

        include linkPage("templateload_backend");

    });


    $router->map('GET', '/updateAccount', function() {
		
		$account = AccountModel::currentUser();
        $tablename = "update_account";

        include linkPage("templateload_backend");

    });

    $router->map('GET', '/edit_contact_us', function() {

		$pageModel = new PageModel();

        $fileUploadModel = new FileuploadModel();
        $getAllContactUsEdits = $pageModel->getAllContactUsEdits();
		
        $tablename = "edit_contact_us";

        include linkPage("templateload_backend");

    });
	
	$router->map('GET', '/logOut', function() {

          if (isset($_SESSION['authenticated']) && $_SESSION['authenticated']) {

			unset($_SESSION);



			session_destroy();

		}

		$tablename = 'login';

		include linkPage('templateload');

    });
	
	$router->map('POST', '/saveAccountUpdate', function() {

		$accountModel = new AccountModel;

		$result = $accountModel->createAccount($_POST);
		
		echo json_encode($result);

	});
	
	$router->map('POST', '/checkMatchPassword', function() {

		$accountModel = new AccountModel;

		$result = $accountModel->checkMatchPassword($_POST);
		
		echo json_encode($result);

	});
	
	
    $router->map('POST', '/accountFileUpload', function() {

        $fileModel = new FileuploadModel;
        $target_dir = "upload/" . $_POST['new_path'] . "/";
		$temp = array();

        if (!file_exists($target_dir)) {

            mkdir($target_dir);

        }

        if (isset($_FILES['file']) && !empty($_FILES['file'])) {

            $temp = $_FILES['file']['tmp_name'];

        }


        foreach ($_FILES['file']['name'] as $key => $name) {

            $target_file = $target_dir . basename($name);

            $return = false;

            if (move_uploaded_file($temp[$key], $target_dir . time() . $key . $name)) {

                $fileName = time() . $key . $name;
                $return = $fileModel->accountFileUpload($_POST, $fileName, $target_dir, $name);

            }

        }

        echo json_encode($return);

    });
	
}

	

