<?php

	$router->map('GET', '/contact_us', function() {
		
		$pageModel = new PageModel;
		$fileModel = new FileuploadModel;
		
		$pageDetails = $pageModel->getAllContactUsEdits();
		
		$tablename="contact_us";
		include linkPage("templateload");
	});


	$router->map('POST', '/sendEmail', function() {

		$emailModel = new EmailModel;
		
		$result = $emailModel->sendEmail($_POST);
		
		echo json_encode($result);

	});
	
