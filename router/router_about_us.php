<?php

$router->map('GET', '/about_us', function() {
	
	$pageModel = new PageModel;
	$fileModel = new FileuploadModel;
	$mapModel = new MapModel;
	$partnerLogoModel = new PartnerLogoModel;
	
	$pageDetails = $pageModel->getAboutUsPageDetails();
	$mapCoordinates = $mapModel->getMapCoordinates();
	$partnerLogos = $partnerLogoModel->getAllPartnerLogos();
	
    $tablename="about_us";
    include linkPage("templateload");
});

