<?php

$router->map('GET', '/services', function() {
	
	
	$pageModel = new PageModel;
	$fileModel = new FileuploadModel;
	$serviceModel = new ServicesModel;
	
	$pageDetails = $pageModel->getServicesPageDetails();
	$services = $serviceModel->getAllServicesForFrontPage();
	
    $tablename="services";
    include linkPage("templateload");
});

