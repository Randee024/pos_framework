<script>
//HEADING------------------------------------------------
	var myDropzone = new Dropzone('#myDropZone', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10 //for upload all at the same time
	});
	
	myDropzone.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-save-heading', function(e){

		var self = $('#form-heading-services');
		var ID = $('#id_edit').val();
		var checker = $('.dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#services-heading-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone').css({'border':'1px solid red'});
			}else{
				$('#myDropZone').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formHeadingValidation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#services-heading-upload #page_edit_id').val(res.id);
							if(classChecker){
								myDropzone.processQueue();
							}
							if(ID != ''){
								
								$('#message_services').text('You have successfully added a new design.');
								
								$('#successServicesMessage').modal({
									backdrop: 'static',
									keyboard: false
								});
							}else{
								$('#successServicesEdit').modal({
									backdrop: 'static',
									keyboard: false
								});
							}
						}
						
				}
				
			});
		}
		
	});
 

	Dropzone.autoDiscover = false;
	
	$('body').on('click','.change-services-heading-image',function(e){
		
		$('#services-heading-image').addClass('hidden');
		
		$('#services-heading-upload').removeClass('hidden');
		$('#services-heading-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-services-heading-image',function(e){
		
		$('#services-heading-upload').addClass('hidden');
		
		$('#services-heading-image').removeClass('hidden');
		$('#services-heading-image').slideDown();
		
	});
	
	function formHeadingValidation(){
		
		var validate = false;
		var error = 0;
		
		var title_heading = $('#title_heading').val();
		var sub_title_heading = $('#sub_title_heading').val();
		
		if(title_heading == ''){
			error++;
			$('#title_heading').css({'border':'1px solid red'});
		}else{
			$('#title_heading').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title_heading == ''){
			error++;
			$('#sub_title_heading').css({'border':'1px solid red'});
		}else{
			$('#sub_title_heading').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
//HEADING------------------------------------------------
//CONTAINER 1------------------------------------------------	
	
	$('#table-services').dataTable({
		"bProcessing":true,
		"bServerSide":true,
		"sAjaxSource":"/getAllServices",
		"aoColumnDefs":[{
			"bSortable":false,"aTargets":[3]
			}]
	});
	
	var myDropzone2 = new Dropzone('#myDropZone2', {
		url: "/uploadServicesIcon",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10 //for upload all at the same time
	});
	
	myDropzone2.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-add-services',function(e){
		resetServiceForm();
		$('#addServices').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	
	$('body').on('click','.btn-save-service', function(e){
		
		
		var self = $('#form-service');

		var checker = $('#myDropZone2 .dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#service-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone2').css({'border':'1px solid red'});
			}else{
				$('#myDropZone2').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formServiceValidation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/addServices',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							
							$('#service-upload #id_service').val(res.id);
							
							if(classChecker){
								myDropzone2.processQueue();
							}
							
							$('#message_service').text('You have successfully added a new service.');
							$('#successServicesMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
	});
	
	
	$('body').on('click','.btn-delete-service', function(e){
		
		var ID = $(this).data('id');
		$('#delete-service-form #id_service').val(ID);
		
		$('#deleteService').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	
	$('body').on('click','.btn-delete-confirm-service', function(e){
			
			var self = $('#delete-service-form');
			
			$.ajax({
				url:'/deleteService',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_service').text('Service has been deleted.');
							$('#successServicesMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
	
	
	$('body').on('click','.btn-edit-service', function(e){
		
		resetServiceForm();
		var ID = $(this).data('id');
		
		$.ajax({
				url:'/getServiceDetails',
				data:{id:ID},
				type:'POST',
				dataType:'json',
				success:function(res){
					
					$.each(res,function(i,field){
					
						$('#service-upload #id_service').val(field.id);
						$('#form-service #id_service').val(field.id);
						$('#form-service #title').val(field.title);
						$('#form-service #description').val(field.description);
						$('#service_icon').html('<img class="img-responsive" alt="" src="'+field.path+field.file_name+'">');
					});
					
					if($('#service_icon').children().length == 0){
						$('#service-image').addClass('hidden');
						$('#service-upload').removeClass('hidden');
						$('.cancel-service-image').removeClass('hidden');
					}else{
						$('#service-image').removeClass('hidden');
						$('#service-upload').addClass('hidden');
						$('.cancel-service-image').addClass('hidden');
					}
					
					$('#addServices').modal({
						backdrop: 'static',
						keyboard: false
					});
					
				}
				
			});
		
	});
	
	
	$('body').on('click','.change-service-image',function(e){
		
		$('#service-image').addClass('hidden');
		
		$('#service-upload').removeClass('hidden');
		$('.cancel-service-image').removeClass('hidden');
		$('#service-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-service-image',function(e){
		
		$('#service-upload').addClass('hidden');
		$('.cancel-service-image').addClass('hidden');
		
		$('#service-image').removeClass('hidden');
		$('#service-image').slideDown();
		
	});
	
	
	
	$('body').on('click','.btn-activation-confirm-service', function(e){
			
			var self = $('#activation-service-form');
			
			$.ajax({
				url:'/activationService',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_services').text('Service status has been changed.');
							$('#successServicesMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
	
	
	$('body').on('click','.btn-activation-service', function(e){
		
		var ID = $(this).data('id');
		$('#activation-service-form #id_service').val(ID);
		
		$('#servicesActivation').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	
	function formServiceValidation(){
		var validate = false;
		var error = 0;
		
		var title = $('#form-service #title').val();
		var description = $('#form-service #description').val();
		
		
		if(title == ''){
			error++;
			$('#form-service #title').css({'border':'1px solid red'});
		}else{
			$('#form-service #title').css({'border':'1px solid #ccc'});
		}
		
		if(description == ''){
			error++;
			$('#form-service #description').css({'border':'1px solid red'});
		}else{
			$('#form-service #description').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
	}
	
	function resetServiceForm(){
		$('#service-upload #id_service').val('');
		$('#form-service #id_service').val('');
		$('#form-service #title').val('');
		$('#form-service #description').val('');
		$('#service_icon').html('');
		$('#service-image').addClass('hidden');
		$('#service-upload').removeClass('hidden');
		$('.cancel-service-image').addClass('hidden');
					
	}
	
//CONTAINER 1------------------------------------------------


</script>