<script>
	$('.numOnly').keypress(function(event) {
		
		if ((event.which != 46 || $(this).val().indexOf('.') != -1) && ((event.which < 48 || event.which > 57) && event.which != 8)) {
				event.preventDefault(); 
		} 
	
	});
//HEADING------------------------------------------------
	var myDropzone = new Dropzone('#myDropZone', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif"
	});
	
	myDropzone.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-save-heading', function(e){

		var self = $('#form-heading-about-us');
		var checker = $('.dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#about-us-heading-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone').css({'border':'1px solid red'});
			}else{
				$('#myDropZone').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formHeadingValidation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#about-us-heading-upload #page_edit_id').val(res.id);
							if(classChecker){
								myDropzone.processQueue();
							}
							
							$('#successAboutUsEdit').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
 

	Dropzone.autoDiscover = false;
	
	$('body').on('click','.change-about-us-heading-image',function(e){
		
		$('#about-us-heading-image').addClass('hidden');
		
		$('#about-us-heading-upload').removeClass('hidden');
		$('#about-us-heading-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-about-us-heading-image',function(e){
		
		$('#about-us-heading-upload').addClass('hidden');
		
		$('#about-us-heading-image').removeClass('hidden');
		$('#about-us-heading-image').slideDown();
		
	});
	
	function formHeadingValidation(){
		
		var validate = false;
		var error = 0;
		
		var title_heading = $('#title_heading').val();
		var sub_title_heading = $('#sub_title_heading').val();
		
		if(title_heading == ''){
			error++;
			$('#title_heading').css({'border':'1px solid red'});
		}else{
			$('#title_heading').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title_heading == ''){
			error++;
			$('#sub_title_heading').css({'border':'1px solid red'});
		}else{
			$('#sub_title_heading').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
//HEADING------------------------------------------------
//CONTAINER1------------------------------------------------
	var myDropzone2 = new Dropzone('#myDropZone2', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif"
	});
	
	myDropzone2.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-save-container-1', function(e){

		var self = $('#container-1-from');
		var checker = $('#myDropZone2 .dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#about-us-container1-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone2').css({'border':'1px solid red'});
			}else{
				$('#myDropZone2').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formContainer1Validation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#about-us-container1-upload #page_edit_id').val(res.id);
							if(classChecker){
								myDropzone2.processQueue();
							}
							
							$('#successAboutUsEdit').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
 
	
	$('body').on('click','.change-about-us-container1-image',function(e){
		
		$('#about-us-container1-image').addClass('hidden');
		
		$('#about-us-container1-upload').removeClass('hidden');
		$('#about-us-container1-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-about-us-container1-image',function(e){
		
		$('#about-us-container1-upload').addClass('hidden');
		
		$('#about-us-container1-image').removeClass('hidden');
		$('#about-us-container1-image').slideDown();
		
	});
	
	function formContainer1Validation(){
		
		var validate = false;
		var error = 0;
		
		var title = $('#container-1-from #title').val();
		var sub_title = $('#container-1-from #sub_title').val();
		
		if(title == ''){
			error++;
			$('#container-1-from #title').css({'border':'1px solid red'});
		}else{
			$('#container-1-from #title').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title == ''){
			error++;
			$('#container-1-from #sub_title').css({'border':'1px solid red'});
		}else{
			$('#container-1-from #sub_title').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
//CONTAINER1------------------------------------------------
//CONTAINER2------------------------------------------------
	
	$('#table-map').dataTable({
		"bProcessing":true,
		"bServerSide":true,
		"sAjaxSource":"/getAllMap",
		"aoColumnDefs":[{
			"bSortable":false,"aTargets":[4]
			}]
	});
	
	$('body').on('click','.btn-add-map', function(e){
		resetMapForm();
		$('#addMap').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});

	
	$('body').on('click','.btn-save-map', function(e){

		var self = $('#form-map');
		
		var error = 0;
		
		
		if(!formMapValidation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/addMap',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res){
							$('#message_about_us').text('Map coordinates has been added.');
							$('#successAboutUsMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
 
	
	$('body').on('click','.btn-delete-map', function(e){
		
		var ID = $(this).data('id');
		$('#delete-map-form #id_map').val(ID);
		
		$('#deleteMap').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	
	$('body').on('click','.btn-delete-confirm-map', function(e){
			
			var self = $('#delete-map-form');
			
			$.ajax({
				url:'/deleteMap',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_about_us').text('Coordinates has been deleted.');
							$('#successAboutUsMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
	
	$('body').on('click','.btn-edit-map', function(e){
		
		resetMapForm();
		var ID = $(this).data('id');
		
		$.ajax({
				url:'/getMapDetails',
				data:{id:ID},
				type:'POST',
				dataType:'json',
				success:function(res){
					
					$.each(res,function(i,field){
					
						$('#form-map #id_map').val(field.id);
						$('#form-map #latitude').val(field.latitude);
						$('#form-map #longitude').val(field.longitude);
					});
					
					$('#addMap').modal({
						backdrop: 'static',
						keyboard: false
					});
					
				}
				
			});
		
	});
	
	
	$('body').on('click','.btn-activation-confirm-map', function(e){
			
			var self = $('#activation-map-form');
			
			$.ajax({
				url:'/activationMap',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_about_us').text('Coordinates status has been changed.');
							$('#successAboutUsMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
	
	
	$('body').on('click','.btn-activation-map', function(e){
		
		var ID = $(this).data('id');
		$('#activation-map-form #id_map').val(ID);
		
		$('#mapActivation').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	
	function formMapValidation(){
		var validate = false;
		var error = 0;
		
		var latitude = $('#form-map #latitude').val();
		var longitude = $('#form-map #longitude').val();
		
		
		if(latitude == ''){
			error++;
			$('#form-map #latitude').css({'border':'1px solid red'});
		}else{
			$('#form-map #latitude').css({'border':'1px solid #ccc'});
		}
		
		if(longitude == ''){
			error++;
			$('#form-map #longitude').css({'border':'1px solid red'});
		}else{
			$('#form-map #longitude').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
	}
	function resetMapForm(){
		$('#form-map #id_map').val('');
		$('#form-map #latitude').val('');
		$('#form-map #longitude').val('');
		
					
	}
//CONTAINER2------------------------------------------------
//CONTAINER3------------------------------------------------
	var myDropzone3 = new Dropzone('#myDropZone3', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif"
	});
	
	myDropzone3.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-save-container-3', function(e){

		var self = $('#form-container-3');
		var checker = $('#myDropZone3 .dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#container-3-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone3').css({'border':'1px solid red'});
			}else{
				$('#myDropZone3').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formContainer3Validation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#container-3-upload #page_edit_id').val(res.id);
							if(classChecker){
								myDropzone3.processQueue();
							}
							
							$('#successAboutUsEdit').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
	
	$('body').on('click','.change-container-3-image',function(e){
		
		$('#container-3-image').addClass('hidden');
		
		$('#container-3-upload').removeClass('hidden');
		$('#container-3-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-container-3-image',function(e){
		
		$('#container-3-upload').addClass('hidden');
		
		$('#container-3-image').removeClass('hidden');
		$('#container-3-image').slideDown();
		
	});
	
	function formContainer3Validation(){
		
		var validate = false;
		var error = 0;
		
		var title = $('#form-container-3 #title').val();
		var sub_title = $('#form-container-3 #sub_title').val();
		
		if(title == ''){
			error++;
			$('#form-container-3 #title').css({'border':'1px solid red'});
		}else{
			$('#form-container-3 #title').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title == ''){
			error++;
			$('#form-container-3 #sub_title').css({'border':'1px solid red'});
		}else{
			$('#form-container-3 #sub_title').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
//CONTAINER3------------------------------------------------
//CONTAINER4------------------------------------------------
	
	$('#table-container-4').dataTable({
		"bProcessing":true,
		"bServerSide":true,
		"sAjaxSource":"/getAllAboutUsFileUploaded",
		"aoColumnDefs":[{
			"bSortable":false,"aTargets":[3]
			}]
	});
	
	var myDropzone4 = new Dropzone('#myDropZone4', {
		url: "/uploadPartnerLogo",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 6, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif",
		queuecomplete: function() {
			$('#message_about_us').text('Upload complete.');
			$('#successAboutUsMessage').modal({
				backdrop: 'static',
				keyboard: false
			});
		}
	});
	
	myDropzone4.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-add-logo', function(e){
		$('#addLogo').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});

	$('body').on('click','.btn-save-logo', function(e){
		var checker = $('#myDropZone4 .dz-preview').children().length;

		if(checker > 0){
			myDropzone4.processQueue();					
			$('#myDropZone4').css({'border':'1px solid #ccc'});
		}else{
			$('#myDropZone4').css({'border':'1px solid red'});
		}
		

	});
	
	$('body').on('click','.btn-delete-logo', function(e){
		
		var ID = $(this).data('id');
		$('#delete-logo-form #id_logo').val(ID);
		
		$('#deleteLogo').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	
	$('body').on('click','.btn-delete-confirm-logo', function(e){
			
			var self = $('#delete-logo-form');
			
			$.ajax({
				url:'/deleteLogo',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_about_us').text('Partner Logo has been deleted.');
							$('#successAboutUsMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
	
	$('body').on('click','.btn-activation-confirm-logo', function(e){
			
			var self = $('#activation-logo-form');
			
			$.ajax({
				url:'/activationLogo',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_about_us').text('Partner Logo status has been changed.');
							$('#successAboutUsMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
	
	
	$('body').on('click','.btn-save-container-4', function(e){
		var title = $('#form-container-4 #title').val();
		var self = $('#form-container-4');
		
		if(title != ''){
			
			$('#form-container-4 #title').css({'border':'1px solid #ccc'});
			$.ajax({
				url:'/addLogoTitle',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
					
					if(res){
						$('#successAboutUsEdit').modal({
							backdrop: 'static',
							keyboard: false
						});
						
					}
					
				}
				
			});
			
		}else{
			
			$('#form-container-4 #title').css({'border':'1px solid red'});
		}
		
		
	});
	
	
	$('body').on('click','.btn-activation-logo', function(e){
		
		var ID = $(this).data('id');
		$('#activation-logo-form #id_logo').val(ID);
		
		$('#logoActivation').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	
//CONTAINER4------------------------------------------------

</script>