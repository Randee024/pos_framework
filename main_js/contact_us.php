<script>

	
	$('body').on('click','.btn-send-message',function(e){
		
		$('#pleaseWait').modal();
		e.preventDefault();
		$('#spinner').removeClass('hidden');
		$('#envelope').addClass('hidden');
		
		var self = $('#form-contact-us');
		if(validateForm()){
			$.ajax({
				url:'sendEmail',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
					
					$('#pleaseWait').fadeOut();
					$('#spinner').addClass('hidden');
					$('#envelope').removeClass('hidden');
					if(res.status){
						
						$('#message_contact_us').text('You have successfully send a message.');
						$('#successContactUsMessage').modal({
							backdrop:'static',
							keyboard:false
						});
						
					}else{
						$('#error_message').text(res.message);
						$('#errorGeneralMessage').modal({
							backdrop:'static',
							keyboard:false
						});
						
					}
					
				}
				
			});
		}else{
			$('#pleaseWait').fadeOut();
			$('#spinner').addClass('hidden');
			$('#envelope').removeClass('hidden');
		}
	});
	
	function validateForm()
	{
		var name = $('#name').val();
		var subject = $('#subject').val();
		var email = $('#email').val();
		var message = $('#message').val();
		var validate = true;
		var error = 0;
		
		if(name != ''){

			$('#name').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#name').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(subject != ''){

			$('#subject').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#subject').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(message != ''){

			$('#message').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#message').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(email != ''){
			
			if(validateEmail(email)){
				$('#email').css({'border':'1px solid #ccc'});
			}else{
				error++;
				$('#email').css({'border':'1px solid #fe0b0b'});
			}
			
			
		}else{
			
			error++;
			$('#email').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(error > 0 ){
			validate = false;
		}
		return validate;
		
		
	}
	
	function validateEmail(email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  return emailReg.test( email );
	}
</script>