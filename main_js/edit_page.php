<script>

//HEADING------------------------------------------------
	var myDropzone = new Dropzone('#myDropZone', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10 //for upload all at the same time
	});
	
	myDropzone.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-save-heading', function(e){

		var self = $('#form-heading-home');
		var checker = $('.dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#home-heading-image' ).hasClass( 'hidden' );
		
		if(checker = 0 ){
			
			if(classChecker){
				error++;
			}
		}
		

		if(error == 0){
			
			$.ajax({
				url:'/changeHeading',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('.heading-home #page_edit_id').val(res.id);
							
							if(classChecker){
								myDropzone.processQueue();
							}
							
							var r = confirm("Congrats!.");
							if (r == true) {
								 window.location.replace("/edit_page");
							}else{
								 window.location.replace("/edit_page");
							}
						}
						
				}
				
			});
		}
		
	});
 

	Dropzone.autoDiscover = false;
	
	$('body').on('click','.change-home-heading-image',function(e){
		
		$('#home-heading-image').addClass('hidden');
		
		$('#home-heading-upload').removeClass('hidden');
		$('#home-heading-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-home-heading-image',function(e){
		
		$('#home-heading-upload').addClass('hidden');
		
		$('#home-heading-image').removeClass('hidden');
		$('#home-heading-image').slideDown();
		
	});
//HEADING------------------------------------------------
</script>