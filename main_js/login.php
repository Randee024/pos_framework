<script>
	
	$('body').on('click', '.button-login', function(e){
		
		e.preventDefault();
		
		var self = $('#login-form');
		
		if(validateForm()){
			$.ajax({
				url: '/authenticate',
				type: 'post',
				data: self.serialize(),
				dataType: 'json',
				success: function(res){
					
					if(res.auth){
						
						$('#successLogin').modal({
							backdrop: 'static',
							keyboard: false
						})
					}else{
						
						$('#username').css({'border':'1px solid #fe0b0b'});
						$('#password').css({'border':'1px solid #fe0b0b'});
			
						$('#errorLogin').modal({
							backdrop: 'static',
							keyboard: false
						})
					}
				}
			});
		}
		
		
	});
	
	function validateForm() {
		
		var username = $('#username').val();
		var password = $('#password').val();
		var validate = false;
		
		if(username != ''){
			
			validate = true;
			$('#username').css({'border':'1px solid #ccc'});
			
		}else{
			
			validate = false;
			$('#username').css({'border':'1px solid #fe0b0b'});
		}
		
		if(password != ''){

			validate = validate && true;
			$('#password').css({'border':'1px solid #ccc'});
			
		}else{
			
			validate = false;
			$('#password').css({'border':'1px solid #fe0b0b'});
			
		}
		
		
		return validate;
		
		
	}
</script>