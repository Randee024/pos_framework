<script>

//HEADING------------------------------------------------
	var myDropzone = new Dropzone('#myDropZone', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif",
	});
	
	myDropzone.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-save-heading', function(e){

		var self = $('#form-heading-faq');
		var checker = $('.dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#faq-heading-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone').css({'border':'1px solid red'});
			}else{
				$('#myDropZone').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formHeadingValidation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#faq-heading-upload #page_edit_id').val(res.id);
							if(classChecker){
								myDropzone.processQueue();
							}
							
							$('#successFaqEdit').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
 

	Dropzone.autoDiscover = false;
	
	$('body').on('click','.change-faq-heading-image',function(e){
		
		$('#faq-heading-image').addClass('hidden');
		
		$('#faq-heading-upload').removeClass('hidden');
		$('#faq-heading-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-faq-heading-image',function(e){
		
		$('#faq-heading-upload').addClass('hidden');
		
		$('#faq-heading-image').removeClass('hidden');
		$('#faq-heading-image').slideDown();
		
	});
	
	function formHeadingValidation(){
		
		var validate = false;
		var error = 0;
		
		var title_heading = $('#title_heading').val();
		var sub_title_heading = $('#sub_title_heading').val();
		
		if(title_heading == ''){
			error++;
			$('#title_heading').css({'border':'1px solid red'});
		}else{
			$('#title_heading').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title_heading == ''){
			error++;
			$('#sub_title_heading').css({'border':'1px solid red'});
		}else{
			$('#sub_title_heading').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
//HEADING------------------------------------------------
//CONTAINER 1------------------------------------------------
	$('#table-faq').dataTable({
		"bProcessing":true,
		"bServerSide":true,
		"sAjaxSource":"/getAllFaqs",
		"aoColumnDefs":[{
			"bSortable":false,"aTargets":[2]
			}]
	});
	
	var myDropzone2 = new Dropzone('#myDropZone2', {
		url: "/uploadFaqImages",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif",
	});
	
	myDropzone2.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-add-faq' , function(e){
		resetFaqForm();
		$('#addFaq').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	$('body').on('click','.btn-save-faq', function(e){
		
		
		var self = $('#form-faq');

		
		var error = 0;
		var classChecker = $( '#faq-image' ).hasClass( 'hidden' );
		
		
		if(!formFaqValidation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/addFaq',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#faq-upload #id_faq').val(res.id);
							
							if(classChecker){
								myDropzone2.processQueue();
							}
							
							$('#message_faq').text('You have successfully added a new FAQ.');
							$('#successFaqMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
	});
	
	$('body').on('click','.btn-edit-faq', function(e){
		
		resetFaqForm();
		var ID = $(this).data('id');
		
		$.ajax({
				url:'/getFaqDetails',
				data:{id:ID},
				type:'POST',
				dataType:'json',
				success:function(res){
					
					$.each(res,function(i,field){
					
						$('#faq-upload #id_faq').val(field.id);
						$('#form-faq #id_faq').val(field.id);
						$('#form-faq #title').val(field.title);
						$('#form-faq #content').val(field.content);
						$('#form-faq #category').val(field.category);
						
						if(field.path != null){
							$('#faq_bg_image').html('<img class="img-responsive" alt="" src="'+field.path+field.file_name+'">');
						}
						
					});
					
					if($('#faq_bg_image').children().length == 0){
						$('#faq-image').addClass('hidden');
						$('#faq-upload').removeClass('hidden');
						// $('.cancel-faq-image').removeClass('hidden');
					}else{
						$('#faq-image').removeClass('hidden');
						$('#faq-upload').addClass('hidden');
						$('.cancel-faq-image').addClass('hidden');
					}
					
					$('#addFaq').modal({
						backdrop: 'static',
						keyboard: false
					});
					
				}
				
			});
		
	});
	
	$('body').on('click','.btn-delete-faq', function(e){
		
		var ID = $(this).data('id');
		$('#delete-faq-form #id_faq').val(ID);
		
		$('#deleteFaq').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	
	$('body').on('click','.btn-delete-confirm-faq', function(e){
			
			var self = $('#delete-faq-form');
			
			$.ajax({
				url:'/deleteFaq',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_faq').text('FAQ has been deleted.');
							$('#successFaqMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
	
	
	$('body').on('click','.change-faq-image',function(e){
		
		$('#faq-image').addClass('hidden');
		
		$('#faq-upload').removeClass('hidden');
		$('.cancel-faq-image').removeClass('hidden');
		$('#faq-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-faq-image',function(e){
		
		$('#faq-upload').addClass('hidden');
		$('.cancel-faq-image').addClass('hidden');
		
		$('#faq-image').removeClass('hidden');
		$('#faq-image').slideDown();
		
	});
	
	
	function formFaqValidation(){
		var validate = false;
		var error = 0;
		
		var title = $('#form-faq #title').val();
		var content = $('#form-faq #content').val();
		
		
		if(title == ''){
			error++;
			$('#form-faq #title').css({'border':'1px solid red'});
		}else{
			$('#form-faq #title').css({'border':'1px solid #ccc'});
		}
		
		if(content == ''){
			error++;
			$('#form-faq #content').css({'border':'1px solid red'});
		}else{
			$('#form-faq #content').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
	}
	
	function resetFaqForm(){
		$('#faq-upload #id_faq').val('');
		$('#form-faq #id_faq').val('');
		$('#form-faq #title').val('');
		$('#form-faq #content').val('');
		$('#faq_bg_image').html('');
		$('#faq-image').addClass('hidden');
		$('#faq-upload').removeClass('hidden');
		$('.cancel-faq-image').addClass('hidden');
					
	}
//CONTAINER 1------------------------------------------------
</script>