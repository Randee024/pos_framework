<script>
	var myDropzone = new Dropzone('#myDropZone', {
		url: "/accountFileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif",
		queuecomplete: function() {
			$('#successLogin').modal({
				backdrop: 'static',
				keyboard: false
			})
		}
	});
	
	myDropzone.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$(document).ready(function() {
    
		var navListItems = $('ul.setup-panel li a'),
			setUpContent = $('.setup-content');

			setUpContent.hide();

		navListItems.click(function(e)
		{
			e.preventDefault();
			var $target = $($(this).attr('href')),
				$item = $(this).closest('li');
			
			if (!$item.hasClass('disabled')) {
				navListItems.closest('li').removeClass('active');
				$item.addClass('active');
				setUpContent.hide();
				$target.show();
				
				if($('#step-1-indicator').hasClass('active')){
					$('ul.setup-panel li:eq(1)').addClass('disabled');
					$('ul.setup-panel li:eq(2)').addClass('disabled');
				}else if($('#step-2-indicator').hasClass('active')){
					$('ul.setup-panel li:eq(2)').addClass('disabled');
				}
				
				
			}
		});
		
		
		
		$('ul.setup-panel li.active a').trigger('click');		  
	});

	
	$('body').on('click','.btn-next-step-2', function(e) {
		
		if(validateFirstForm()){
			$('ul.setup-panel li:eq(1)').removeClass('disabled');
			$('#step-2').removeClass('hidden');
			$('ul.setup-panel li a[href="#step-2"]').trigger('click');
		}
		
	})  
	
	$('body').on('click','.btn-next-step-3', function(e) {
		
		if(validateSecondForm()){
			$('ul.setup-panel li:eq(2)').removeClass('disabled');
			$('#step-3').removeClass('hidden');
			$('ul.setup-panel li a[href="#step-3"]').trigger('click');
			
			$('.dz-message span').text('Drop photo here to upload');
		}
		
	})  
	
	$('body').on('click', '.btn-create-account', function(e){
		
		e.preventDefault();
		
		var self = $('#create-account-form');
		var checker = $('.dz-preview').children().length;
		var error = 0;

		if(checker == 0 ){
			error++;
			$('#myDropZone').css({'border':'1px solid #fe0b0b'});
		}
		
		if(error == 0){
			$.ajax({
				url: '/createAccount',
				type: 'POST',
				data: self.serialize(),
				dataType: 'json',
				success: function(res){
					
					if(res.status){
						
						$('#account_id').val(res.id);
						myDropzone.processQueue();

					}
				}
			});
		}
		
		
	});
	
	function validateFirstForm() {
		
		var first_name = $('#first_name').val();
		var last_name = $('#last_name').val();
		var email = $('#email').val();
		var validate = true;
		var error = 0;
		
		if(first_name != ''){

			$('#first_name').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#first_name').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(last_name != ''){

			$('#last_name').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#last_name').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(email != ''){
			
			if(validateEmail(email)){
				$('#email').css({'border':'1px solid #ccc'});
			}else{
				error++;
				$('#email').css({'border':'1px solid #fe0b0b'});
			}
			
			
		}else{
			
			error++;
			$('#email').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(error > 0 ){
			validate = false;
		}
		return validate;
		
		
	}
	
	function validateEmail(email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  return emailReg.test( email );
	}
	
	function validateSecondForm() {
		
		var username = $('#username').val();
		var password = $('#password').val();
		var confirm_password = $('#confirm_password').val();
		var validate = true;
		var error = 0;
		
		if(username != ''){
			
			$('#username').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#username').css({'border':'1px solid #fe0b0b'});
		}
		
		if(password != ''){

			$('#password').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#password').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(confirm_password != ''){

			$('#confirm_password').css({'border':'1px solid #ccc'});
			
			if(confirm_password == password){
				$('#confirm_password').css({'border':'1px solid #ccc'});
			}else{
				error++;
				$('#password').css({'border':'1px solid #fe0b0b'});
				$('#confirm_password').css({'border':'1px solid #fe0b0b'});
			}
			
		}else{
			
			error++;
			$('#confirm_password').css({'border':'1px solid #fe0b0b'});
			
		}
		
		
		if(error > 0 ){
			validate = false;
		}
		return validate;
		
		
	}
	
	

</script>