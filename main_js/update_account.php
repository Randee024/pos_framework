<script>
	var myDropzone = new Dropzone('#myDropZone', {
		url: "/accountFileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif",
		queuecomplete: function() {
			$('#message_update').text('You have successfully changed your photo.');
					
			$('#successUpdateAccountMessage').modal({
				keyboard:false,
				backdrop:'static'
				
			});
		}
	});
	
	myDropzone.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	
	$('body').on('click','.btn-save-info',function(e){
		
		var self = $('#form-user-info');
		
		if(validateInfoForm()){
			$.ajax({
				url:'saveAccountUpdate',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
					
					if(res.status){
						$('#message_update').text('You have successfully updated your account details.');
					
						$('#successUpdateAccountMessage').modal({
							keyboard:false,
							backdrop:'static'
							
						});
					}else{
						$('#error_message').text('Failed to update account details.');
						$('#errorMessage').modal({
							keyboard:false,
							backdrop:'static'
							
						});
					}
					
					
				}
				
			});
		}else{
			
			$('#error_message').text('Failed to update account details.');
			$('#errorMessage').modal({
				keyboard:false,
				backdrop:'static'
				
			});
		}
		
	});
	
	$('body').on('click','.btn-upload-photo',function(e){
		
		var checker = $('.dz-preview').children().length;
		var error = 0;

		if(checker == 0 ){
			error++;
			$('#myDropZone').css({'border':'1px solid #fe0b0b'});
		}else{
			myDropzone.processQueue();
		}
		
	});
	$('body').on('click','.btn-save-pass',function(e){
		
		if(validatePassForm()){
			
			var self = $('#form-change-pass');
			
			$.ajax({
				url:'checkMatchPassword',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(response){
					if(response){
						$.ajax({
							url:'saveAccountUpdate',
							data:self.serialize(),
							type:'POST',
							dataType:'json',
							success:function(res){
								if(res.status){
										$('#message_update').text('Password has been changed.');
									
										$('#successUpdateAccountMessage').modal({
											keyboard:false,
											backdrop:'static'
											
										});
									}else{
										$('#error_message').text('Failed to update account details.');
										$('#errorMessage').modal({
											keyboard:false,
											backdrop:'static'
											
										});
									}
								}
						});
						
					}else{
						$('#old_password').css({'border':'1px solid #fe0b0b'});
					}
				}
				
			});
			
		}else{
			$('#error_message').text('Failed to change password.');
			$('#errorMessage').modal({
				keyboard:false,
				backdrop:'static'
				
			});
		}
		
		
	});
	
	
	function validateInfoForm() {
		
		var first_name = $('#first_name').val();
		var last_name = $('#last_name').val();
		var email = $('#email').val();
		var validate = true;
		var error = 0;
		
		if(first_name != ''){

			$('#first_name').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#first_name').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(last_name != ''){

			$('#last_name').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#last_name').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(email != ''){
			
			if(validateEmail(email)){
				$('#email').css({'border':'1px solid #ccc'});
			}else{
				error++;
				$('#email').css({'border':'1px solid #fe0b0b'});
			}
			
			
		}else{
			
			error++;
			$('#email').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(error > 0 ){
			validate = false;
		}
		return validate;
		
		
	}
	
	function validateEmail(email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  return emailReg.test( email );
	}
	
	function validatePassForm() {
		
		var username = $('#username').val();
		var old_password = $('#old_password').val();
		var password = $('#password').val();
		var confirm_password = $('#confirm_password').val();
		var validate = true;
		var error = 0;
		
		if(username != ''){
			
			$('#username').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#username').css({'border':'1px solid #fe0b0b'});
		}
		
		if(password != ''){

			$('#password').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#password').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(old_password != ''){

			$('#old_password').css({'border':'1px solid #ccc'});
			
		}else{
			
			error++;
			$('#old_password').css({'border':'1px solid #fe0b0b'});
			
		}
		
		if(confirm_password != ''){

			$('#confirm_password').css({'border':'1px solid #ccc'});
			
			if(confirm_password == password){
				$('#confirm_password').css({'border':'1px solid #ccc'});
			}else{
				error++;
				$('#password').css({'border':'1px solid #fe0b0b'});
				$('#confirm_password').css({'border':'1px solid #fe0b0b'});
			}
			
		}else{
			
			error++;
			$('#confirm_password').css({'border':'1px solid #fe0b0b'});
			
		}
		
		
		if(error > 0 ){
			validate = false;
		}
		return validate;
		
		
	}
	
</script>