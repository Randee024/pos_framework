<script>
	$('.numOnly').keypress(function(event) {
		
		if ((event.which != 46 || $(this).val().indexOf('.') != -1) && ((event.which < 48 || event.which > 57) && event.which != 8)) {
				event.preventDefault(); 
		} 
	
	});
//HEADING------------------------------------------------
	var myDropzone = new Dropzone('#myDropZone', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif"
	});
	
	myDropzone.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-save-heading', function(e){

		var self = $('#form-heading-home');
		var checker = $('.dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#home-heading-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone').css({'border':'1px solid red'});
			}else{
				$('#myDropZone').css({'border':'1px solid #ccc'});
			}
		}

		if(!formHeadingValidation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#home-heading-upload #page_edit_id').val(res.id);
							if(classChecker){
								myDropzone.processQueue();
							}
							
							$('#successHomeEdit').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
 

	Dropzone.autoDiscover = false;
	
	$('body').on('click','.change-home-heading-image',function(e){
		
		$('#home-heading-image').addClass('hidden');
		
		$('#home-heading-upload').removeClass('hidden');
		$('#home-heading-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-home-heading-image',function(e){
		
		$('#home-heading-upload').addClass('hidden');
		
		$('#home-heading-image').removeClass('hidden');
		$('#home-heading-image').slideDown();
		
	});
	
	function formHeadingValidation(){
		
		var validate = false;
		var error = 0;
		
		var title_heading = $('#title_heading').val();
		var sub_title_heading = $('#sub_title_heading').val();
		
		if(title_heading == ''){
			error++;
			$('#title_heading').css({'border':'1px solid red'});
		}else{
			$('#title_heading').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title_heading == ''){
			error++;
			$('#sub_title_heading').css({'border':'1px solid red'});
		}else{
			$('#sub_title_heading').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
//HEADING------------------------------------------------

//CONTAINER 1 ---------------------------------------------

	$('body').on('click','.btn-save-container-1', function(e){

		var self = $('#container-1-from');

		var error = 0;
		
		if(!formContainer1Validation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
					$('#successHomeEdit').modal({
							backdrop: 'static',
							keyboard: false
						});
						
				}
				
			});
		}
		
	});
	
	function formContainer1Validation(){
		
		var validate = false;
		var error = 0;

		var title = $('#container-1-from #title').val();
		var sub_title = $('#container-1-from #sub_title').val();
		
		if(title == ''){
			error++;
			$('#container-1-from #title').css({'border':'1px solid red'});
		}else{
			$('#container-1-from #title').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title == ''){
			error++;
			$('#container-1-from #sub_title').css({'border':'1px solid red'});
		}else{
			$('#container-1-from #sub_title').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
//CONTAINER 1 ---------------------------------------------

//CONTAINER2------------------------------------------------
	var myDropzone2 = new Dropzone('#myDropZone2', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif"
	});
	
	myDropzone2.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-save-container-2', function(e){

		var self = $('#container-2-from');
		var checker = $('#myDropZone2 .dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#home-container2-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone2').css({'border':'1px solid red'});
			}else{
				$('#myDropZone2').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formContainer2Validation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#home-container2-upload #page_edit_id').val(res.id);
							if(classChecker){
								myDropzone2.processQueue();
							}
							
							$('#successHomeEdit').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
 
	
	$('body').on('click','.change-home-container2-image',function(e){
		
		$('#home-container2-image').addClass('hidden');
		
		$('#home-container2-upload').removeClass('hidden');
		$('#home-container2-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-home-container2-image',function(e){
		
		$('#home-container2-upload').addClass('hidden');
		
		$('#home-container2-image').removeClass('hidden');
		$('#home-container2-image').slideDown();
		
	});
	
	function formContainer2Validation(){
		
		var validate = false;
		var error = 0;
		
		var title = $('#container-2-from #title').val();
		var sub_title = $('#container-2-from #sub_title').val();
		
		if(title == ''){
			error++;
			$('#container-2-from #title').css({'border':'1px solid red'});
		}else{
			$('#container-2-from #title').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title == ''){
			error++;
			$('#container-2-from #sub_title').css({'border':'1px solid red'});
		}else{
			$('#container-2-from #sub_title').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
//CONTAINER2------------------------------------------------

//CONTAINER3------------------------------------------------
	var myDropzone3 = new Dropzone('#myDropZone3', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif"
	});
	
	myDropzone3.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-save-container-3', function(e){

		var self = $('#container-3-from');
		var checker = $('#myDropZone3 .dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#home-container3-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone3').css({'border':'1px solid red'});
			}else{
				$('#myDropZone3').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formContainer3Validation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#home-container3-upload #page_edit_id').val(res.id);
							if(classChecker){
								myDropzone3.processQueue();
							}
							
							$('#successHomeEdit').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
 
	
	$('body').on('click','.change-home-container3-image',function(e){
		
		$('#home-container3-image').addClass('hidden');
		
		$('#home-container3-upload').removeClass('hidden');
		$('#home-container3-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-home-container3-image',function(e){
		
		$('#home-container3-upload').addClass('hidden');
		
		$('#home-container3-image').removeClass('hidden');
		$('#home-container3-image').slideDown();
		
	});
	
	function formContainer3Validation(){
		
		var validate = false;
		var error = 0;
		
		var title = $('#container-3-from #title').val();
		var sub_title = $('#container-3-from #sub_title').val();
		
		if(title == ''){
			error++;
			$('#container-3-from #title').css({'border':'1px solid red'});
		}else{
			$('#container-3-from #title').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title == ''){
			error++;
			$('#container-3-from #sub_title').css({'border':'1px solid red'});
		}else{
			$('#container-3-from #sub_title').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
//CONTAINER3------------------------------------------------


//CONTAINER4------------------------------------------------
	$('#table-pricing').dataTable({
		"bProcessing":true,
		"bServerSide":true,
		"sAjaxSource":"/getPricingDetails",
		"aoColumnDefs":[{
			"bSortable":false,"aTargets":[5]
			}]
	});

	var myDropzone4 = new Dropzone('#myDropZone4', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif"
	});
	
	myDropzone4.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-add-plan', function(e){
		
		resetPlanForm();
		$('#add-plan-form #id_plan').val('');
		
		$('#addPlan').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	
	$('body').on('click','.btn-save-plan', function(e){
		
		
		var self = $('#add-plan-form');
		
		var error = 0;
		
		if(!formPlanValidation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/addPlan',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res){
							$('#message_plan').text('You have successfully added a new plan.');
							$('#successPlanMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
	});
	
	
	$('body').on('click','.btn-delete-plan', function(e){
		
		var ID = $(this).data('id');
		$('#delete-plan-form #id_plan').val(ID);
		
		$('#deletePlan').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	$('body').on('click','.btn-delete-confirm-plan', function(e){
			
			var self = $('#delete-plan-form');
			
			$.ajax({
				url:'/deletePlan',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_plan').text('Plan has been deleted.');
							$('#successPlanMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
	
	$('body').on('click','.btn-activation-confirm-plan', function(e){
			
			var self = $('#activation-plan-form');
			
			$.ajax({
				url:'/activationPlan',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_plan').text('Plan status has been changed.');
							$('#successPlanMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
	
	
	$('body').on('click','.btn-activation-plan', function(e){
		
		var ID = $(this).data('id');
		$('#activation-plan-form #id_plan').val(ID);
		
		$('#planActivation').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	
	
	$('body').on('click','.btn-save-container-4', function(e){

		var self = $('#container-4-from');
		var checker = $('#myDropZone4 .dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#home-container4-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone4').css({'border':'1px solid red'});
			}else{
				$('#myDropZone4').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formContainer4Validation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#home-container4-upload #page_edit_id').val(res.id);
							if(classChecker){
								myDropzone4.processQueue();
							}
							
							$('#successHomeEdit').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
 
	
	$('body').on('click','.change-home-container4-image',function(e){
		
		$('#home-container4-image').addClass('hidden');
		
		$('#home-container4-upload').removeClass('hidden');
		$('#home-container4-upload').slideDown();
		
	});
	
	
	$('body').on('click','.cancel-home-container4-image',function(e){
		
		$('#home-container4-upload').addClass('hidden');
		
		$('#home-container4-image').removeClass('hidden');
		$('#home-container4-image').slideDown();
		
	});
	
	$('body').on('click','.btn-edit-plan',function(e){
		resetPlanForm();
		var ID = $(this).data('id');
		
		$.ajax({
				url:'/getPlanDetails',
				data:{id:ID},
				type:'POST',
				dataType:'json',
				success:function(res){
					
					$.each(res,function(i,field){
					
						$('#add-plan-form #id_plan').val(field.id);
						$('#add-plan-form #title').val(field.title);
						$('#add-plan-form #description').val(field.description);
						$('#add-plan-form #pricing').val(field.pricing);
						
					});
					
					$('#addPlan').modal({
						backdrop: 'static',
						keyboard: false
					});
					
				}
				
			});
		
	});

	function formContainer4Validation(){
		
		var validate = false;
		var error = 0;
		
		var title = $('#container-4-from #title').val();
		var sub_title = $('#container-4-from #sub_title').val();
		
		
		if(title == ''){
			error++;
			$('#container-4-from #title').css({'border':'1px solid red'});
		}else{
			$('#container-4-from #title').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title == ''){
			error++;
			$('#container-4-from #sub_title').css({'border':'1px solid red'});
		}else{
			$('#container-4-from #sub_title').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
	
	function formPlanValidation(){
		var validate = false;
		var error = 0;
		
		var title = $('#add-plan-form #title').val();
		var description = $('#add-plan-form #description').val();
		var pricing = $('#add-plan-form #pricing').val();
		
		
		if(title == ''){
			error++;
			$('#add-plan-form #title').css({'border':'1px solid red'});
		}else{
			$('#add-plan-form #title').css({'border':'1px solid #ccc'});
		}
		
		if(description == ''){
			error++;
			$('#add-plan-form #description').css({'border':'1px solid red'});
		}else{
			$('#add-plan-form #description').css({'border':'1px solid #ccc'});
		}
		
		if(pricing == ''){
			error++;
			$('#add-plan-form #pricing').css({'border':'1px solid red'});
		}else{
			$('#add-plan-form #pricing').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
	}
	
	function resetPlanForm(){
		$('#add-plan-form #id_plan').val('');
		$('#add-plan-form #title').val('');
		$('#add-plan-form #description').val('');
		$('#add-plan-form #pricing').val('');
	}
//CONTAINER4------------------------------------------------

//TESTIMONY------------------------------------------------
	$('#table-testimony').dataTable({
		"bProcessing":true,
		"bServerSide":true,
		"sAjaxSource":"/getAllTestimony",
		"aoColumnDefs":[{
			"bSortable":false,"aTargets":[3]
			}]
	});

	var myDropzone5 = new Dropzone('#myDropZone5', {
		url: "/clientPictureUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif"
	});
	
	myDropzone5.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-add-testimony',function(e){
		resetTestimonyForm();
		$('#addTestimony').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	$('body').on('click','.btn-save-testimony', function(e){

		var self = $('#form-testimony-home');
		var checker = $('#myDropZone5 .dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#home-testimony-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone5').css({'border':'1px solid red'});
			}else{
				$('#myDropZone5').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formTestimonyValidation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/addTestimony',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#home-testimony-upload #id_testimony').val(res.id);
							if(classChecker){
								myDropzone5.processQueue();
							}
							$('#message_testimony').text('You have successfully added a new testimony.');
							$('#successTestimonyMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
	
	
	$('body').on('click','.btn-delete-testimony', function(e){
		
		var ID = $(this).data('id');
		$('#delete-testimony-form #id_testimony').val(ID);
		
		$('#deleteTestimony').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	$('body').on('click','.btn-delete-confirm-testimony', function(e){
			
			var self = $('#delete-testimony-form');
			
			$.ajax({
				url:'/deleteTestimony',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_testimony').text('Testimony has been deleted.');
							$('#successTestimonyMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
	
	
	
	$('body').on('click','.btn-edit-testimony',function(e){
		
		var ID = $(this).data('id');
		
		$.ajax({
				url:'/getTestimonyDetails',
				data:{id:ID},
				type:'POST',
				dataType:'json',
				success:function(res){
					
					resetTestimonyForm();
					
					$.each(res,function(i,field){
					
						$('#form-testimony-home #id_testimony').val(field.id);
						$('#form-testimony-home #title').val(field.title);
						$('#form-testimony-home #content').val(field.content);
						$('#form-testimony-home #name').val(field.name);
						$('#form-testimony-home #company').val(field.company);
						$('#form-testimony-home #rating_'+field.rating).prop( 'checked', true );
						$('#client_image').html('<img class="img-responsive" alt="" src="'+field.path+field.file_name+'">');
						
					});
					
					if($('#client_image').children().length == 0){
						$('#home-testimony-image').addClass('hidden');
						$('#home-testimony-upload').removeClass('hidden');
						$('.cancel-home-testimony-image').removeClass('hidden');
					}else{
						$('#home-testimony-image').removeClass('hidden');
						$('#home-testimony-upload').addClass('hidden');
						$('.cancel-home-testimony-image').addClass('hidden');
					}
					
					$('#addTestimony').modal({
						backdrop: 'static',
						keyboard: false
					});
					
				}
				
			});
		
	});
	
	$('body').on('click','.change-home-testimony-image',function(e){
		
		$('#home-testimony-image').addClass('hidden');
		$('#home-testimony-upload').removeClass('hidden');
		$('.cancel-home-testimony-image').removeClass('hidden');
		
	});
	
	
	$('body').on('click','.cancel-home-testimony-image',function(e){
		
		$('#home-testimony-image').removeClass('hidden');
		$('#home-testimony-upload').addClass('hidden');
		$('.cancel-home-testimony-image').addClass('hidden');
		
	});
	
	
	function formTestimonyValidation(){
		
		var validate = false;
		var error = 0;
		
		var title = $('#form-testimony-home #title').val();
		var company = $('#form-testimony-home #company').val();
		var content = $('#form-testimony-home #content').val();
		var name = $('#form-testimony-home #name').val();
		
		if(title == ''){
			error++;
			$('#form-testimony-home #title').css({'border':'1px solid red'});
		}else{
			$('#form-testimony-home #title').css({'border':'1px solid #ccc'});
		}
		
		if(company == ''){
			error++;
			$('#form-testimony-home #company').css({'border':'1px solid red'});
		}else{
			$('#form-testimony-home #company').css({'border':'1px solid #ccc'});
		}
		
		if(content == ''){
			error++;
			$('#form-testimony-home #content').css({'border':'1px solid red'});
		}else{
			$('#form-testimony-home #content').css({'border':'1px solid #ccc'});
		}
		
		if(name == ''){
			error++;
			$('#form-testimony-home #name').css({'border':'1px solid red'});
		}else{
			$('#form-testimony-home #name').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
	
	function resetTestimonyForm(){
		$('#form-testimony-home #id_testimony').val('');
		$('#form-testimony-home #title').val('');
		$('#form-testimony-home #content').val('');
		$('#form-testimony-home #name').val('');
		$('#form-testimony-home #company').val('');
		$("input:radio").removeAttr('checked');
		$('#home-testimony-upload #id_testimony').val('');
		$('#home-testimony-image').addClass('hidden');
		$('#home-testimony-upload').removeClass('hidden');
		$('.cancel-home-testimony-image').addClass('hidden');
	}
//TESTIMONY------------------------------------------------
//SAMPLE FRONT END------------------------------------------------
	$('#table-front-end').dataTable({
		"bProcessing":true,
		"bServerSide":true,
		"sAjaxSource":"/getAllSampleFrontEnd",
		"aoColumnDefs":[{
			"bSortable":false,"aTargets":[2]
			}]
	});
	
	var myDropzone6 = new Dropzone('#myDropZone6', {
		url: "/uploadFrontEndDesign",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 6, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		queuecomplete: function() {
			$('#successFrontEnd').modal({
				backdrop: 'static',
				keyboard: false
			});
		}
	});
	
	$('body').on('click','.btn-add-front-end',function(e){
		
		$('#addSampleFrontEnd').modal({
			backdrop: 'static',
			keyboard: false
		});
		
	});
	
	
	$('body').on('click','.btn-front-end', function(e){
		var checker = $('#myDropZone6 .dz-preview').children().length;

		if(checker > 0){
			myDropzone6.processQueue();					
			
		}

	});
	
	$('body').on('click','.btn-delete-front-end',function(e){
		
		var ID = $(this).data('id');
		$('#delete-front-end-form #id_design').val(ID);
		
		$('#deleteFrontEndDesign').modal({
			backdrop: 'static',
			keyboard: false
		});
		
		
	});
	
	$('body').on('click','.btn-delete-confirm-front-end', function(e){
			
			var self = $('#delete-front-end-form');
			
			$.ajax({
				url:'/deleteFrontEndDesign',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						if(res){
							$('#message_front_end').text('Design has been deleted.');
							$('#successFrontEndMessage').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		
	});
//SAMPLE FRONT END------------------------------------------------


</script>