<script>

//HEADING------------------------------------------------
	var myDropzone = new Dropzone('#myDropZone', {
		url: "/fileUpload",                        
		autoProcessQueue: false, 
		addRemoveLinks: true, //remove button/link
		maxFiles: 1, //max files
		uploadMultiple: true, //multiple upload
		parallelUploads: 10, //for upload all at the same time
		acceptedFiles: "image/jpeg,image/png,image/gif",
	});
	
	myDropzone.on("maxfilesexceeded", function(file)
	{
		this.removeFile(file);
	});
	
	$('body').on('click','.btn-save-heading', function(e){

		var self = $('#form-heading-contact-us');
		var checker = $('.dz-preview').children().length;

		var error = 0;
		var classChecker = $( '#contact-us-heading-image' ).hasClass( 'hidden' );
		
		if(checker == 0 ){
			
			if(classChecker){
				error++;
				$('#myDropZone').css({'border':'1px solid red'});
			}else{
				$('#myDropZone').css({'border':'1px solid #ccc'});
			}
		}
		
		if(!formHeadingValidation()){
			error++;
		}

		if(error == 0){
			
			$.ajax({
				url:'/UpdateFixPage',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res.status){
							$('#contact-us-heading-upload #page_edit_id').val(res.id);
							if(classChecker){
								myDropzone.processQueue();
							}
							
							$('#successContactUsEdit').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
	});
 

	Dropzone.autoDiscover = false;
	
	$('body').on('click','.change-contact-us-heading-image',function(e){
		
		$('#contact-us-heading-image').addClass('hidden');
		
		$('#contact-us-heading-upload').removeClass('hidden');
		$('#contact-us-heading-upload').slideDown();
		
	});
	
	$('body').on('click','.cancel-contact-us-heading-image',function(e){
		
		$('#contact-us-heading-upload').addClass('hidden');
		
		$('#contact-us-heading-image').removeClass('hidden');
		$('#contact-us-heading-image').slideDown();
		
	});
	
	function formHeadingValidation(){
		
		var validate = false;
		var error = 0;
		
		var title_heading = $('#title_heading').val();
		var sub_title_heading = $('#sub_title_heading').val();
		
		if(title_heading == ''){
			error++;
			$('#title_heading').css({'border':'1px solid red'});
		}else{
			$('#title_heading').css({'border':'1px solid #ccc'});
		}
		
		if(sub_title_heading == ''){
			error++;
			$('#sub_title_heading').css({'border':'1px solid red'});
		}else{
			$('#sub_title_heading').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
		
	}
//HEADING------------------------------------------------
//CONTAINER1------------------------------------------------
	
	$('body').on('click','.btn-save-container-1',function(e){

		var self = $('#form-container-1');
		
		if(formContainer1Validation()){
			$.ajax({
				url:'/UpdateContactUsContent',
				data:self.serialize(),
				type:'POST',
				dataType:'json',
				success:function(res){
						
						if(res){
							
							$('#successContactUsEdit').modal({
								backdrop: 'static',
								keyboard: false
							});
						}
						
				}
				
			});
		}
		
		
	});
	
	function formContainer1Validation(){
		
		var validate = false;
		var error = 0 ;
		
		var content = $('#form-container-1 #content').val();
		
		if(content == ''){
			error++;
			$('#form-container-1 #content').css({'border':'1px solid red'});
		}else{
			$('#form-container-1 #content').css({'border':'1px solid #ccc'});
		}
		
		if(error == 0){
			validate = true;
		}
		
		return validate;
	}

//CONTAINER1------------------------------------------------

</script>