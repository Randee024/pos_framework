
<?php
	$headings = array();
	$container1 = array();
	$container2 = array();
	$container3 = array();
	$container4 = array();
	$fileHeading = array();
	$fileContainer1 = array();
	$fileContainer3 = array();
	$fileContainer4 = array();
	$pricingLists = array();
	
	if(!empty($getAllAboutUsEdits)){
		foreach($getAllAboutUsEdits as $key => $value)
		{
			if($value['id_child'] == PageModel::HEADING){
				$headings = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_1){
				$container1 = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_2){
				$container2 = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_3){
				$container3 = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_4){
				$container4 = $value;
			}
		}
		
		if(!empty($headings) && isset($headings['id_file_upload'])){
			$fileHeading = $fileUploadModel->getFileDetails($headings['id_file_upload']);
		}
		
		if(!empty($container1) && isset($container1['id_file_upload'])){
			$fileContainer1 = $fileUploadModel->getFileDetails($container1['id_file_upload']);
		}
		
		if(!empty($container3) && isset($container3['id_file_upload'])){
			$fileContainer3 = $fileUploadModel->getFileDetails($container3['id_file_upload']);
		}
		
		if(!empty($container4) && isset($container4['id_file_upload'])){
			$fileContainer4 = $fileUploadModel->getFileDetails($container4['id_file_upload']);
			
		}
	
	}
	
	
?>


<header class="page-heading-backend">
    <div class="container">
        <div class="modal-content-backend-content animate col-md-12">
            <div class="container1">
                <div class="col-md-12">
                    <a href="#"><strong><i class="glyphicon glyphicon-pencil"></i> Edit Pages</strong></a>
                    <hr>
                    <ol class="breadcrumb">
                        <li><a href="/edit_home">Home</a></li>
                        <li><a href="/edit_services">Services</a></li>
                        <li class="active">About Us</li>
                        <li><a href="/edit_faqs">FAQ's</a></li>
                        <li><a href="/edit_contact_us">Contact Us</a></li>
                    </ol>
                    <!---------------- ABOUT US PANEL------------------->
                    <div class="panel panel-default" id="home">
                        <div class="panel-body">
                            <div class="panel-group" id="accordion2">
                                <!-------------------- HEADING -------------------->
                                <?php
									include 'edit_about_us/heading.php';
								?>
                                <!-------------------- END HEADING -------------------->
                                <!-------------------- CONTAINER 1 -------------------->
                                <?php
									include 'edit_about_us/container_1.php';
								?>
                                <!-------------------- CONTAINER 1 -------------------->
                                <!-------------------- CONTAINER 2 -------------------->
                                <?php
									include 'edit_about_us/container_2.php';
								?>
                                <!-------------------- CONTAINER 2 -------------------->
                                <!-------------------- CONTAINER 3 -------------------->
								<?php
									include 'edit_about_us/container_3.php';
								?>
                                <!-------------------- END CONTAINER 3 -------------------->
                                <!-------------------- CONTAINER 4 -------------------->
                                <?php
									include 'edit_about_us/container_4.php';
								?>
                                <!-------------------- END CONTAINER 4 -------------------->
                                
                            </div><!---------------- END PANEL -------------------->
                        </div><!---------------- END PANEL BODY -------------------->
                    </div><!------------- END ABOUT US PANEL -------------------->


                </div><!----------------- END COLUMN -------------------->
            </div><!------------------ END CONTAINER 1 -------------------->
        </div><!----------------- END MODAL CONTENT ANIMATE -------------------->
    </div><!---------------- END CONTAINER -------------------->
</header><!---------------- END HEADER -------------------->

<?php
	include 'modal/addMap.php';
	include 'modal/addLogo.php';
	include 'modal/deleteMap.php';
	include 'modal/deleteLogo.php';
	include 'modal/mapActivation.php';
	include 'modal/logoActivation.php';
	include 'modal/successAboutUsEdit.php';
	include 'modal/successAboutUsMessage.php';
?>




