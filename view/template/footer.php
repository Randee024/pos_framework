
<script src="./public/jquery.js" type="text/javascript"></script>
<script src="./public/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="./public/bootstrap-toggle.js" type="text/javascript"></script>
<script src='https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js'></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/additional-methods.js"></script>
<script src="./public/validator.min.js" type="text/javascript"></script>
<script src="./public/jquery-ui-1.10.4.custom.js" type="text/javascript"></script>
<script src="./public/jquery.PrintArea.js" type="text/javascript"></script>
<script src="./public/bootstrap/js/clock.js" type="text/javascript"></script>
<script src="./public/datatables.js" type="text/javascript"></script>
<script src="./public/bootstrap/js/tabs1.js" type="text/javascript"></script>
<script src="./bootstrap_plugins/public/Custom JS/vnavbar.js" type="text/javascript"></script>
<script src="./bootstrap_plugins/public/Custom JS/vnavbar3.js" type="text/javascript"></script>
<script src="./bootstrap_plugins/public/Custom JS/dropdown1.js" type="text/javascript"></script>
<script src="./public/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="./public/wow.min.js" type="text/javascript"></script>
<script src="./public/colorpicker-color.js" type="text/javascript"></script>
<script>
              new WOW().init();
</script>
<script>
    $('#myModal').appendTo("body");
    
    
//FOR NAVBAR
// toggle class scroll 
$(window).scroll(function() {
    if($(this).scrollTop() > 400)
    {
        $('.navbar-trans').addClass('afterscroll');
        $('.navbar').addClass('afterscroll');
    } else
    {
        $('.navbar-trans').removeClass('afterscroll');
        $('.navbar').removeClass('afterscroll');
    }  

});
//

// FOR SIDE NAVBAR HIDE/SHOW
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
// FOR SIDE NAVBAR ACTIVE STATE
    $(document).ready(function () {
        // get current URL path and assign 'active' class
        var pathname = window.location.pathname;
        $('.list3 > li > a[href="' + pathname + '"]').parent().addClass('active');
    })
</script>

<script>
// FOR DATEPICKER
    $(function () {
        $('.datepicker').datepicker();
    });
</script>

<script>
// FOR POPOVER
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});
</script>

<script>
// FOR FILE BROWSING
$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
</script>

<script>
// FOR DATATABLES ELEMENTS
$(document).ready(function() {
    $('table.display').DataTable( {
        "paging":   true,
        "ordering": true,
        "info":     false,
        "searching": false
    } );
} );
</script>

<script>
// FOR LIST LINKING
$('.list3').on('click', 'li', function(e) {
    location.href = $(this).children("a").attr("href");          
});
</script>


<footer class="footer hidden-xs">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-6 footerleft ">
          <div class="logofooter"><img src="../../images/5_picture (1).png" alt="" style="width: 70px;" /></div>
          
        <p>E-Tech Business Solutions is an up-and-coming IT developing service that aims to provide its clients with both online and offline systems that fit their requirements without hampering performance.</p>
        <p><i class="fa fa-map-pin"></i> Unit 1904, World Trade Exchange Building<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Juan Luna Street, Binondo, Manila</p>
        <p><i class="fa fa-phone"></i> Phone: +63 2 313 89 00</p>
        <p><i class="fa fa-envelope"></i> E-mail : hr@e-tech.com.ph</p>
        
      </div>
      <div class="col-md-2 col-sm-6 paddingtop-bottom">
        <h6 class="heading7">GENERAL LINKS</h6>
        <ul class="footer-ul">
          <li><a href="#"> Career</a></li>
          <li><a href="#"> Privacy Policy</a></li>
          <li><a href="#"> Terms & Conditions</a></li>
          <li><a href="#"> Client Gateway</a></li>
          <li><a href="#"> Ranking</a></li>
          <li><a href="#"> Case Studies</a></li>
          <li><a href="#"> Frequently Ask Questions</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-sm-6 paddingtop-bottom">
        <h6 class="heading7">LATEST POST</h6>
        <div class="post">
          <p>There are many variations of passages of Lorem Ipsum available,  <span>August 3, 2017</span></p>
          <p>but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. <span>August 5, 2017</span></p>
          <p> in some form, by injected humour, or randomised words which don't look even slightly believable. <span>August 7, 2017</span></p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 paddingtop-bottom">
        <div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="timeline" data-height="300" data-small-header="false" style="margin-bottom:15px;" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
          <div class="fb-xfbml-parse-ignore">
            <blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="copyright">
  <div class="container">
    <div class="col-md-6">
      <p>© 2017 - All Rights Reserved</p>
    </div>
    <div class="col-md-6">
      <ul class="bottom_ul">
        <li><a href="#">e-tech.com.ph</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="#">Faq's</a></li>
        <li><a href="#">Contact us</a></li>
        <li><a href="#">Site Map</a></li>
      </ul>
    </div>
  </div>
</div>
</footer>
<!--footer start from here-->

<footer class="footer visible-xs">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-6 footerleft ">
          <div class="logofooter"><img src="../../images/5_picture (1).png" alt="" style="width: 70px;" /></div>
        <p>E-Tech Business Solutions is an up-and-coming IT developing service that aims to provide its clients with both online and offline systems that fit their requirements without hampering performance.</p>
        <p><i class="fa fa-map-pin"></i> Unit 1904, World Trade Exchange Building<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Juan Luna Street, Binondo, Manila</p>
        <p><i class="fa fa-phone"></i> Phone: +63 2 313 89 00</p>
        <p><i class="fa fa-envelope"></i> E-mail : hr@e-tech.com.ph</p>
      </div>
    </div>
  </div>
    <div class="copyright">
  <div class="container">
    <div class="col-md-6">
      <p>© 2017 - All Rights Reserved</p>
    </div>
  </div>
</div>
</footer>


</body>
</html>
