<?php 

include 'modal/logout.php';
?>
<script src="./public/jquery.js" type="text/javascript"></script>
<script src="./public/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="./public/bootstrap-toggle.js" type="text/javascript"></script>
<!--<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>-->
<script src='https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js'></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/additional-methods.js"></script>
<script src="./public/validator.min.js" type="text/javascript"></script>
<script src="./public/jquery-ui-1.10.4.custom.js" type="text/javascript"></script>
<script src="./public/jquery.PrintArea.js" type="text/javascript"></script>
<script src="./public/bootstrap/js/clock.js" type="text/javascript"></script>
<script src="./public/datatables.js" type="text/javascript"></script>
<script src="./public/bootstrap/js/tabs1.js" type="text/javascript"></script>
<script src="./bootstrap_plugins/public/Custom JS/vnavbar.js" type="text/javascript"></script>
<script src="./bootstrap_plugins/public/Custom JS/vnavbar3.js" type="text/javascript"></script>
<script src="./bootstrap_plugins/public/Custom JS/dropdown1.js" type="text/javascript"></script>
<script src="./public/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="./public/wow.min.js" type="text/javascript"></script>
<script src="./public/js/tinymce.min.js"></script>

<script>
	$('body').on('click','.btn-logout',function(e){
			$('#logout').modal({
				backdrop: 'static',
				keyboard: false
			});
		
	});
</script>
<script>
    tinymce.init({ selector:'textarea1' });
</script>
  
<script>
//FOR NAVBAR
// toggle class scroll 
$(window).scroll(function() {
    if($(this).scrollTop() > 100)
    {
        $('.navbar-trans').addClass('afterscroll');
        $('.navbar').addClass('afterscroll');
    } else
    {
        $('.navbar-trans').removeClass('afterscroll');
        $('.navbar').removeClass('afterscroll');
    }  

});
//

// FOR SIDE NAVBAR HIDE/SHOW
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
// FOR SIDE NAVBAR ACTIVE STATE
    $(document).ready(function () {
        // get current URL path and assign 'active' class
        var pathname = window.location.pathname;
        $('.list3 > li > a[href="' + pathname + '"]').parent().addClass('active');
    })
</script>

<script>
// FOR DATEPICKER
    $(function () {
        $('.datepicker').datepicker();
    });
</script>

<script>
// FOR POPOVER
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});
</script>

<script>
// FOR FILE BROWSING
$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
</script>

<script>
// FOR DATATABLES ELEMENTS
$(document).ready(function() {
    $('table.display').DataTable( {
        "paging":   true,
        "ordering": true,
        "info":     false,
        "searching": false
    } );
} );
</script>

<script>
// FOR LIST LINKING
$('.list3').on('click', 'li', function(e) {
    location.href = $(this).children("a").attr("href");          
});
</script>

<script>
$(function(){
$('.clickable').on('click',function(){
    var effect = $(this).data('effect');
        $(this).closest('.panel')[effect]();
	})
})
</script>

<!--<footer class="footer">
</footer>-->
<!--footer start from here-->

<div class="copyright">
  <div class="container">
    <div class="col-md-6">
      <p>© 2017 - All Rights Reserved</p>
    </div>
  </div>
</div>
</body>
</html>
