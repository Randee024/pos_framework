<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title></title>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />


		<link rel="stylesheet" href="./public/css/dropzone.css" type="text/css"/>
        <link href="./public/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="./public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="./public/bootstrap/css/clock.css" rel="stylesheet" type="text/css"/>
        <link href="./public/bootstrap/css/tabs1.css" rel="stylesheet" type="text/css"/>
        <link href="./public/bootstrap/css/toggle.css" rel="stylesheet" type="text/css"/>
        <link href="./public/DataTables-1.10.15/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/tabs1.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/upload_file.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/login3.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/navbar.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/button2.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/page-heading.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/carousel1.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/animate.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/footer1.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/table_price.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/collapsible1.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/login1.css" rel="stylesheet" type="text/css"/>
        <link href="./public/Custom CSS/backend.css" rel="stylesheet" type="text/css"/>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.css" rel="stylesheet">
        <link href="./public/Custom CSS/styles.css" rel="stylesheet" type="text/css"/>
        <script src="./public/js/dropzone.js"></script>
    </head>

    <body>

        <nav class="navbar navbar-default navbar-doublerow navbar-trans navbar-fixed-top">
            <!-- top nav -->
            <nav class="navbar navbar-top hidden-xs">
                <div class="container">
                    <!-- left nav top -->
                    <ul class="nav navbar-nav pull-left">
                        <li><a href="/backend"><span class="text-white">BACKEND PAGE</span></a></li>
                    </ul>
                    <!-- right nav top -->
                    <ul class="nav navbar-nav pull-right">
                        <li><a href="#" class="text-white"><div id="timediv"></div></a></li>
                        <li><a href="javascript:void(0)" class="text-white btn-logout">Logout</a></li> 
                        
                    </ul>
                    
                </div>
                <div class="dividline light-grey"></div>
            </nav>
            <!-- down nav -->
            <nav class="navbar navbar-down">
                <div class="container">
                    <div class="flex-container">  
                        <div class="navbar-header flex-item">
                            <div class="navbar-brand" href="/backend">POS BACKEND</div>
                        </div>
                        <ul class="nav navbar-nav flex-item hidden-xs pull-right">
                            <li><a href="#" class="">Media</a></li>
                            <li><a href="/edit_home" class="">Edit Pages</a></li> 
                            <li><a href="#" class="">Appearance</a></li> 
                            <li><a href="#" class="">Users</a></li>
                            <li><a href="#" class="">Tools</a></li>
                            <li><a href="#" class="">Settings</a></li> 
                        </ul>
                        <!-- dropdown only moblie -->
                        <div class="dropdown visible-xs pull-right">
                            <button class="btn btn-default dropdown-toggle " type="button" id="dropdownmenu" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-align-justify"></span> 
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="">Media</a></li>
                            <li><a href="/edit_page" class="">Edit Pages</a></li> 
                            <li><a href="#" class="">Appearance</a></li> 
                            <li><a href="#" class="">Users</a></li>
                            <li><a href="#" class="">Tools</a></li>
                            <li><a href="#" class="">Settings</a></li> 
                            </ul>
                        </div>
                    </div>  
                </div>
            </nav>
        </nav>

        

       







