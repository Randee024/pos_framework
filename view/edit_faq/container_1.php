<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion3" href="#collapse2">
				Container 1</a>
		</h4>
	</div>
	<div id="collapse2" class="panel-collapse collapse">
		<div class="panel-body"><br>
			<div class="row">
				<label>FAQ's Lists</label><br>
                <a href="javascript:void(0)" class="btn btn-info pull-left btn-add-faq"><i class="glyphicon glyphicon-plus"></i>Add FAQ's</a>
                
                
				<div class="col-md-12">
				
					<table id="table-faq" class="table table-hover table-bordered" width="100%">
						<colgroup>
							<col width="20%">
							<col width="50%">
							<col width="20%">
							<col width="10%">
						</colgroup>
						<thead>
							<tr>
								<th>Title</th>
								<th>Content</th>
								<th>Category</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>