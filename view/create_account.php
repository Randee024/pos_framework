<header class="page-heading-backend">
    <!--<div class="container-fluid">
        <div class="row">
            <form class="modal-content-newaccount animate" id="create-account-form"><br>
                <legend>Create Account</legend>
                <div class="row">
					<input id="userID"  type="hidden" name="userID">
					<div class="col-md-12">
						<label><b>First Name</b></label>
						<input id="first_name"  type="text" placeholder="Enter First Name" name="first_name" value="" required>
                     </div>
					 <div class="col-md-12">
						<label><b>Last Name</b></label>
						<input id="last_name"  type="text" placeholder="Enter Last Name" name="last_name" value="" required>
                    </div>
					<div class="col-md-12">
						<label><b>E-Mail</b></label>
						<input id="email"  type="text" placeholder="Enter E-Mail" name="email" value="" required>
					</div>
					<div class="col-md-12">
						<label><b>Username</b></label>
						<input id="username"  type="text" placeholder="Enter Username" name="username" value="" required>
                    </div>
					<div class="col-md-12">
						<label><b>Password</b></label>
						<input id="password" type="password" placeholder="Enter Password" name="password" value=""  required>
                    </div>
					<div class="col-md-12">
						<label><b>Confirm Password</b></label>
						<input id="confirm_password" type="password" placeholder="Enter Password" name="confirm_password" value=""  required>
					</div>
                </div>

                <div class="container1" style="background-color:#f1f1f1">
                    <button type="button" class="btn-create-account">Submit</button>
                </div>
            </form>
        </div>
    </div>-->
	<div class="container-fluid">
		<div class="modal-content-newaccount animate">

				<div class="row form-group">
					<div class="col-xs-12">
						<ul class="nav nav-pills nav-justified thumbnail setup-panel">
							<li id="step-1-indicator" class="active"><a href="#step-1">
								<h4 class="list-group-item-heading">Step 1</h4>
								<p class="list-group-item-text">User Information</p>
							</a></li>
							<li id="step-2-indicator" class="disabled"><a href="#step-2">
								<h4 class="list-group-item-heading">Step 2</h4>
								<p class="list-group-item-text">Credential</p>
							</a></li>
							<li class="disabled"><a href="#step-3">
								<h4 class="list-group-item-heading">Step 3</h4>
								<p class="list-group-item-text">Upload Avatar</p>
							</a></li>
						</ul>
					</div>
				</div>
				<form id="create-account-form">
					<div class="row setup-content" id="step-1">
						<div class="col-xs-12">
							<div class="col-md-12 well ">
									<div class="row">
										<div class="col-md-12">
											<label><b>First Name</b></label>
											<input id="first_name"  type="text" placeholder="Enter First Name" name="first_name">
										 </div>
										 <div class="col-md-12">
											<label><b>Last Name</b></label>
											<input id="last_name"  type="text" placeholder="Enter Last Name" name="last_name">
										</div>
										<div class="col-md-12">
											<label><b>E-Mail</b></label>
											<input id="email"  type="text" placeholder="Enter E-Mail" name="email">
										</div>
									</div>
								<button type="button" class="btn-next-step-2">NEXT</button>
							</div>
						</div>
					</div>
					<div class="row setup-content hidden" id="step-2">
						<div class="col-xs-12">
							<div class="col-md-12 well">
									<div class="row">
										<div class="col-md-12">
											<label><b>Username</b></label>
											<input id="username"  type="text" placeholder="Enter Username" name="username">
										</div>
										<div class="col-md-12">
											<label><b>Password</b></label>
											<input id="password" type="password" placeholder="Enter Password" name="password">
										</div>
										<div class="col-md-12">
											<label><b>Confirm Password</b></label>
											<input id="confirm_password" type="password" placeholder="Enter Password" name="confirm_password">
										</div>
									</div>

									<div class="container1" style="background-color:#f1f1f1">
										<button type="button" class="btn-next-step-3">NEXT</button>
									</div>
								
							</div>
						</div>
					</div>
				</form>
				<div class="row setup-content hidden" id="step-3">
					<div class="col-xs-12">
						<div class="col-md-12 well">
							<form id="myDropZone" class="dropzone heading-home col-md-4 col-md-offset-4" method="POST">
								<input type="hidden" id="account_id" name="account_id" value="" />
								<input type="hidden" name="new_path" value="account" />
								
							</form>
							</br>
							</br>
							</br>
							<div class="container1" style="background-color:#f1f1f1">
									<button type="button" class="btn-create-account">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
	  
		</div>
	</div>
</header>

<?php
	include 'modal/successCreateAccount.php';
	include 'modal/errorCreateAccount.php';
?>
