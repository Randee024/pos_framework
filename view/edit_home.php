<?php
	$headings = array();
	$container1 = array();
	$container2 = array();
	$container3 = array();
	$container4 = array();
	$testimonials = array();
	$fileHeading = array();
	$fileContainer2 = array();
	$fileContainer3 = array();
	$fileContainer4 = array();
	$fileTestimony = array();
	
	if(!empty($getAllHomeEdits)){
		foreach($getAllHomeEdits as $key => $value)
		{
			if($value['id_child'] == PageModel::HEADING){
				$headings = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_1){
				$container1 = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_2){
				$container2 = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_3){
				$container3 = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_4){
				$container4 = $value;
			}else if($value['id_child'] == PageModel::TESTIMONY){
				$testimonials = $value;
			}
		}
		
		if(!empty($headings) && isset($headings['id_file_upload'])){
			$fileHeading = $fileUploadModel->getFileDetails($headings['id_file_upload']);
		}
		
		if(!empty($container2) && isset($container2['id_file_upload'])){
			$fileContainer2 = $fileUploadModel->getFileDetails($container2['id_file_upload']);
		}
		
		if(!empty($container3) && isset($container3['id_file_upload'])){
			$fileContainer3 = $fileUploadModel->getFileDetails($container3['id_file_upload']);
		}
		
		if(!empty($container4) && isset($container4['id_file_upload'])){
			$fileContainer4 = $fileUploadModel->getFileDetails($container4['id_file_upload']);
			
		}
		
		if(!empty($testimonials) && isset($testimonials['id_file_upload'])){
			$fileTestimony = $fileUploadModel->getFileDetails($testimonials['id_file_upload']);
			
		}
		
	}
	
	
?>

<header class="page-heading-backend">
    <div class="container">
        <div class="modal-content-backend-content animate col-md-12">
            <div class="container1">
                <div class="col-md-12">
                    <a href="#"><strong><i class="glyphicon glyphicon-pencil"></i> Edit Pages</strong></a>
                    <hr>
                    <ol class="breadcrumb">
                        <li class="active">Home</li>
                        <li><a href="/edit_services">Services</a></li>
                        <li><a href="/edit_about_us">About Us</a></li>
                        <li><a href="/edit_faqs">FAQ's</a></li>
                        <li><a href="/edit_contact_us">Contact Us</a></li>
                    </ol>
                    <div class="row">
                        <!-- center left-->
                        <div class="col-md-12">

                            <!---------------- HOME ------------------->
                            <div class="panel panel-default" id="home">
                                            <div class="panel-body">
                                                <!------------------ EDIT CONTAINER HOME ---------------------->
                                                <div class="container-fluid">
                                                    <div class="panel-group" id="accordion">
                                                        <!--------------------- EDIT HEADING --------------------------->
                                                        <?php
															include 'edit_home/heading.php';
                                                        ?>
                                                        <!--------------------- END EDIT HEADING --------------------------->
                                                        <!--------------------- EDIT CONTAINER 1 --------------------------->
                                                        <?php
															include 'edit_home/container_1.php';
                                                        ?>
                                                        <!------------------------- END EDIT CONTAINER 1 --------------------------->
                                                        <!-------------------------- EDIT CONTAINER 2 --------------------------->
                                                        <?php
															include 'edit_home/container_2.php';
                                                        ?>
                                                        <!--------------------- END EDIT CONTAINER 2 --------------------------->
                                                        <!--------------------- EDIT CONTAINER 3 --------------------------->
														<?php
															include 'edit_home/container_3.php';
                                                        ?>
                                                        <!--------------------- END EDIT CONTAINER 3 --------------------------->
                                                        <!--------------------- EDIT CONTAINER 4 --------------------------->
														<?php
															include 'edit_home/container_4.php';
                                                        ?>
                                                        <!--------------------- END EDIT CONTAINER 4 --------------------------->
                                                        <!--------------------- EDIT MODAL TESTIMONIES --------------------------->
                                                        <?php
															include 'edit_home/testimony.php';
                                                        ?>
                                                        <!--------------------- END EDIT MODAL TESTIMONIES ----------------------->
                                                        <!--------------------- EDIT EDIT SAMPLE FRONT END --------------------------->
                                                        <?php
															include 'edit_home/sample_front_end.php';
                                                        ?>
                                                        <!--------------------- END EDIT SAMPLE FRONT END --------------------------->
                                                    </div>
                                                </div>
                                                <!------------------ END EDIT CONTAINER ---------------------->
                                            </div><!--/panel content-->
                                    <!------------------------- END HOME  ----------------------------->
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                </div>
            </div>
        </div>
    </div>
</header>
<?php
	include 'modal/addPlan.php';
	include 'modal/addSampleFrontEnd.php';
	include 'modal/deletePlan.php';
	include 'modal/deleteTestimony.php';
	include 'modal/deleteFrontEndDesign.php';
	include 'modal/planActivation.php';
	include 'modal/addTestimony.php';
	include 'modal/successHomeEdit.php';
	include 'modal/successPlanMessage.php';
	include 'modal/successTestimonyMessage.php';
	include 'modal/successFrontEndMessage.php';
	include 'modal/successFrontEnd.php';
?>




