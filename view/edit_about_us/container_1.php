<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion2" href="#aboutus_container1">Container 1</a>
		</h4>
	</div>
	<div id="aboutus_container1" class="panel-collapse collapse">
		<div class="panel-body"><br>
			<form class="form form-vertical" id="container-1-from">
				<input type="hidden" name="id_edit" value="<?php echo isset($container1['id'])? $container1['id']: ''; ?>" />
				<input type="hidden" name="id_page" value="<?php echo PageModel::PAGE_ABOUT_US; ?>" />
				<input type="hidden" name="id_child" value="<?php echo PageModel::CONTAINER_1; ?>" />
				<div class="control-group">
					<label>Title</label>
					<div class="controls">
						<input type="text" name="title" id="title" class="form-control" placeholder="Enter Title" value="<?php echo (isset($container1['title']))? $container1['title'] :'Lorem Ipsum'; ?>">
					</div>
				</div>
				<div class="control-group">
					<label>Sub-Title</label>
					<div class="controls">
						<textarea name="sub_title" id="sub_title" class="form-control" rows="4"><?php echo (isset($container1['sub_title']))? $container1['sub_title'] :'Lorem Ipsum'; ?></textarea>
					</div>
				</div>
			</form>
			</br>
			<div class="row">
				<div class="col-md-3">
					<div id="about-us-container1-upload" <?php echo isset($container1['id_file_upload'])? 'class="hidden"':''; ?> >
						<form id="myDropZone2" class="dropzone container1-about-us" method="POST">
							<input type="hidden" id="page_edit_id" name="page_edit_id" value="" />
							<input type="hidden" name="new_path" value="aboutus" />
							
						</form>
						<?php if(isset($container1['id_file_upload'])){ ?>
								</br>
								<a href="javascript:void(0)" class="btn btn-default cancel-about-us-container1-image">Cancel</a>
						<?php } ?>
					</div>

					<div id="about-us-container1-image" <?php echo isset($container1['id_file_upload'])? '':'class="hidden"'; ?>>
						<img class="img-responsive" alt="" src="<?php echo $fileContainer1['path'].$fileContainer1['file_name']; ?>">
						</br>
						<a href="javascript:void(0)" class="btn btn-danger change-about-us-container1-image">Change background image</a>
					</div>

				</div>
			</div>
			<div class="control-group">
				<label></label>
				<div class="controls">
					<a href="javascript:void(0)" class="btn btn-success col-md-3 pull-right btn-save-container-1">
						Save     
					</a>
				</div>
			</div>
		</div>
	</div>
</div>