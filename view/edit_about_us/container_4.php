<div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#aboutus_container4">Container 4</a>
                                        </h4>
                                    </div>
                                    <div id="aboutus_container4" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <br>
                                            <div class="row">
                                                <form id="form-container-4" class="form form-vertical">
														<input type="hidden" name="id_edit" value="<?php echo isset($container4['id'])? $container4['id']: ''; ?>" />
														<input type="hidden" name="id_page" value="<?php echo PageModel::PAGE_ABOUT_US; ?>" />
														<input type="hidden" name="id_child" value="<?php echo PageModel::CONTAINER_4; ?>" />
                                                    <div class="control-group">
                                                        <label>Title</label>
                                                        <div class="controls">
                                                            <input type="text" id="title" name="title" class="form-control" placeholder="Enter Name" value="<?php echo isset($container4['title'])? $container4['title']: 'Trusted by'; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label></label>
                                                        <div class="controls">
                                                            <a href="javascript:void(0)" class="btn btn-success col-md-3 pull-right btn-save-container-4">
                                                                Save     
                                                            </a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div><br>
                                            <div class="row">
												<br>
													<label>Partner Logo Lists</label><br>
													<a href="javascript:void(0)" class="btn btn-info pull-left btn-add-logo"><i class="glyphicon glyphicon-plus"></i>Add Partner Logo</a>
												
                
                                                <div class="col-md-12">
													<table id="table-container-4" class="table table-hover table-bordered" width="100%">
														<colgroup>
															<col width="20%">
															<col width="40%">
															<col width="20%">
															<col width="20%">
														</colgroup>
														<thead>
															<tr>
																<th>ID</th>
																<th>File Name</th>
																<th>Status</th>
																<th>Actions</th>
															</tr>
														</thead>
													</table>
												</div>
                                            </div><!-------------------- END ROW -------------------->
                                        </div><!-------------------- END PANEL BODY -------------------->
                                    </div><!-------------------- END CONTAINER 4 -------------------->
                                </div><!------------------ END PANEL BODY -------------------->