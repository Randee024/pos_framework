

<div class="panel panel-default">
	 <div class="panel-container3">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion2" href="#aboutus_container3">Container 3</a>
		</h4>
	</div>
	<div id="aboutus_container3" class="panel-collapse collapse">
		<div class="panel-body"><br>
			<form class="form form-vertical" id="form-container-3">
				<input type="hidden" name="id_edit" value="<?php echo isset($container3['id'])? $container3['id']: ''; ?>" />
				<input type="hidden" name="id_page" value="<?php echo PageModel::PAGE_ABOUT_US; ?>" />
				<input type="hidden" name="id_child" value="<?php echo PageModel::CONTAINER_3; ?>" />
				<div class="control-group">
					<label>Title</label>
					<div class="controls">
						<input type="text" name="title" id="title" class="form-control" placeholder="Enter Title" value="<?php echo (isset($container3['title']))? $container3['title']:'';?>">
					</div>
				</div>
				<div class="control-group">
					<label>Sub-Title</label>
					<div class="controls">
						<textarea name="sub_title" id="sub_title" class="form-control" rows="4"><?php echo (isset($container3['title']))? $container3['sub_title']:'';?></textarea>
					</div>
				</div>
			</form>
			</br>
			<div class="row">
				<div class="col-md-3">
					
						<div id="container-3-upload" <?php echo isset($container3['id_file_upload'])? 'class="hidden"':''; ?> >
							<form id="myDropZone3" class="dropzone container-3" method="POST">
								<input type="hidden" id="page_edit_id" name="page_edit_id" value="" />
								<input type="hidden" name="new_path" value="aboutus" />
								
							</form>
							<?php if(isset($container3['id_file_upload'])){ ?>
									</br>
									<a href="javascript:void(0)" class="btn btn-danger cancel-container-3-image">Cancel</a>
							<?php } ?>
						</div>

						<div id="container-3-image" <?php echo isset($container3['id_file_upload'])? '':'class="hidden"'; ?>>
							<img class="img-responsive" alt="" src="<?php echo $fileContainer3['path'].$fileContainer3['file_name']; ?>">
							</br>
							<a href="javascript:void(0)" class="btn btn-info change-container-3-image">Change background image</a>
						</div>

				</div>
			</div>
			<div class="control-group">
				<label></label>
				<div class="controls">
					<a href="javascript:void(0)" class="btn btn-success col-md-3 pull-right btn-save-container-3">
						Save     
					</a>
				</div>
			</div>
		</div>
	</div>
</div>