<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion2" href="#aboutus_container2">Container 2</a>
		</h4>
	</div>
	<div id="aboutus_container2" class="panel-collapse collapse">
		<div class="panel-body">
			<br>
			<div class="row">
				<br>
                    <label>Map Lists</label><br>
                    <a href="javascript:void(0)" class="btn btn-info pull-left btn-add-map"><i class="glyphicon glyphicon-plus"></i>Add Map</a>
                
                
				<div class="col-md-12">
					<table id="table-map" class="table table-hover table-bordered" width="100%">
						<colgroup>
							<col width="20%">
							<col width="20%">
							<col width="20%">
							<col width="20%">
							<col width="20%">
						</colgroup>
						<thead>
							<tr>
								<th>ID</th>
								<th>Latitude</th>
								<th>Longitude</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>