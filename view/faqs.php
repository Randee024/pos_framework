<?php
	$headings = array();
	$fileHeading = array();
	
	
	if(!empty($pageDetails)){
		foreach($pageDetails as $key => $value)
		{
			if($value['id_child'] == PageModel::HEADING){
				$headings = $value;
			}
		}
		
		if(!empty($headings) && isset($headings['id_file_upload'])){
			$fileHeading = $fileModel->getFileDetails($headings['id_file_upload']);
		}
	}
	
if(!empty($fileHeading)){ ?>
	<header class="page-heading-faqs" style="background-image: url('<?php echo $fileHeading['path'].$fileHeading['file_name']; ?>');">
<?php  }else{ ?>
	<header class="page-heading-faqs">
<?php  } ?>
    <div class="container-fluid padding-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            <p>
                            <h1 class="wow fadeIn"><?php echo (isset($headings['title']))? $headings['title']: 'FAQs'; ?></h1>
                            </p>
                            <br><br>
                            <p>
                            <h5><?php echo (isset($headings['sub_title']))? $headings['sub_title']: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'; ?></h5>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="container">
    <div class="panel-group" id="accordion1"><br>
        <legend>Administrator Settings</legend>
		
		<?php 
			if(!empty($faqAdmin)){
				
					foreach($faqAdmin as $key=>$value){
						
						if(!empty($value['id_file_upload'])){
							echo '
								<div class="panel panel-default">
									<div class="panel-heading">
										<h5 class="panel-title">
											<div class="accordion-toggle">'.$value['title'].'</div>
										</h5>
									</div>
									<div id="accordion1_1" class="panel-collapse in">
										<div class="panel-body">
											<div class="col-md-3">
												<img src="'.$value['path'].$value['file_name'].'" alt="" style="width: 100%; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"/>
											</div>
											<div class="col-md-9">
												<p>'.$value['content'].'</p>
											</div>
										</div>
									</div>
								</div>
							';
							
						}else{
							echo '
								<div class="panel panel-default">
									<div class="panel-heading">
										<h5 class="panel-title">
											<div class="accordion-toggle">'.$value['title'].'</div>
										</h5>
									</div>
									<div id="accordion1_1" class="panel-collapse in">
										<div class="panel-body">
											<p>'.$value['content'].'</p>
										</div>
									</div>
								</div>
							';
						}
					}
				
				
			}else{
		?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <div class="accordion-toggle">Lorem ipsum dolor sit amet</div>
                </h5>
            </div>
            <div id="accordion1_1" class="panel-collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore. <a href="/faqs_tutorial">Read more</a></p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse">Consectetur adipisicing elit</a>
                </h5>
            </div>
            <div id="accordion1_2" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle">Augue assum anteposuerit dolore</a>
                </h5>
            </div>
            <div id="accordion1_3" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="col-md-3">
                    <img src="../images/login_panel.jpg" alt="" style="width: 100%; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"/>
                    </div>
                    <div class="col-md-9">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.exercitation ullamco sed eiusmod tempor ut labore et dolore.exercitation ullamco sed eiusmod tempor 
                        ut labore et dolore.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
    </div>
</div>

<div class="container">
    <div class="panel-group">
        <legend>Customer Support</legend>
		<?php 
			if(!empty($faqCustomer)){
				
					foreach($faqCustomer as $key=>$value){
						
						if(!empty($value['id_file_upload'])){
							echo '
								<div class="panel panel-default">
									<div class="panel-heading">
										<h5 class="panel-title">
											<div class="accordion-toggle">'.$value['title'].'</div>
										</h5>
									</div>
									<div id="accordion1_1" class="panel-collapse in">
										<div class="panel-body">
											<div class="col-md-3">
												<img src="'.$value['path'].$value['file_name'].'" alt="" style="width: 100%; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"/>
											</div>
											<div class="col-md-9">
												<p>'.$value['content'].'</p>
											</div>
										</div>
									</div>
								</div>
							';
							
						}else{
							echo '
								<div class="panel panel-default">
									<div class="panel-heading">
										<h5 class="panel-title">
											<div class="accordion-toggle">'.$value['title'].'</div>
										</h5>
									</div>
									<div id="accordion1_1" class="panel-collapse in">
										<div class="panel-body">
											<p>'.$value['content'].'</p>
										</div>
									</div>
								</div>
							';
						}
					}
				
				
			}else{
		?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle">Lorem ipsum dolor sit amet</a>
                </h5>
            </div>
            <div id="accordion2_1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle">Consectetur adipisicing elit</a>
                </h5>
            </div>
            <div id="accordion2_2" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle">Augue assum anteposuerit dolore</a>
                </h5>
            </div>
            <div id="accordion2_3" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle">Sollemnes in futurum</a>
                </h5>
            </div>
            <div id="accordion2_4" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle">Nostrud Tempor veniam</a>
                </h5>
            </div>
            <div id="accordion2_5" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle">Ut enem magana sed dolore</a>
                </h5>
            </div>
            <div id="accordion2_6" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                </div>
            </div>
        </div>
		<?php } ?>
    </div>
</div>

<div class="container">
    <div class="panel-group">
        <legend>Contract and Policy</legend>
		<?php 
			if(!empty($faqContract)){
				
					foreach($faqContract as $key=>$value){
						
						if(!empty($value['id_file_upload'])){
							echo '
								<div class="panel panel-default">
									<div class="panel-heading">
										<h5 class="panel-title">
											<div class="accordion-toggle">'.$value['title'].'</div>
										</h5>
									</div>
									<div id="accordion1_1" class="panel-collapse in">
										<div class="panel-body">
											<div class="col-md-3">
												<img src="'.$value['path'].$value['file_name'].'" alt="" style="width: 100%; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"/>
											</div>
											<div class="col-md-9">
												<p>'.$value['content'].'</p>
											</div>
										</div>
									</div>
								</div>
							';
							
						}else{
							echo '
								<div class="panel panel-default">
									<div class="panel-heading">
										<h5 class="panel-title">
											<div class="accordion-toggle">'.$value['title'].'</div>
										</h5>
									</div>
									<div id="accordion1_1" class="panel-collapse in">
										<div class="panel-body">
											<p>'.$value['content'].'</p>
										</div>
									</div>
								</div>
							';
						}
					}
				
				
			}else{
		?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle">Lorem ipsum dolor sit amet</a>
                </h5>
            </div>
            <div id="accordion3_1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle">Consectetur adipisicing elit</a>
                </h5>
            </div>
            <div id="accordion3_2" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <a class="accordion-toggle">Augue assum anteposuerit dolore</a>
                </h5>
            </div>
            <div id="accordion3_3" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                </div>
            </div>
        </div>
		<?php } ?>
    </div>
</div>
<br>