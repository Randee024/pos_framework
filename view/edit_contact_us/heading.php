

<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion4" href="#contactus_heading">Heading</a>
		</h4>
	</div>
	<div id="contactus_heading" class="panel-collapse collapse in">
		<div class="panel-body"><br>
			<form class="form form-vertical" id="form-heading-contact-us">
				<input type="hidden" name="id_edit" value="<?php echo isset($headings['id'])? $headings['id']: ''; ?>" />
				<input type="hidden" name="id_page" value="<?php echo PageModel::PAGE_CONTACT_US; ?>" />
				<input type="hidden" name="id_child" value="<?php echo PageModel::HEADING; ?>" />
				<div class="control-group">
					<label>Title</label>
					<div class="controls">
						<input type="text" name="title" id="title_heading" class="form-control" placeholder="Enter Title" value="<?php echo (isset($headings['title']))? $headings['title']:'';?>">
					</div>
				</div>
				<div class="control-group">
					<label>Sub-Title</label>
					<div class="controls">
						<textarea name="sub_title" id="sub_title_heading" class="form-control" rows="4"><?php echo (isset($headings['title']))? $headings['sub_title']:'';?></textarea>
					</div>
				</div>
			</form>
			</br>
			<div class="row">
				<div class="col-md-3">
					
						<div id="contact-us-heading-upload" <?php echo isset($headings['id_file_upload'])? 'class="hidden"':''; ?> >
							<form id="myDropZone" class="dropzone heading-contact-us" method="POST">
								<input type="hidden" id="page_edit_id" name="page_edit_id" value="" />
								<input type="hidden" name="new_path" value="contactus" />
								
							</form>
							<?php if(isset($headings['id_file_upload'])){ ?>
									</br>
									<a href="javascript:void(0)" class="btn btn-danger cancel-contact-us-heading-image">Cancel</a>
							<?php } ?>
						</div>

						<div id="contact-us-heading-image" <?php echo isset($headings['id_file_upload'])? '':'class="hidden"'; ?>>
							<img class="img-responsive" alt="" src="<?php echo $fileHeading['path'].$fileHeading['file_name']; ?>">
							</br>
							<a href="javascript:void(0)" class="btn btn-info change-contact-us-heading-image">Change background image</a>
						</div>

				</div>
			</div>
			<div class="control-group">
				<label></label>
				<div class="controls">
					<a href="javascript:void(0)" class="btn btn-success col-md-3 pull-right btn-save-heading">
						Save     
					</a>
				</div>
			</div>
		</div>
	</div>
</div>