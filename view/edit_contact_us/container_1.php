<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion4" href="#contactus_container1">Container 1</a>
		</h4>
	</div>
	<div id="contactus_container1" class="panel-collapse collapse">
		<div class="panel-body"><br>
			<form class="form form-vertical" id="form-container-1">
				<input type="hidden" name="id_edit" value="<?php echo isset($container1['id'])? $container1['id']: ''; ?>" />
				<input type="hidden" name="id_page" value="<?php echo PageModel::PAGE_CONTACT_US; ?>" />
				<input type="hidden" name="id_child" value="<?php echo PageModel::CONTAINER_1; ?>" />
				<div class="control-group">
					<label>Content</label>
					<div class="controls">
						<textarea name="content" id="content" class="form-control" rows="4"><?php echo isset($container1['sub_title'])? $container1['sub_title']: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'; ?></textarea>
					</div>
				</div>
				<div class="control-group">
					<label></label>
					<div class="controls">
						<a href="javascript:void(0)" class="btn btn-success col-md-3 pull-right btn-save-container-1">
							Save     
						</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>