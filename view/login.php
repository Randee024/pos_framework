<header class="page-heading-backend">
	<div class="container-fluid">
		<div class="row">
			<form class="modal-content animate" id="login-form" class="form-signin">
				<div class="imgcontainer">
					<img src="./images/img_avatar2.png" alt="Avatar" class="avatar">
				</div>

				<div class="container1">
					<label><b>Username</b></label>
					<input id="username"  type="text" placeholder="Enter Username" name="username" value="<?php if(isset($_COOKIE['username'])) { echo $_COOKIE['username']; } ?>" required>

					<label><b>Password</b></label>
					<input id="password" type="password" placeholder="Enter Password" name="password" value="<?php if(isset($_COOKIE['password'])) { echo $_COOKIE['password']; } ?>"  required>
					
					<button type="submit" class="button-login">Login</button>
					<input name="remember" id="remember" type="checkbox" <?php if(isset($_COOKIE['username'])) { ?> checked <?php } ?> > Remember me

				</div>

				<div class="container1" style="background-color:#f1f1f1">
                    <a class="btn btn-primary btn-lg" href="/create_account">Register</a>
					<span class="psw">Forgot <a href="#">password?</a></span>
				</div>
			</form>
		</div>
	</div>
</header>

<?php
	include 'modal/successLogin.php';
	include 'modal/errorLogin.php';
?>
