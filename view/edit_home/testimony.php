
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#testimonials">
                Testimonies</a>
        </h4>
    </div>
    <div id="testimonials" class="panel-collapse collapse">
        <div class="panel-body"><br>
            <div class="row">
				<a href="javascript:void(0)" class="btn btn-info pull-left btn-add-testimony"><i class="glyphicon glyphicon-plus"></i> Add Testimony</a>
				<br>
			</div>
			<div class="row">
				<table id="table-testimony" class="table table-hover table-bordered" width="100%">
					<colgroup>
						<col width="25%">
						<col width="25%">
						<col width="25%">
						<col width="25%">
					</colgroup>
					<thead>
					<th>Name</th>
					<th>Title</th>
					<th>Content</th>
					<th>Action</th>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
        </div>
    </div>
</div>