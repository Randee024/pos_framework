<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
				Container 1</a>
		</h4>
	</div>
	<div id="collapse2" class="panel-collapse collapse">
		<div class="panel-body"><br>
			<form class="form form-vertical" id="container-1-from">
				<input type="hidden" name="id_edit" value="<?php echo isset($container1['id'])? $container1['id']: ''; ?>" />
				<input type="hidden" name="id_page" value="<?php echo PageModel::PAGE_HOME; ?>" />
				<input type="hidden" name="id_child" value="<?php echo PageModel::CONTAINER_1; ?>" />
				<div class="control-group">
					<label>Title</label>
					<div class="controls">
						<input type="text" name="title" id="title" class="form-control" placeholder="Enter Title" value="<?php echo (isset($container1['title']))? $container1['title'] :'Lorem Ipsum'; ?>">
					</div>
				</div>
				<div class="control-group">
					<label>Sub-Title</label>
					<div class="controls">
						<textarea name="sub_title" id="sub_title" class="form-control" rows="4"><?php echo (isset($container1['sub_title']))? $container1['sub_title'] :'Lorem Ipsum'; ?></textarea>
					</div>
				</div>
				<div class="control-group"><br>
					<label>Change Background Color</label>
					<div class="controls">
						<input id="textColor" name="bg_color" type="color" value="<?php echo (isset($container1['bg_color']))? $container1['bg_color'] :''; ?>" class="btn btn-colorpicker" />
					</div>
				</div>
				<div class="control-group">
					<label></label>
					<div class="controls">
						<a href="javascript:void(0)" class="btn btn-success col-md-3 pull-right btn-save-container-1">
							Save     
						</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>