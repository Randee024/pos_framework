<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#sample_front">
                Sample Front End</a>
        </h4>
    </div>
    <div id="sample_front" class="panel-collapse collapse">
        <div class="panel-body"><br>

            <div class="row">
                <div class="col-md-12">
                    <label>Front End Design Lists</label><br>
                    <a href="javascript:void(0)" class="btn btn-info pull-left btn-add-front-end"><i class="glyphicon glyphicon-plus"></i> Add Front End Design</a>
                </div>
                <div class="col-md-12">
                    <table id="table-front-end" class="table dataTable no-footer" width="100%">
                        <colgroup>
                            <col width="30%">
                            <col width="35%">
                            <col width="35%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>File Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="control-group">
                <label></label>
                <div class="controls">
                    <a href="javascript:void(0)" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right btn-save-container-2">
                        Save     
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>