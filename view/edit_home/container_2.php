<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
				Container 2</a>
		</h4>
	</div>
	<div id="collapse3" class="panel-collapse collapse">
		<div class="panel-body"><br>
			<form class="form form-vertical" id="container-2-from">
				<input type="hidden" name="id_edit" value="<?php echo isset($container2['id'])? $container2['id']: ''; ?>" />
				<input type="hidden" name="id_page" value="<?php echo PageModel::PAGE_HOME; ?>" />
				<input type="hidden" name="id_child" value="<?php echo PageModel::CONTAINER_2; ?>" />
				<div class="control-group">
					<label>Title</label>
					<div class="controls">
						<input type="text" name="title" id="title" class="form-control" placeholder="Enter Title" value="<?php echo (isset($container2['title']))? $container2['title'] :'Lorem Ipsum'; ?>">
					</div>
				</div>
				<div class="control-group">
					<label>Sub-Title</label>
					<div class="controls">
						<textarea name="sub_title" id="sub_title" class="form-control" rows="4"><?php echo (isset($container2['sub_title']))? $container2['sub_title'] :'Lorem Ipsum'; ?></textarea>
					</div>
				</div>
			</form>
			</br>
			<div class="row">
				<div class="col-md-3">
					<div id="home-container2-upload" <?php echo isset($container2['id_file_upload'])? 'class="hidden"':''; ?> >
						<form id="myDropZone2" class="dropzone container2-home" method="POST">
							<input type="hidden" id="page_edit_id" name="page_edit_id" value="" />
							<input type="hidden" name="new_path" value="home" />
							
						</form>
						<?php if(isset($container2['id_file_upload'])){ ?>
								</br>
								<a href="javascript:void(0)" class="btn btn-default cancel-home-container2-image">Cancel</a>
						<?php } ?>
					</div>

					<div id="home-container2-image" <?php echo isset($container2['id_file_upload'])? '':'class="hidden"'; ?>>
						<img class="img-responsive" alt="" src="<?php echo $fileContainer2['path'].$fileContainer2['file_name']; ?>">
						</br>
						<a href="javascript:void(0)" class="btn btn-danger change-home-container2-image">Change background image</a>
					</div>

				</div>
			</div>
			<div class="control-group">
				<label></label>
				<div class="controls">
					<a href="javascript:void(0)" class="btn btn-success col-md-3 pull-right btn-save-container-2">
						Save     
					</a>
				</div>
			</div>
		</div>
	</div>
</div>