<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                Container 4</a>
        </h4>
    </div>
    <div id="collapse5" class="panel-collapse collapse">
        <div class="panel-body"><br>
            <form class="form form-vertical" id="container-4-from">
                <input type="hidden" name="id_edit" value="<?php echo isset($container4['id']) ? $container4['id'] : ''; ?>" />
                <input type="hidden" name="id_page" value="<?php echo PageModel::PAGE_HOME; ?>" />
                <input type="hidden" name="id_child" value="<?php echo PageModel::CONTAINER_4; ?>" />
                <div class="control-group">
                    <label>Title</label>
                    <div class="controls">
                        <input type="text" name="title" id="title" class="form-control" placeholder="Enter Title" value="<?php echo (isset($container4['title'])) ? $container4['title'] : 'Lorem Ipsum'; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label>Sub-Title</label>
                    <div class="controls">
                        <textarea name="sub_title" id="sub_title" class="form-control" rows="4"><?php echo (isset($container4['sub_title'])) ? $container4['sub_title'] : 'Lorem Ipsum'; ?></textarea>
                    </div>
                </div>
                <!--<div class="control-group"><br>
                    <label>Change Background Color</label>
                    <div class="controls">
                        <input id="textColor" name="button_color" type="color" value="<?php echo (isset($container4['button_color'])) ? $container4['button_color'] : ''; ?>" class="btn btn-colorpicker" />
                    </div>
                </div>
                <div class="control-group"><br>
                    <label>Change Hover Color</label>
                    <div class="controls">
                        <input id="textColor" name="button_hover_color" type="color" value="<?php echo (isset($container4['button_hover_color'])) ? $container4['button_hover_color'] : ''; ?>" class="btn btn-colorpicker" />
                    </div>
                </div>-->
             </form>
            <div class="row">
                <div class="control-group"><br>
					<label>Change Background Photo</label>
					<div class="controls">
						<div class="col-md-3">
							<div id="home-container4-upload" <?php echo isset($container4['id_file_upload']) ? 'class="hidden"' : ''; ?> >
								<form id="myDropZone4" class="dropzone container4-home" method="POST">
									<input type="hidden" id="page_edit_id" name="page_edit_id" value="" />
									<input type="hidden" name="new_path" value="home" />

								</form>
								<?php if (isset($container4['id_file_upload'])) { ?>
									</br>
									<a href="javascript:void(0)" class="btn btn-danger cancel-home-container4-image">Cancel</a>
								<?php } ?>
							</div>

							<div id="home-container4-image" <?php echo isset($container4['id_file_upload']) ? '' : 'class="hidden"'; ?>>
								<img class="img-responsive" alt="" src="<?php echo $fileContainer4['path'] . $fileContainer4['file_name']; ?>">
								</br>
								<a href="javascript:void(0)" class="btn btn-primary change-home-container4-image">Change background image</a>
							</div>
						</div>
					</div>
				</div>
            </div>
              <div class="control-group">
                <label></label>
                <div class="controls">
                    <a href="javascript:void(0)" class="btn btn-success col-md-3 pull-right btn-save-container-4">
                        Save     
                    </a>
                </div>
            </div>  
            
            
            <div class="row">
                <br>
                    <label>Pricing Lists</label><br>
                    <a href="javascript:void(0)" class="btn btn-info pull-left btn-add-plan"><i class="glyphicon glyphicon-plus"></i>Add Plan</a>
                
                
                <div class="col-md-12"><br>
                    <table id="table-pricing" class="table dataTable no-footer" width="100%">
                        <colgroup>
                            <col width="10%">
                            <col width="20%">
                            <col width="20%">
                            <col width="20%">
                            <col width="10%">
                            <col width="20%">
                        </colgroup>
                        <thead>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Pricing</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            <br>
            
            </div>
           
        </div>
    </div>
</div>
    