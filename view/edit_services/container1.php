<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion1" href="#services_container1">Container 1</a>
		</h4>
	</div>
	<div id="services_container1" class="panel-collapse collapse">
		<div class="row">
			<div class="col-md-12">
				</br>
				<div class="row">
					<label>Service Lists</label>
				</div>
				</br>
				<div class="row">
					<a href="javascript:void(0)" class="btn btn-info pull-left btn-add-services"><i class="glyphicon glyphicon-plus"></i> Add Services</a>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel-body"><br>
					<table id="table-services" class="table table-hover table-bordered" width="100%">
						<colgroup>
							<col width="25%">
							<col width="25%">
							<col width="25%">
							<col width="25%">
						</colgroup>
						<thead>
							<tr>
								<th>Title</th>
								<th>Description</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>