<?php 

$headings = array();
$conatainer1 = array();
$conatainer3 = array();
$conatainer4 = array();
$backgroundImages = '';
$backgroundImagesContainer1 = '';
$backgroundImagesContainer3 = '';
$backgroundImagesContainer4 = '';


if(!empty($pageDetails)){

	foreach($pageDetails as $key => $value)
	{
		if($value['id_child'] == PageModel::HEADING){
			$headings = $value;
		}else if($value['id_child'] == PageModel::CONTAINER_1){
			$conatainer1 = $value;
		}else if($value['id_child'] == PageModel::CONTAINER_3){
			$conatainer3 = $value;
		}else if($value['id_child'] == PageModel::CONTAINER_4){
			$conatainer4 = $value;
		}
	}
	
	if(!empty($headings) && isset($headings['id_file_upload'])){
		$backgroundImages = $fileModel->getFileDetails($headings['id_file_upload']);
	}
	
	if(!empty($conatainer1) && isset($conatainer1['id_file_upload'])){
		$backgroundImagesContainer1 = $fileModel->getFileDetails($conatainer1['id_file_upload']);
	}
	
	if(!empty($conatainer3) && isset($conatainer3['id_file_upload'])){
		$backgroundImagesContainer3 = $fileModel->getFileDetails($conatainer3['id_file_upload']);
	}
	
	if(!empty($conatainer4) && isset($conatainer4['id_file_upload'])){
		$backgroundImagesContainer4 = $fileModel->getFileDetails($conatainer4['id_file_upload']);
	}
	
	
}

if(!empty($backgroundImages)){ ?>
	<header class="page-heading-about" style="background-image: url('<?php echo $backgroundImages['path'].$backgroundImages['file_name']; ?>');">
<?php  }else{ ?>
	<header class="page-heading-about">
<?php  } ?>


    <div class="container-fluid padding-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            <p>
								<h1 class="wow fadeIn">
									<?php if(!empty($headings['title'])){ 
											echo $headings['title'];
										}else{ ?>
											About us
									<?php  } ?>
								</h1>
                            </p>
                            <br><br>
                            <p>
								<h5>
									<?php if(!empty($headings['sub_title'])){ 
												echo $headings['sub_title'];
										}else{ ?>
											Budget-friendly, cloud-based POS system that's packed with all the time-saving features retailers need to run and grow their business. 
											It comes with a wide range of advanced, yet easy-to-use tools to help you save time and boost sales, without any long-term contracts or expensive fees.
									<?php  } ?>
								</h5>
							</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="container-fluid page-content-about">
	<div class="row">
		<div class="col-md-6 wow fadeIn" data-wow-delay="2.5s">
			<legend>
				<h2>
					<?php if(!empty($conatainer1['title'])){ 
							echo $conatainer1['title'];
						}else{ ?>
							About the Company
					<?php  } ?>
				</h2>
			</legend><br>
			
			
			
				<?php if(!empty($conatainer1['title'])){ 
							echo '<p>'.$conatainer1['title'].'</p><br>';
						}else{ ?>
							<p>We’re designers, developers, illustrators, copywriters and coders, but we also have pure gold ideas.
								iPhone games, time-keeping apps, dog-backpacks, too many tshirt slogans to keep track of.
								One day we’ll be rich, you’d best work with us while we need the money. We’re designers, developers, illustrators, copywriters and coders, but we also have pure gold ideas.
								iPhone games, time-keeping apps, dog-backpacks, too many tshirt slogans to keep track of.
								One day we’ll be rich, you’d best work with us while we need the money.
							</p><br>
							<p>We’re designers, developers, illustrators, copywriters and coders, but we also have pure gold ideas.
								iPhone games, time-keeping apps, dog-backpacks, too many tshirt slogans to keep track of.
								One day we’ll be rich, you’d best work with us while we need the money.
							</p><br>
							<p>We’re designers, developers, illustrators, copywriters and coders, but we also have pure gold ideas.
								iPhone games, time-keeping apps, dog-backpacks, too many tshirt slogans to keep track of.
								One day we’ll be rich, you’d best work with us while we need the money. We’re designers, developers, illustrators, copywriters and coders, but we also have pure gold ideas.
								iPhone games, time-keeping apps, dog-backpacks, too many tshirt slogans to keep track of.
								One day we’ll be rich, you’d best work with us while we need the money.
							</p>
				<?php  } ?>
				
			
			
		</div>
		<div class="col-md-6 wow fadeIn" data-wow-delay="3s">
		
			<?php if(!empty($backgroundImagesContainer1)){ ?>
				<img src="<?php echo $backgroundImagesContainer1['path'].$backgroundImagesContainer1['file_name']; ?>" alt="" style="width: 100%;" />
			<?php }else{ ?>
				<img src="../images/Employees.jpg" alt="" style="width: 100%;" />
			<?php } ?>
			
		</div>
	</div>
</div>

<!-- Add Google Maps -->
<div id="googleMap" style="height:400px;width:100%;"></div>
<script>
    function myMap() {
        var myCenter = new google.maps.LatLng(<?php echo (isset($mapCoordinates['latitude']) && !empty($mapCoordinates['latitude']))? $mapCoordinates['latitude'].',' :'14.5974606,'; 
													echo (isset($mapCoordinates['longitude']) && !empty($mapCoordinates['longitude']))? $mapCoordinates['longitude']:'120.9751059';  ?> );
        var mapProp = {center: myCenter, zoom: 12, scrollwheel: false, draggable: false, mapTypeId: google.maps.MapTypeId.ROADMAP};
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        var marker = new google.maps.Marker({position: myCenter});
        marker.setMap(map);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxHe6gmawMeGRVE3XKJJpvNh8IrcBrwbk&callback=myMap">
</script>

<div class='container-fluid page-content3'>
    <div class="col-md-3 wow fadeIn" data-wow-delay="1s">
        
		<?php 
			if(!empty($backgroundImagesContainer3)){
				echo '<img src="'.$backgroundImagesContainer3['path'].$backgroundImagesContainer3['file_name'].'" alt="" style="width: 98%"/>';
			}else{
				echo '<img src="../images/building_PNG25.png" alt="" style="width: 98%"/>';
			}
		?>
		
    </div>
    <div class="col-md-6">
        <div class="container wow fadeInDown" data-wow-delay="2s">
            <h1><?php echo (isset($conatainer3['title']))? $conatainer3['title']:'History' ; ?></h1>
            <p><?php echo (isset($conatainer3['sub_title']))? $conatainer3['sub_title']:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.' ; ?> </p>
        </div>
    </div>
</div>

<div class="container-fluid page-content-about">
    <div class="row">
        <div class="col-md-12">
            <legend class="wow fadeIn" data-wow-delay="1s"><h2><?php echo (isset($conatainer4['title']))? $conatainer4['title']:'Trusted by' ; ?></h2></legend><br>
            <div class="clients wow fadeIn" data-wow-delay="2s">
                <ul>
					
					<?php 
						if(!empty($partnerLogos)){
							foreach($partnerLogos as $key=>$value){
								echo '	 <li style="margin-right: 39px;">
											<img class="img-responsive" src="'.$value['path'].$value['file_name'].'">
										</li>
									';
							}
						}else{
					?>
					
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/NBC2.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2013/08/wired2.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2013/04/abc12.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2013/08/tayler1.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2013/08/QualcommLogo2.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/BMW2.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2013/04/usa11.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/Chargers1.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/TorreyPines2.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/Ikea2.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2013/08/ubisoft.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2013/04/cbs11.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2013/08/NapaCellars2.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2013/08/dji.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/TheMusicBed3.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/CookingLight2.png">
                    </li>
                    <li style="margin-right: 39px;">
                        <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/TheRockChurch3.png">
                    </li>
					
					<?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>