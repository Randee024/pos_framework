<?php 

$headings = array();
$conatainer1 = array();

$backgroundImages = '';
$backgroundImagesContainer1 = '';



if(!empty($pageDetails)){

	foreach($pageDetails as $key => $value)
	{
		if($value['id_child'] == PageModel::HEADING){
			$headings = $value;
		}else if($value['id_child'] == PageModel::CONTAINER_1){
			$conatainer1 = $value;
		}
	}
	
	if(!empty($headings) && isset($headings['id_file_upload'])){
		$backgroundImages = $fileModel->getFileDetails($headings['id_file_upload']);
	}
	
	if(!empty($conatainer1) && isset($conatainer1['id_file_upload'])){
		$backgroundImagesContainer2 = $fileModel->getFileDetails($conatainer1['id_file_upload']);
	}
	
	
}

if(!empty($backgroundImages)){ ?>
	<header class="page-heading-services" style="background-image: url('<?php echo $backgroundImages['path'].$backgroundImages['file_name']; ?>');">
<?php  }else{ ?>
	<header class="page-heading-services">
<?php  } ?>
		<div class="container-fluid padding-30">
			<div class="row">
				<div class="col-lg-12">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<p>
									<h1 class="wow fadeIn">
										<?php if(!empty($headings['title'])){ 
												echo $headings['title'];
											}else{ ?>
												Services
										<?php  } ?>
									</h1>
								</p>
								<br><br>
								<p>
									<h5>
										<?php if(!empty($headings['sub_title'])){ 
													echo $headings['sub_title'];
											}else{ ?>
												Budget-friendly, cloud-based POS system that's packed with all the time-saving features retailers need to run and grow their business. 
												It comes with a wide range of advanced, yet easy-to-use tools to help you save time and boost sales, without any long-term contracts or expensive fees.
										<?php  } ?>
									</h5>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>



<div class="container"><br><br>
    <legend>What our Company Offers</legend>
		<?php
		if(!empty($services)){
			
			$divChecker = 1;
			
			foreach($services as $key => $value)
			{
				
				if($divChecker == 1){
					echo '<div class="row">';
				}
				
					echo'
						<div class="col-md-3">
							<img class="img-responsive" src="'.$value['path'].$value['file_name'].'">
							<h2 class="h3 services-list__item-title">'.$value['title'].'</h2>
							<p class="services-list__item-intro">
								'.$value['description'].'
							</p>
						</div>
					';
					
				if($divChecker == 4){
					echo '</div>';
					$divChecker = 1;
				}else{
					$divChecker++;
				}
	
				
			}
			
			if($divChecker < 4){
				echo '</div>';
			}
			
		}else{
		?>
		<div class="row">
		
			<div class="col-md-3">
				<img src="../images/icons/icon1.png" alt="code icon">
				<h2 class="h3 services-list__item-title">Website Design &amp; Development</h2>
				<p class="services-list__item-intro">
					Creating beautiful and functional websites which your users will love to browse.
				</p>
			</div>
			<div class="col-md-3">
				<img src="../images/icons/icon1.png" alt="code icon">
				<h2 class="h3 services-list__item-title">Website Design &amp; Development</h2>
				<p class="services-list__item-intro">
					Creating beautiful and functional websites which your users will love to browse.
				</p>
			</div>
			<div class="col-md-3">
				<img src="../images/icons/icon1.png" alt="code icon">
				<h2 class="h3 services-list__item-title">Website Design &amp; Development</h2>
				<p class="services-list__item-intro">
					Creating beautiful and functional websites which your users will love to browse.
				</p>
			</div>
			<div class="col-md-3">
				<img src="../images/icons/icon1.png" alt="code icon">
				<h2 class="h3 services-list__item-title">Website Design &amp; Development</h2>
				<p class="services-list__item-intro">
					Creating beautiful and functional websites which your users will love to browse.
				</p>
			</div>

		</div>
		<div class="row">
		
			<div class="col-md-3">
				<img src="../images/icons/icon1.png" alt="code icon">
				<h2 class="h3 services-list__item-title">Website Design &amp; Development</h2>
				<p class="services-list__item-intro">
					Creating beautiful and functional websites which your users will love to browse.
				</p>
			</div>
			<div class="col-md-3">
				<img src="../images/icons/icon1.png" alt="code icon">
				<h2 class="h3 services-list__item-title">Website Design &amp; Development</h2>
				<p class="services-list__item-intro">
					Creating beautiful and functional websites which your users will love to browse.
				</p>
			</div>
			<div class="col-md-3">
				<img src="../images/icons/icon1.png" alt="code icon">
				<h2 class="h3 services-list__item-title">Website Design &amp; Development</h2>
				<p class="services-list__item-intro">
					Creating beautiful and functional websites which your users will love to browse.
				</p>
			</div>
			<div class="col-md-3">
				<img src="../images/icons/icon1.png" alt="code icon">
				<h2 class="h3 services-list__item-title">Website Design &amp; Development</h2>
				<p class="services-list__item-intro">
					Creating beautiful and functional websites which your users will love to browse.
				</p>
			</div>

		</div>
		
	<?php } ?>
  
</div>
<br>