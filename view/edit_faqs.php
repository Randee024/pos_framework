
<?php
	$headings = array();
	$container1 = array();
	$fileHeading = array();

	
	if(!empty($getAllFaqEdits)){
		
		foreach($getAllFaqEdits as $key => $value)
		{
			if($value['id_child'] == PageModel::HEADING){
				$headings = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_1){
				$container1 = $value;
			}
		}
		
		if(!empty($headings) && isset($headings['id_file_upload'])){
			$fileHeading = $fileUploadModel->getFileDetails($headings['id_file_upload']);
		}
		
	}
	
	
?>
<header class="page-heading-backend">
    <div class="container">
        <div class="modal-content-backend-content animate col-md-12">
            <div class="container1">
                <div class="col-md-12">
                    <a href="#"><strong><i class="glyphicon glyphicon-pencil"></i> Edit Pages</strong></a>
                    <hr>
                    <ol class="breadcrumb">
                        <li><a href="/edit_home">Home</a></li>
                        <li><a href="/edit_services">Services</a></li>
                        <li><a href="/edit_about_us">About Us</a></li>
                        <li class="active">FAQ's</li>
                        <li><a href="/edit_contact_us">Contact Us</a></li>
                    </ol>
                    <!---------------- ABOUT US PANEL------------------->
                    <div class="panel panel-default" id="home">
                        <div class="panel-body">
                            <div class="panel-group" id="accordion3">
                                <!---------------- HEADING------------------->
                                <?php
									include 'edit_faq/heading.php';
								?>
								<!---------------- CONTAINER 1------------------->
                                <?php
									include 'edit_faq/container_1.php';
								?>
                            </div><!---------------- END PANEL -------------------->
                        </div><!---------------- END PANEL BODY -------------------->
                    </div><!------------- END ABOUT US PANEL -------------------->
                </div><!----------------- END COLUMN -------------------->
            </div><!------------------ END CONTAINER 1 -------------------->
        </div><!----------------- END MODAL CONTENT ANIMATE -------------------->
    </div><!---------------- END CONTAINER -------------------->
</header><!---------------- END HEADER -------------------->
<?php

	include 'modal/addFaq.php';
	include 'modal/deleteFaq.php';
	include 'modal/successFaqEdit.php';
	include 'modal/successFaqMessage.php';
?>




