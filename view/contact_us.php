<?php
	$headings = array();
	$container1 = array();
	$fileHeading = array();
	
	
	if(!empty($pageDetails)){
		foreach($pageDetails as $key => $value)
		{
			if($value['id_child'] == PageModel::HEADING){
				$headings = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_1){
				$container1 = $value;
			}
		}
		
		if(!empty($headings) && isset($headings['id_file_upload'])){
			$fileHeading = $fileModel->getFileDetails($headings['id_file_upload']);
		}
	}
	
if(!empty($fileHeading)){ ?>
	<header class="page-heading-contact-us" style="background-image: url('<?php echo $fileHeading['path'].$fileHeading['file_name']; ?>');">
<?php  }else{ ?>
	<header class="page-heading-contact-us">
<?php  } ?>
    <div class="container-fluid padding-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            <p>
                            <h1 class="wow fadeIn"><?php echo (isset($headings['title']))? $headings['title']: 'Contact Us'; ?></h1>
                            </p>
                            <br><br>
                            <p>
                            <h5><?php echo (isset($headings['sub_title']))? $headings['sub_title']: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'; ?></h5>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="container"><br>
    <div class="container page-content">
        <div class="card-block medium row-fluid page-body"><br>
            <p><?php echo (isset($container1['sub_title']))? $container1['sub_title']: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'; ?></p><br>
            <form accept-charset="UTF-8" class="form-horizontal edit-form" id="form-contact-us" >
                <legend>Please fill in the form below</legend>
                <div class="span12 col-md-6 col-md-offset-3">
                    <div class="control-group">
                        <label class="control-label" for="name">Name</label>
                        <div class="controls">
                            <input id="name" name="name" placeholder="Your name" size="30" type="text" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email">Email</label>
                        <div class="controls">
                            <input id="email" name="email" placeholder="Your email address" size="30" type="text" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="subject">Subject</label>
                        <div class="controls">
                            <input id="subject" name="subject" placeholder="What will this message be about?" size="30" type="text" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="message">Message</label>
                        <div class="controls">
                            <textarea cols="74" id="message" name="message" placeholder="Leave a message here" rows="10" style="width: 100%" required></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <a id="load" class="btn btn-success btn-send-message" href="javascript:void(0)">
                               <i id="spinner" class="fa fa-spinner fa-spin hidden"></i> <i id="envelope" class="fa fa-envelope"></i> <span>Send</span>
                            </a> 
						</div>
                    </div>
                </div>
            </form>
        </div>
    </div><br><br>
</div>

<?php

	include 'modal/successContactUsMessage.php';
	include 'modal/errorGeneralMessage.php';
	include 'modal/pleaseWait.php';

?>