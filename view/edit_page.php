

<header class="page-heading-backend">
    <div class="container">
        <!--                <form class="modal-content-backend animate col-md-3">
                            <div class="imgcontainer">
                                <img src="./images/sample-user.png" alt="Avatar" class="avatar"><br>
                                <h2>Welcome, Ralph</h2>
                                <h6>Last login: 04 August 2017 11:29 AM</h6>
                            </div>
                            <div class="container1">
                                <button type="submit">Update Profile</button>
                                <button type="submit">Check Status</button>
                                <button type="submit">Settings</button>
                            </div>
        
                            <div class="container1" style="background-color:#f1f1f1">
                                <button type="button" onclick="document.getElementById('id01').style.display = 'none'" class="cancelbtn">Logout</button>
                            </div>
                        </form>-->
        <div class="modal-content-backend-content animate col-md-12">
            <div class="container1">
                <div class="col-md-12">
                    <a href="#"><strong><i class="glyphicon glyphicon-pencil"></i> Edit Pages</strong></a>
                    <hr>
                    <ol class="breadcrumb">
                        <li class="active"><a href="#">Home</a></li>
                        <li><a href="#">Services</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">FAQ's</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ol>

                    <div class="row">
                        <!-- center left-->
                        <div class="col-md-12">

                            <!--tabs-->
                            <div class="panel">
                                <ul class="nav nav-tabs" id="myTab">
                                    <li class="active"><a href="#home" data-toggle="tab">Home</a></li>
                                    <li><a href="#services" data-toggle="tab">Services</a></li>
                                    <li><a href="#about-us" data-toggle="tab">About Us</a></li>
                                    <li><a href="#faqs" data-toggle="tab">FAQ's</a></li>
                                    <li><a href="#contact-us" data-toggle="tab">Contact Us</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!---------------- HOME TAB ------------------->
                                    <div class="tab-pane active" id="home">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!------------------ EDIT CONTAINER HOME ---------------------->
                                                <div class="container-fluid">
                                                    <div class="panel-group" id="accordion">
                                                        <!--------------------- EDIT HEADING --------------------------->
                                                        <?php
                                                        include 'edit_page/heading.php';
                                                        ?>
                                                        <!--------------------- END EDIT HEADING --------------------------->
                                                        <!--------------------- EDIT CONTAINER 1 --------------------------->
                                                        <?php
                                                        include 'edit_page/container_1.php';
                                                        ?>
                                                        <!------------------------- END EDIT CONTAINER 1 --------------------------->
                                                        <!-------------------------- EDIT CONTAINER 2 --------------------------->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                                                        Container 2</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse3" class="panel-collapse collapse">
                                                                <div class="panel-body"><br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Background Photo</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--------------------- END EDIT CONTAINER 2 --------------------------->
                                                        <!--------------------- EDIT CONTAINER 3 --------------------------->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                                                        Container 3</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse4" class="panel-collapse collapse">
                                                                <div class="panel-body"><br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Product Photo</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Change Background Color</label>
                                                                            <div class="controls">
                                                                                <input id="textColor" type="color" value="" class="btn btn-colorpicker" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--------------------- END EDIT CONTAINER 3 --------------------------->
                                                        <!--------------------- EDIT CONTAINER 4 --------------------------->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                                                        Container 4</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse5" class="panel-collapse collapse">
                                                                <div class="panel-body"><br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Background Photo</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Change Background Color</label>
                                                                            <div class="controls">
                                                                                <input id="textColor" type="color" value="" class="btn btn-colorpicker" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Change Pricing</label>
                                                                            <div class="controls">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon">Price 1</span>
                                                                                        <input id="prependedtext" name="prependedtext" class="form-control" placeholder="Amount" type="text">
                                                                                    </div>
                                                                                    <br>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon">Price 2</span>
                                                                                        <input id="prependedtext" name="prependedtext" class="form-control" placeholder="Amount" type="text">
                                                                                    </div>
                                                                                    <br>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon">Price 3</span>
                                                                                        <input id="prependedtext" name="prependedtext" class="form-control" placeholder="Amount" type="text">
                                                                                    </div>
                                                                                    <br>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon">Price 4</span>
                                                                                        <input id="prependedtext" name="prependedtext" class="form-control" placeholder="Amount" type="text">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <div class="row">
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <div class="control-group">
                                                                                <label></label>
                                                                                <div class="controls">
                                                                                    <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                        Save     
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--------------------- END EDIT CONTAINER 4 --------------------------->
                                                        <!--------------------- EDIT MODAL TESTIMONIES --------------------------->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                                                                        Testimonies (Modal)</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse6" class="panel-collapse collapse">
                                                                <div class="panel-body"><br>
                                                                    <legend>Testimony 1</legend>
                                                                    <form class="form form-vertical well">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Content</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Client Name</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Client Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Upload Client Picture</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                    <hr>
                                                                    <legend>Testimony 2</legend>
                                                                    <form class="form form-vertical well">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Content</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Client Name</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Client Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Upload Client Picture</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                    <hr>
                                                                    <legend>Testimony 3</legend>
                                                                    <form class="form form-vertical well">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Content</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Client Name</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Client Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Upload Client Picture</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                    </form>

                                                                    <div class="row">
                                                                        <form class="form form-vertical">
                                                                            <div class="control-group">
                                                                                <div class="control-group">
                                                                                    <label></label>
                                                                                    <div class="controls">
                                                                                        <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                            Save     
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--------------------- END EDIT MODAL TESTIMONIES ----------------------->
                                                        <!--------------------- EDIT EDIT SAMPLE FRONT END --------------------------->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                                                                        Sample Front-End (Modal)</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse7" class="panel-collapse collapse">
                                                                <div class="panel-body"><br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Photo</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--------------------- END EDIT SAMPLE FRONT END --------------------------->
                                                    </div>
                                                </div>
                                                <!------------------ END EDIT CONTAINER ---------------------->

                                            </div><!--/panel content-->
                                        </div><!--/panel-->
                                    </div>
                                    <!------------------------- END HOME TAB ----------------------------->
                                    <!--------------------------- SERVICES TAB -------------------------------->
                                    <div class="tab-pane" id="services">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!------------------ EDIT CONTAINER SERVICES ---------------------->
                                                <div class="container-fluid">
                                                    <div class="panel-group" id="accordion1">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion1" href="#services_heading">Heading</a>
                                                                </h4>
                                                            </div>
                                                            <div id="services_heading" class="panel-collapse collapse in">
                                                                <div class="panel-body"><br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Background Photo</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion1" href="#services_container1">Container 1</a>
                                                                </h4>
                                                            </div>
                                                            <div id="services_container1" class="panel-collapse collapse">
                                                                <div class="panel-body"><br>
                                                                    <form class="form form-vertical">
                                                                        <legend>Service 1</legend>
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Icon</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div><br>
                                                                        <legend>Service 2</legend>
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Icon</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div><br>
                                                                        <legend>Service 3</legend>
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Icon</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <!------------------ END EDIT CONTAINER ---------------------->

                                            </div><!--/panel content-->
                                        </div><!--/panel-->
                                    </div>
                                    <!------------------------- END SERVICES TAB -------------------------------->
                                    <!--------------------------- ABOUT US TAB -------------------------------->
                                    <div class="tab-pane" id="about-us">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!------------------ EDIT CONTAINER ABOUT US ---------------------->
                                                <div class="container-fluid">
                                                    <div class="panel-group" id="accordion2">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion2" href="#aboutus_heading">Heading</a>
                                                                </h4>
                                                            </div>
                                                            <div id="aboutus_heading" class="panel-collapse collapse in">
                                                                <div class="panel-body"><br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Background Photo</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion2" href="#aboutus_container1">Container 1</a>
                                                                </h4>
                                                            </div>
                                                            <div id="aboutus_container1" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Company Photo</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion2" href="#aboutus_container2">Container 2</a>
                                                                </h4>
                                                            </div>
                                                            <div id="aboutus_container2" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <br>
                                                                    <form class="form form-vertical">
                                                                        <legend>Coordinates</legend>
                                                                        <div class="control-group">
                                                                            <label>Latitude</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="44.265">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Longitude</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="128.63">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion2" href="#aboutus_container3">Container 3</a>
                                                                </h4>
                                                            </div>
                                                            <div id="aboutus_container3" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Photo</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion2" href="#aboutus_container4">Container 4</a>
                                                                </h4>
                                                            </div>
                                                            <div id="aboutus_container4" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <br>
                                                                    <div class="row">
                                                                        <form class="form form-vertical">
                                                                            <div class="control-group">
                                                                                <label>Title</label>
                                                                                <div class="controls">
                                                                                    <input type="text" class="form-control" placeholder="Enter Name" value="Trusted by">
                                                                                </div>
                                                                            </div>
                                                                            <div class="control-group">
                                                                                <label></label>
                                                                                <div class="controls">
                                                                                    <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                        Save     
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                            <div class="control-group"><br>
                                                                                <label>Upload Photo</label>
                                                                                <div class="controls">
                                                                                    <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                                </div>
                                                                            </div>

                                                                        </form>
                                                                    </div><br><br><br><br>
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading">
                                                                                    <!-- Watch Out: Here We must use the effect name in the data tag-->
                                                                                    <span class="pull-right clickable" data-effect="fadeOut"><i class="fa fa-times"></i></span>
                                                                                </div>
                                                                                <div class="panel-body">
                                                                                    <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/NBC2.png">
                                                                                </div>
                                                                            </div>  
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading">
                                                                                    <!-- Watch Out: Here We must use the effect name in the data tag-->
                                                                                    <span class="pull-right clickable" data-effect="fadeOut"><i class="fa fa-times"></i></span>
                                                                                </div>
                                                                                <div class="panel-body">
                                                                                    <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/NBC2.png">
                                                                                </div>
                                                                            </div>  
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading">
                                                                                    <!-- Watch Out: Here We must use the effect name in the data tag-->
                                                                                    <span class="pull-right clickable" data-effect="fadeOut"><i class="fa fa-times"></i></span>
                                                                                </div>
                                                                                <div class="panel-body">
                                                                                    <img src="http://www.fortyonetwenty.com/wp-content/uploads/2012/12/NBC2.png">
                                                                                </div>
                                                                            </div>  
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <!------------------ END EDIT CONTAINER ---------------------->

                                            </div><!--/panel content-->
                                        </div><!--/panel-->
                                    </div>
                                    <!------------------------- END ABOUT US TAB -------------------------------->
                                    <!--------------------------- FAQS TAB -------------------------------->
                                    <div class="tab-pane" id="faqs">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!------------------ EDIT CONTAINER ABOUT US ---------------------->
                                                <div class="container-fluid">
                                                    <div class="panel-group" id="accordion3">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion3" href="#faqs_heading">Heading</a>
                                                                </h4>
                                                            </div>
                                                            <div id="faqs_heading" class="panel-collapse collapse in">
                                                                <div class="panel-body"><br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Background Photo</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div id="summernote">Hello Summernote</div>

                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <!------------------ END EDIT CONTAINER ---------------------->

                                            </div><!--/panel content-->
                                        </div><!--/panel-->
                                    </div>
                                    <!------------------------- END FAQS TAB -------------------------------->
                                    <!--------------------------- CONTACT US TAB -------------------------------->
                                    <div class="tab-pane" id="contact-us">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!------------------ EDIT CONTAINER ABOUT US ---------------------->
                                                <div class="container-fluid">
                                                    <div class="panel-group" id="accordion4">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion4" href="#contactus_heading">Heading</a>
                                                                </h4>
                                                            </div>
                                                            <div id="contactus_heading" class="panel-collapse collapse in">
                                                                <div class="panel-body"><br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <label>Title</label>
                                                                            <div class="controls">
                                                                                <input type="text" class="form-control" placeholder="Enter Name" value="Lorem Ipsum">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label>Sub-Title</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group"><br>
                                                                            <label>Upload Background Photo</label>
                                                                            <div class="controls">
                                                                                <input id="filebutton" name="filebutton" class="input-file" type="file">
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion4" href="#contactus_container1">Container 1</a>
                                                                </h4>
                                                            </div>
                                                            <div id="contactus_container1" class="panel-collapse collapse in">
                                                                <div class="panel-body"><br>
                                                                    <form class="form form-vertical">
                                                                        <div class="control-group">
                                                                            <label>Content</label>
                                                                            <div class="controls">
                                                                                <textarea class="form-control" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <label></label>
                                                                            <div class="controls">
                                                                                <button type="submit" class="btn btn-outlined btn-black btn-xs col-md-3 pull-right">
                                                                                    Save     
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <!------------------ END EDIT CONTAINER ---------------------->

                                            </div><!--/panel content-->
                                        </div><!--/panel-->
                                    </div>
                                    <!------------------------- END CONTACT US TAB -------------------------------->
                                </div>
                                <!------------------------- END TAB CONTENT -------------------------------->
                            </div>
                        </div>
                        <!--/col-->
                        <div class="col-md-6">
                        </div>
                        <!--/col-span-6-->

                    </div>
                    <!--/row-->
                </div>
            </div>
        </div>
    </div>
</div>
</header>





