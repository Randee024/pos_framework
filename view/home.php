
<?php 

$headings = array();
$conatainer1 = array();
$conatainer2 = array();
$conatainer3 = array();
$conatainer4 = array();
$backgroundImages = '';
$backgroundImagesContainer2 = '';
$backgroundImagesContainer3 = '';
$backgroundImagesContainer4 = '';
$clientImage = '';
$planLists =array();

$testimonials = $testimonyModel->getAllTestimonials();

if(!empty($pageDetails)){

	$planLists = $planModel->getAllPricingList();
	
	foreach($pageDetails as $key => $value)
	{
		if($value['id_child'] == PageModel::HEADING){
			$headings = $value;
		}else if($value['id_child'] == PageModel::CONTAINER_1){
			$conatainer1 = $value;
		}else if($value['id_child'] == PageModel::CONTAINER_2){
			$conatainer2 = $value;
		}else if($value['id_child'] == PageModel::CONTAINER_3){
			$conatainer3 = $value;
		}else if($value['id_child'] == PageModel::CONTAINER_4){
			$conatainer4 = $value;
		}
	}
	
	if(!empty($headings) && isset($headings['id_file_upload'])){
		$backgroundImages = $fileModel->getFileDetails($headings['id_file_upload']);
	}
	
	if(!empty($conatainer2) && isset($conatainer2['id_file_upload'])){
		$backgroundImagesContainer2 = $fileModel->getFileDetails($conatainer2['id_file_upload']);
	}
	
	if(!empty($conatainer3) && isset($conatainer3['id_file_upload'])){
		$backgroundImagesContainer3 = $fileModel->getFileDetails($conatainer3['id_file_upload']);
	}
	
	if(!empty($conatainer4) && isset($conatainer4['id_file_upload'])){
		$backgroundImagesContainer4 = $fileModel->getFileDetails($conatainer4['id_file_upload']);
	}
	
	
	
		
	
	
}

if(!empty($backgroundImages)){ ?>
	<header class="page-heading" style="background-image: url('<?php echo $backgroundImages['path'].$backgroundImages['file_name']; ?>');">
<?php  }else{ ?>
	<header class="page-heading">
<?php  } ?>
            <div class="container-fluid padding-50">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 col-lg-offset-3">
                                    <p>
                                    <h4 class="wow fadeIn" data-wow-delay="1s">
										<?php if(!empty($headings)){ 
													echo $headings['sub_title'];
											}else{ ?>
												Budget-friendly, cloud-based POS system that's packed with all the time-saving features retailers need to run and grow their business. It comes with a wide range of advanced, yet easy-to-use tools to help you save time and boost sales, without any long-term contracts or expensive fees.
										<?php  } ?>
									</h4>
									</p>
                                    <br><br>
                                    <p>
                                         <a href="#" class="BE_btn BE_btn-outlined BE_btn-white BE_btn-sm wow fadeInLeft" data-wow-delay="1.7s" data-toggle="modal" data-target="#frontendModal">Sample Front End</a>
                                        <a href="#" class="BE_btn BE_btn-outlined BE_btn-white BE_btn-sm wow fadeInRight" data-wow-delay="1.7s" data-toggle="modal" data-target="#testimonyModal">Client Testimonies</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>



<div class='container-fluid page-content1' style="<?php echo (isset($conatainer1['bg_color']))? 'background-color:'.$conatainer1['bg_color']: ''; ?>">
	<div class="container wow fadeInDown">
		<h1><?php echo (isset($conatainer1['title']))? $conatainer1['title']: 'Service Description'; ?></h1>
		<p>
			<?php echo (isset($conatainer1['sub_title']))? $conatainer1['sub_title']: 'POS Now is a retail management system for small to mid-sized businesses. In addition to its Point of Sale module, 
					the solution also offers integrated Inventory Management, Customer Management, and Retail Accounting, including general ledger, 
					purchase orders, and payroll processing. Epos now is cloud-based, and compatible on Windows, Mac, Android, 
					and iPad. Hardware can be purchased directly through POS Now, or through a third party vendor of choice.'; 
			?>
		</p>
	</div>
</div>
<?php if(!empty($backgroundImagesContainer2)){ ?>
	<div class='container-fluid page-content2' style="background-image: url('<?php echo $backgroundImagesContainer2['path'].$backgroundImagesContainer2['file_name']; ?>');">
<?php }else{ ?>
	<div class='container-fluid page-content2'>
<?php } ?>
		<div class="container wow bounceIn">
			<h1><?php echo (isset($conatainer2['title']))? $conatainer2['title'] : 'Service Description'; ?></h1>
			<p>
				<?php echo (isset($conatainer2['sub_title']))? $conatainer2['sub_title']: 'POS Now is a retail management system for small to mid-sized businesses. In addition to its Point of Sale module, 
						the solution also offers integrated Inventory Management, Customer Management, and Retail Accounting, including general ledger, purchase orders, and payroll processing. 
						Epos now is cloud-based, and compatible on Windows, Mac, Android, and iPad. 
						Hardware can be purchased directly through POS Now, or through a third party vendor of choice.';
				?>
			</p>
		</div>
	</div>

<div class='container-fluid page-content3' style="<?php echo (isset($conatainer3['bg_color']))? 'background-color:'.$conatainer3['bg_color']: ''; ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-6 pull-left">
				<img class="wow fadeInLeft img-responsive-m" src="<?php echo (isset($backgroundImagesContainer3['path']))? $backgroundImagesContainer3['path'].$backgroundImagesContainer3['file_name']: '../images/All in one POS.png'; ?>" alt=""/>
			</div>
			<div class="col-md-6 wow fadeIn" data-wow-delay="1s">
				<h1><?php echo (isset($conatainer3['title']))? $conatainer3['title'] : 'Service Description'; ?></h1>
				<p class="pull-left"><?php echo (isset($conatainer3['sub_title']))? $conatainer3['sub_title']: 'POS Now is a retail management system for small to mid-sized businesses. In addition to its Point of Sale module, 
					the solution also offers integrated Inventory Management, Customer Management, and Retail Accounting, including general ledger, purchase orders, and payroll processing. 
					Epos now is cloud-based, and compatible on Windows, Mac, Android, and iPad. 
					Hardware can be purchased directly through POS Now, or through a third party vendor of choice.';
			?>	</p>
			</div>
		</div>
	</div>
</div>
<?php if(!empty($backgroundImagesContainer4)){ ?>
	<div class='container-fluid page-content4' style="background-image: url('<?php echo $backgroundImagesContainer4['path'].$backgroundImagesContainer4['file_name']; ?>');">
<?php }else{ ?>
	<div class='container-fluid page-content4'>
<?php } ?>

	<div class="container wow jackInTheBox">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-xs-12">
				<div class="table_title">
					<p style="color: white"><?php echo (isset($conatainer4['title']))? $conatainer4['title']: 'POS Cloud Service';?></p>
					<hr>
					<h1><?php echo (isset($conatainer4['title']))? $conatainer4['sub_title']: 'Choose Your Plan';?></h1>
				</div>
			</div>
			
			<?php 
				if(!empty($planLists)){
					$addClass = 'custom-to-center';
					
					if(count($planLists) > 3){
						$addClass = '';
					}
					
					foreach($planLists as $key=>$values)
					{ 
						if($key > 3){
							$addClass = 'custom-to-center';
						}
					
					?>
						<div class="col-sm-3 col-md-3 col-xs-12 <?php echo $addClass; ?>">
							<div class="box-1 center">
								<div class="panel panel-success panel-pricing">
									<div class="panel-heading" style="<?php echo (isset($conatainer4['button_color']))? 'background:'.$conatainer4['button_color'].';': '';?>">
										<h3><?php echo (isset($values['pricing']))? $values['title']: '1 Month'; ?></h3>
									</div>
									<div class="panel-body text-center">
										<p><strong><?php echo (isset($values['pricing']))? '$'.$values['pricing']: '$100'; ?></strong></p>
									</div>
									<ul class="list-group text-center">
										<li class="list-group-item">
											<?php echo (isset($values['description']))? $values['description']: '<strong>10</strong> </br>Fidelity Point'; ?>
										</li>
									</ul>
									<div class="panel-footer"> 
										<a class="btn1 btn-lg btn-block btn-success change-hover"  id="join1" style="<?php echo (isset($conatainer4['button_color']))? 'background-color:'.$conatainer4['button_color'].';': '';?>" >
											JOIN
										</a> 
									</div>
								</div>
							</div>
						</div>
				<?php }
				}else{ 
				?>
					<div class="col-sm-3 col-md-3 col-xs-12">
						<div class="box-1 center">
							<div class="panel panel-success panel-pricing">
								<div class="panel-heading">
									<h3>1 Month</h3>
								</div>
								<div class="panel-body text-center">
									<p><strong>$100</strong></p>
								</div>
								<ul class="list-group text-center">
									<li class="list-group-item">
										<strong>10</strong> </br>Fidelity Point
									</li>
								</ul>
								<div class="panel-footer"> 
									<a class="btn1 btn-lg btn-block btn-success change-hover"  id="join1">
										JOIN
									</a> 
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-3 col-xs-12">
						<div class="box-1 center">
							<div class="panel panel-success panel-pricing">
								<div class="panel-heading">
									<h3>12 Month</h3>
								</div>
								<div class="panel-body text-center">
									<p><strong>$200</strong></p>
								</div>
								<ul class="list-group text-center">
									<li class="list-group-item">
										<strong>20</strong> </br>Fidelity Point
									</li>
								</ul>
								<div class="panel-footer"> 
									<a class="btn1 btn-lg btn-block btn-success change-hover"  id="join1">
										JOIN
									</a> 
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-3 col-xs-12">
						<div class="box-1 center">
							<div class="panel panel-success panel-pricing">
								<div class="panel-heading">
									<h3>3 Month</h3>
								</div>
								<div class="panel-body text-center">
									<p><strong>$300</strong></p>
								</div>
								<ul class="list-group text-center">
									<li class="list-group-item">
										<strong>30</strong> </br>Fidelity Point
									</li>
								</ul>
								<div class="panel-footer"> 
									<a class="btn1 btn-lg btn-block btn-success change-hover"  id="join1">
										JOIN
									</a> 
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-3 col-xs-12">
						<div class="box-1 center">
							<div class="panel panel-success panel-pricing">
								<div class="panel-heading">
									<h3>4 Month</h3>
								</div>
								<div class="panel-body text-center">
									<p><strong>$400</strong></p>
								</div>
								<ul class="list-group text-center">
									<li class="list-group-item">
										<strong>40</strong> </br>Fidelity Point
									</li>
								</ul>
								<div class="panel-footer"> 
									<a class="btn1 btn-lg btn-block btn-success change-hover"  id="join1">
										JOIN
									</a> 
								</div>
							</div>
						</div>
					</div>
			<?php } ?>
		</div>
	</div>
</div>

 

<!-- Modal -->
<div id="frontendModal" class="modal fade" data-wow-delay="1s" role="dialog" d>
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content1">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div class="modal-body">
          <img src="../images/pos_sample.png" style="width: 100%" alt=""/>
              </div>
    </div>

  </div>
</div>
<!-- Modal -->
<?php
	include 'modal/testimony.php';
?>