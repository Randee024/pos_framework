<?php
	$headings = array();
	$container1 = array();
	
	$fileHeading = array();
	$fileContainer1 = array();
	
	
	if(!empty($getAllServicesEdits)){
		foreach($getAllServicesEdits as $key => $value)
		{
			if($value['id_child'] == PageModel::HEADING){
				$headings = $value;
			}else if($value['id_child'] == PageModel::CONTAINER_1){
				$container1 = $value;
			}
		}
		
		if(!empty($headings) && isset($headings['id_file_upload'])){
			$fileHeading = $fileUploadModel->getFileDetails($headings['id_file_upload']);
		}
		
		if(!empty($container1) && isset($container1['id_file_upload'])){
			$fileContainer1 = $fileUploadModel->getFileDetails($container1['id_file_upload']);
		}
		
	}
	
?>

<header class="page-heading-backend">
    <div class="container">
        <div class="modal-content-backend-content animate col-md-12">
            <div class="container1">
                <div class="col-md-12">
                    <a href="#"><strong><i class="glyphicon glyphicon-pencil"></i> Edit Pages</strong></a>
                    <hr>
                    <ol class="breadcrumb">
                        <li><a href="/edit_home">Home</a></li>
                        <li class="active">Services</li>
                        <li><a href="/edit_about_us">About Us</a></li>
                        <li><a href="/edit_faqs">FAQ's</a></li>
                        <li><a href="/edit_contact_us">Contact Us</a></li>
                    </ol>
                    <div class="row">
                        <!-- center left-->
                        <div class="col-md-12">

                            <!---------------- HOME ------------------->
                            <div class="panel panel-default" id="home">
                                    
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                
                                                <div class="container-fluid">
                                                    <div class="panel-group" id="accordion1">
                                                        <!--------------------- EDIT HEADING --------------------------->
                                                        <?php
															include 'edit_services/heading.php';
                                                        ?>
                                                        <!--------------------- END EDIT HEADING --------------------------->
														<!------------------ EDIT CONTAINER SERVICES ---------------------->
                                                        <?php
															include 'edit_services/container1.php';
                                                        ?>
														<!------------------ END EDIT CONTAINER ---------------------->
                                                    </div> 
                                                </div>
                                                
                                            </div><!--/panel content-->
                                        </div><!--/panel-->
                                    <!------------------------- END HOME  ----------------------------->
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                </div>
            </div>
        </div>
    </div>
</header>
<?php
	include 'modal/addServices.php';
	include 'modal/servicesActivation.php';
	include 'modal/deleteService.php';
	include 'modal/successServicesEdit.php';
	include 'modal/successServicesMessage.php';
?>




