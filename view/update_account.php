
<header class="page-heading-backend">
    <div class="container">
        <div class="modal-content-backend-content animate col-md-12">
            <div class="container1">
                <div class="col-md-12">
                   <strong><i class="glyphicon glyphicon-pencil"></i> Update Account</strong>
                    <hr>
					
                    <div class="row">
                        <!-- center left-->
                        <div class="col-md-12">

                            <!---------------- HOME ------------------->
                            <div class="panel panel-default" id="home">
								<div class="panel-body">
									<!------------------ EDIT CONTAINER HOME ---------------------->
									<div class="container-fluid">
										<ul class="nav nav-tabs">
											<li class="active">
												<a  href="#userInfo" data-toggle="tab">User Info</a>
											</li>
											<li>
												<a href="#changePassword" data-toggle="tab">Change Password</a>
											</li>
											<li>
												<a href="#changePhoto" data-toggle="tab">Change Photo</a>
											</li>
										</ul>

										<div class="tab-content ">
											<div class="tab-pane active" id="userInfo">
												<div class="container-fluid">
													
													<form id="form-user-info">
														</br>
														<div class="row">
															<input id="userID"  type="hidden" name="userID" value="<?php echo (isset($account['id']))? $account['id'] : ''; ?>">
															<div class="col-md-5">
																<div class="form-group">
																	<label><b>First Name</b></label>
																	<input id="first_name" class="form-control" type="text" placeholder="Enter First Name" name="first_name" value="<?php echo (isset($account['first_name']))? $account['first_name'] : ''; ?>">
																 </div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-5">
																<div class="form-group">
																	<label><b>Last Name</b></label>
																	<input id="last_name" class="form-control" type="text" placeholder="Enter Last Name" name="last_name" value="<?php echo (isset($account['last_name']))? $account['last_name'] : ''; ?>">
																 </div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-5">
																<div class="form-group">
																	<label><b>E-Mail</b></label>
																	<input id="email" class="form-control" type="text" placeholder="Enter E-Mail" name="email" value="<?php echo (isset($account['email']))? $account['email'] : ''; ?>">
																 </div>
															</div>
														</div>
														<div class="row">
															<a href="javascript:void(0)" class="btn btn-success pull-right btn-save-info">Save</a>
														</div>
													</form>
													
												</div>
											</div>
											<div class="tab-pane" id="changePassword">
												
												<form id="form-change-pass">
													</br>
													<div class="row">
														<input id="userID"  type="hidden" name="userID" value="<?php echo (isset($account['id']))? $account['id'] : ''; ?>">
														<div class="col-md-5">
															<div class="form-group">
																<label><b>Username</b></label>
																<input id="username"  type="text" placeholder="Enter Username" class="form-control" name="username" value="<?php echo (isset($account['username']))? $account['username'] : ''; ?>">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-5">
															<div class="form-group">
																<label><b>Old Password</b></label>
																<input id="old_password" type="password" class="form-control" placeholder="Enter Password" name="old_password">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-5">
															<div class="form-group">
																<label><b>New Password</b></label>
																<input id="password" type="password" class="form-control" placeholder="Enter New Password" name="password">
															</div>
														</div>
														<div class="col-md-5">
															<div class="form-group">
																<label><b>Confirm Password</b></label>
																<input id="confirm_password" type="password" class="form-control" placeholder="Enter Password" name="confirm_password">
															</div>
														</div>
													</div>
													<div class="row">
														<a href="javascript:void(0)" class="btn btn-success pull-right btn-save-pass">Save</a>
													</div>
												</form>
											</div>
											<div class="tab-pane" id="changePhoto">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															</br>
															</br>
															<div class="row">
																<form id="myDropZone" class="dropzone heading-home col-md-4 col-md-offset-4" method="POST">
																	<input type="hidden" id="account_id" name="account_id" value="<?php echo (isset($account['id']))? $account['id'] : ''; ?>" />
																	<input type="hidden" name="new_path" value="account" />
																</form>
															</div>
															</br>
															</br>
															<div class="row">
																<a href="javascript:void(0)" class="btn btn-success pull-right btn-upload-photo">Save</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
									<!------------------ END EDIT CONTAINER ---------------------->
								</div><!--/panel content-->
                                    <!------------------------- END HOME  ----------------------------->
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                </div>
            </div>
        </div>
    </div>
</header>

<?php

	include 'modal/successUpdateAccountMessage.php';
	include 'modal/errorGeneralMessage.php';

?>



