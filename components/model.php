<?php
/*
	developer model for common PHP functions
*/

class Model
{
		
	//model = string
	//id = int
	public static function findByPk($model,$id)
	{
		$query = '
				Select * from `'.$model.'` Where id = ?
		';
	
		$params = array($id);
		
		$result = prepareTable($query,$params);
		
		if(!empty($result)){
			return $result[0];
		}
		
		return array();
	}
	
	//model = string
	//arrayParams = array
	public static function findAll($model,$arrayParams = array())
	{
		$where = '';
		$condition ='';
		$params = array();
		$limit = ' limit 1000';
		
		if(!empty($arrayParams)){
				$where =' WHERE ';
		}
		
		
		foreach($arrayParams as $key=>$value){
			
			if(!empty($condition)){
				
				$condition.= ' AND `'.$key.'` in('.implode(',',$value).') ';
				
			}else{
				$condition = ' `'.$key.'` in('.implode(',',$value).') ';
			}
			
		
		}

		$query = '
				Select * from `'.$model.'`
		'.$where.$condition.$limit;
				
		
		$result = prepareTable($query,$params);
		if(!empty($result)){
			
			return $result;
		}
		
		return array();
	}
	
	public static function deleteByPk($model,$id)
	{
		$query[] = '
				Delete from `'.$model.'` Where id = ?
		';
	
		$params[] = array($id);
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
		
	}
	
	public static function deleteAll()
	{
		$where = '';
		$condition ='';
		$params = array();
		
		if(!empty($arrayParams)){
				$where =' WHERE ';
		}
		
		foreach($arrayParams as $key=>$value){
			
			if(!empty($condition)){
				
				$condition.= ' AND `'.$key.'` in('.implode(',',$value).') ';
				
			}else{
				$condition = ' `'.$key.'` in('.implode(',',$value).') ';
			}
			
		}
		
		$query[] = '
				Delete from `'.$model.'`
		'.$where.$condition;
						
		
		if(transactStatement($query,$params)){
			
		}
		
		return array();
	}
}