<?php
require  './extensions/phpmailer/PHPMailerAutoload.php';

class EmailModel{


    function __construct() {
        
    }
	
	
	public static function sendEmail($data)
	{
			
		$return = array();
		$name = $data['name'];
		$email = $data['email'];		
		$subject = $data['subject'];		
		$message = $data['message'];			
		$emailAddress = 'etechqa2017@gmail.com';
		$emailAdmin = 'etechqa2017@gmail.com';                    			
		$password = 'etech2017';			

		$mail = new PHPMailer;						
		$mail->isSMTP();			
		$mail->Host = 'smtp.gmail.com';			
		$mail->Port = 587;			
		$mail->SMTPSecure = 'tls';			
		$mail->SMTPAuth = true;			
		$mail->Username = $emailAddress;			
		$mail->Password = $password;			
		$mail->setFrom('noreply@etech.com', 'Etech Business Solutions');			
		$mail->addReplyTo('noreply@etech.com', 'Etech Business Solutions');			
		$mail->addAddress($emailAdmin);			
		$mail->Subject = $subject;			
		$mail->msgHTML(' <h2>Name : <span>'.$name.'</span></h2><br><h2>Email : <span>'.$email.'</span></h2><br><h2>Message : <span>'.$message.'</span></h2> ');

		//TEST EMAIL BELOW			
		$toInquiryMessage = '	

		
			<!DOCTYPE html>
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			<title>Your Message Subject or Title</title>
			<body style ="background:#eee;">
			<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style ="padding: 40px; max-width:640px;background-color:#f3f3f3;display:block;margin:0 auto; font-family:Lato, sans-serif; color:#626262;font-size:13px;line-height:22px;" width ="600">	
			<tbody>									
			<tr>										
			<td style ="background-color: #c30e0e; padding: 34px 122px;">	
				<h1 width ="600" style ="max-width:600px;width:100%; text-align:center; color:#fff;">Etech Business Solutions</h1>										
			</td>									
			</tr>									
			<tr>										
			<td style ="background-color: #ffffff; text-align: center; padding: 40px;">											
			<h1 style="font-size: 40px; color:#000; font-style: italic;">Thank you!</h1>											
			<br>											
			<p style ="color:#000; font-size: 16px;">Thank you for your interest we will response as soon as possible.</p>											
			<br>											
			<br>										
			</td>									
			</tr>									
			<tr>										
			<td  style ="background-color: #c30e0e; text-align: center; font-size: 16px; color: #fff;">											
			<p>© 2016. All rights reserved. Created by Etech Business Solutions</p>										
			</td>									
			</tr>								
			</tbody>							
			</table>						
			</body>';						
		////////////////		
		if (!$mail->send()) {
				
				$return =array(
					'status' => false,
					'message' => "Mailer Error: " . $mail2->ErrorInfo	
				);
			return $return;
		
		} else {
			$mail2 = new PHPMailer;	
			$mail2->isSMTP();				
			$mail2->Host = 'smtp.gmail.com';				
			$mail2->Port = 587;				
			$mail2->SMTPSecure = 'tls';				
			$mail2->SMTPAuth = true;				
			$mail2->Username = $emailAddress;				
			$mail2->Password = $password;				
			$mail2->setFrom('noreply@etech.com', 'Etech Business Solutions');				
			$mail2->addReplyTo('noreply@etech.com', 'Etech Business Solutions');				
			$mail2->addAddress($email);				
			$mail2->Subject = $subject;				
			$mail2->msgHTML($toInquiryMessage);				
		
			if (!$mail2->send()) {			  					
			
				$return =array(
					'status' => false,
					'message' => "Mailer Error: " . $mail2->ErrorInfo
				);
				return $return;
				
			}else{
				$return =array(
					'status' => true,
				);
				return $return;

			}			
		}	
		
		$return =array(
			'status' => false,
		);
		
		return $return;	
	}
}
