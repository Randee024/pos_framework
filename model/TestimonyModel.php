<?php

class TestimonyModel{

    function __construct() {
        
    }
	
	public static function getAllTestimony()
	{
		$aColumns = array('`name`','`title`','`content`');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";
        if ($_GET['sSearch'] != "") {
            $sWhere = " where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aColumns[$i] !== "" && $_GET["bSearchable_$i"] == "true") {
                    $sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch'] ) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }
        include './lib/language.php';
        $query = "select * from `testimonys`";
        $arr = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));
        $farray = array();
		
        foreach ($arr as $rows) {
			
            $button= '
				<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-edit-testimony" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
				<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-delete-testimony" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
			';
           
            array_push(
				$farray, 
				array(
					$rows['name'],
					$rows['title'],
					$rows['content'],
					$button
				)
			);
        }


        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $count, "iTotalDisplayRecords" => $count, "aaData" => $farray);
        return json_encode($jsonarray);
	}
	
	public static function addTestimony($data)
	{
		// Dev::pvx($data);
		if(isset($data['id_testimony']) && !empty($data['id_testimony'])){
			
			$query[] = '
				UPDATE `testimonys` SET `title` =? ,`content` =? ,`name` =? ,`rating` =? ,`company` =? WHERE `id` =? ;
			';
			
			$params[] = array(
				$data['title'],
				$data['content'],
				$data['name'],
				$data['rating'],
				$data['company'],
				$data['id_testimony'],
			
			);
				
			if(transactStatement($query,$params)){
				return $data['id_testimony'];
			}
			return NULL;
				
		}else{
			$query = '
				INSERT INTO `testimonys` (`title`,`content`,`name`,`rating`,`company`) VALUES(?,?,?,?,?);
			';
			
			$params = array(
					$data['title'],
					$data['content'],
					$data['name'],
					$data['rating'],
					$data['company'],
				
				);
				
			return prepareTable($query,$params);
		}
		
		return NULL;
	}
	
	
	public static function deleteTestimony($id)
	{
		$query[] = '
			DELETE FROM `testimonys` WHERE `id` =?;
			
		';
		
		$params[] = array($id);
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	
	public static function getTestimonyDetails($id)
	{
		$query='
			SELECT 
			t.*,
			f.`path`,
			f.`file_name`
			FROM `testimonys` t
			LEFT JOIN `file_uploads` as f on f.`id` = t.`id_file_upload`
			WHERE

			t.`id` =?;
		';
		
		$params = array($id);
		
		return prepareTable($query,$params);
	}
	
	public static function getAllTestimonials()
	{
		$query='
			SELECT 
			t.*,
			f.`path`,
			f.`file_name`
			FROM `testimonys` t
			LEFT JOIN `file_uploads` as f on f.`id` = t.`id_file_upload`;
		';
		
		$params = array();
		
		return prepareTable($query,$params);
	}
}
