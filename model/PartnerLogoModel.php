<?php

class PartnerLogoModel{

    function __construct() {
        
    }
	
	const STATUS_ACTIVE = 0;
	const STATUS_INACTIVE = 1;
	
	public static function getAllAboutUsFileUploaded()
	{
		$aColumns = array('`id`','`original_file_name`','`status`');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";
        if ($_GET['sSearch'] != "") {
            $sWhere = " where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aColumns[$i] !== "" && $_GET["bSearchable_$i"] == "true") {
                    $sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch'] ) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }
        include './lib/language.php';
        $query = "select * from `partner_logos`";
        $arr = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));
        $farray = array();
		
        foreach ($arr as $rows) {
						
			$status = '<span class="label label-success">Active</span>';
			$button ='';
			if($rows['status'] == self::STATUS_INACTIVE){
				$status = '<span class="label label-danger">Inactive</span>';
				$button ='
					<a href="javascript:void(0)" class="btn btn-sm btn-success btn-activation-logo" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Activate"><i class="glyphicon glyphicon-ok-sign"></i></a>
				';
			}else{
				$button ='
					<a href="javascript:void(0)" class="btn btn-sm btn-warning btn-activation-logo" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Deactivate"><i class="glyphicon glyphicon-remove-sign"></i></a>
				';
			}
			
			
            $button.= '
				<a href="'.$rows['path'].$rows['file_name'].'" class="btn btn-sm btn-info btn-view-logo" data-toggle="tooltip" title="View" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a>
				<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-delete-logo" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
			';
           
            array_push(
				$farray, 
				array(
					$rows['id'],
					$rows['original_file_name'],
					$status,
					$button
				)
			);
        }


        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $count, "iTotalDisplayRecords" => $count, "aaData" => $farray);
        return json_encode($jsonarray);
	}
	
	public static function uploadPartnerLogo($fileName,$path,$originalFileName)
	{
			
		$query[] = '
				Insert into `partner_logos` (`file_name`,`path`,`original_file_name`) VALUES(?,?,?);
		';
		$params[] =array(
			$fileName,
			$path,
			$originalFileName,
		);
		
		
		if(transactStatement($query,$params)){
			
			return true;
			
		}else{
			
			return false;
			
		}
	}
	
	
	public static function deleteLogo($id)
	{
		
		$query[]='
			
			Delete from `partner_logos` where `id` = ?
		
		';
		$params[] = array($id);
		
		
		
		if(transactStatement($query,$params)){
			
			return true;
		}else{
			return false;
		}
		
	}
	
	public static function activationLogo($id)
	{
		$query[]='
			UPDATE `partner_logos` SET `status` = (CASE WHEN status = '.self::STATUS_ACTIVE.' THEN '.self::STATUS_INACTIVE.' ELSE '.self::STATUS_ACTIVE.' END )  WHERE `id` =?  
		';
		
		$params[] = array($id);
		
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	public static function getAllPartnerLogos()
	{
		$query = '
			SELECT * FROM `partner_logos` WHERE `status` = ? ;
		';
		
		$params = array(self::STATUS_ACTIVE);
		
		return prepareTable($query,$params);
	}
}
