<?php

class ServicesModel{

	
	const STATUS_ACTIVE = 0;
	const STATUS_INACTIVE = 1;
	
    function __construct() {
        
    }
	
	function getAllServices() {
		
        $aColumns = array('`title`','`description`','`status`');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";
        if ($_GET['sSearch'] != "") {
            $sWhere = " where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aColumns[$i] !== "" && $_GET["bSearchable_$i"] == "true") {
                    $sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch'] ) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }
        include './lib/language.php';
        $query = "select * from `services`";
        $arr = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));
        $farray = array();
		
        foreach ($arr as $rows) {
			
			$status = '<span class="label label-success">Active</span>';
			
			if($rows['status'] == self::STATUS_INACTIVE){
				$status = '<span class="label label-danger">Inactive</span>';
				$button ='
					<a href="javascript:void(0)" class="btn btn-sm btn-success btn-activation-service" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Activate"><i class="glyphicon glyphicon-ok-sign"></i></a>
				';
			}else{
				$button ='
					<a href="javascript:void(0)" class="btn btn-sm btn-warning btn-activation-service" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Deactivate"><i class="glyphicon glyphicon-remove-sign"></i></a>
				';
			}
			
			
            $button.= '
				<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-edit-service" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
				<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-delete-service" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
			';
           
            array_push(
				$farray, 
				array(
					$rows['title'],
					$rows['description'],
					$status,
					$button
				)
			);
        }


        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $count, "iTotalDisplayRecords" => $count, "aaData" => $farray);
        return json_encode($jsonarray);
    }
	
	public static function uploadServicesIcon($data,$fileName,$path,$originalFileName)
	{
			
		$query[]="
			set @`fileID`:=(SELECT `AUTO_INCREMENT`
				FROM  INFORMATION_SCHEMA.TABLES
				WHERE TABLE_SCHEMA = 'pos_framework'
				AND   TABLE_NAME   = 'file_uploads');
				";
		
		$params[] = array();
		
		$query[] = '
				UPDATE `services` SET `id_file_upload`= (select @`fileID`) WHERE `id`=?;
		';
		$params[] =array($data['id_service']);
		
		
		$query[] = '
				Insert into `file_uploads` (`file_name`,`path`,`original_file_name`) VALUES(?,?,?);
		';
		$params[] =array(
			$fileName,
			$path,
			$originalFileName,
		);
		
		
		
		
		if(transactStatement($query,$params)){
			
			return true;
			
		}else{
			
			return false;
			
		}
	}
	
	public static function addServices($data)
	{
		if(isset($data['id_service']) && !empty($data['id_service'])){
			
			$query[] = '
				UPDATE `services` SET  `title` =? ,`description` =? WHERE `id` =? ;
			';
			
			$params[] = array(
				$data['title'],
				$data['description'],
				$data['id_service'],
			);
			
			if(transactStatement($query,$params)){
				
				return $data['id_service'];
			}
			
			return NULL;
			
		}else{
			$query = '
				INSERT INTO `services` (`title`,`description`) VALUES (?,?);
			';
			
			$params = array(
				$data['title'],
				$data['description'],
			);
			
			return prepareTable($query,$params);
		}
		
		return NULL;
	}
	
	public static function deleteService($id)
	{
		
		$query[] = 'DELETE FROM `services` WHERE `id` = ? ;';
		$params[] = array($id);
		
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	
	public static function getServiceDetails($id)
	{
		$query = '
				SELECT
				s.*,
				fu.`file_name`,
				fu.`path`
				FROM
				
				`services` s
				LEFT JOIN `file_uploads` as fu on fu.`id` = s.`id_file_upload`
				WHERE s.`id` =?
		
		';
		
		$params = array($id);
		
		return prepareTable($query,$params);
	}
	
	
	public static function getAllServicesForFrontPage()
	{
		$query = '
				SELECT
				s.*,
				fu.`file_name`,
				fu.`path`
				FROM
				
				`services` s
				LEFT JOIN `file_uploads` as fu on fu.`id` = s.`id_file_upload`
				WHERE s.`status` = ?
		
		';
		
		$params = array(self::STATUS_ACTIVE);
		
		return prepareTable($query,$params);
	}
	
	public static function activationService($id)
	{
		$query[]='
			UPDATE `services` SET `status` = (CASE WHEN status = '.self::STATUS_ACTIVE.' THEN '.self::STATUS_INACTIVE.' ELSE '.self::STATUS_ACTIVE.' END )  WHERE `id` =?  
		';
		
		$params[] = array($id);
		
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	
}
