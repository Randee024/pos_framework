<?php

class FaqModel{

	
	const STATUS_ACTIVE = 0;
	const STATUS_INACTIVE = 1;
	
	
	const CATEGORY_ADMIN = 1;
	const CATEGORY_CUSTOMER= 2;
	const CATEGORY_CONTRACT = 3;
	
    function __construct() {
        
    }
	
	function getAllFaqs() {
		
        $aColumns = array('`title`','`content`','`status`','`category`');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";
        if ($_GET['sSearch'] != "") {
            $sWhere = " where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aColumns[$i] !== "" && $_GET["bSearchable_$i"] == "true") {
                    $sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch'] ) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }
        include './lib/language.php';
        $query = "select * from `faqs`";
        $arr = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));
        $farray = array();
		
        foreach ($arr as $rows) {
            $button = '
				<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-edit-faq" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
				<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-delete-faq" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
			';
           
			$category = '';
			
			
			if($rows['category'] == self::CATEGORY_ADMIN){
				$category = 'Administrator Settings';
			}else if($rows['category'] == self::CATEGORY_CUSTOMER){
				$category = 'Customer Support';
			}else{
				$category = 'Contract and Policy';
			}
			
			
            array_push(
				$farray, 
				array(
					$rows['title'],
					$rows['content'],
					$category,
					$button
				)
			);
        }


        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $count, "iTotalDisplayRecords" => $count, "aaData" => $farray);
        return json_encode($jsonarray);
    }
	
	public static function addFaq($data)
	{
		
		if(isset($data['id_faq']) && !empty($data['id_faq'])){
			$query[] = '
				UPDATE `faqs` SET `title`=?,`content`=?, `category` =? WHERE `id` = ?
			';
			
			$params[] = array(
				$data['title'],
				$data['content'],
				$data['category'],
				$data['id_faq']
			);
			
			if(transactStatement($query,$params)){
				
				return $data['id_faq'];
				
			}else{
				return NULL;
			}
			
		}else{
			$query = '
				INSERT INTO `faqs` (`title`,`content`,`category`) VALUES(?,?,?)
			';
			
			$params = array(
				$data['title'],
				$data['content'],
				$data['category'],
			);
			return prepareTable($query,$params);
		}
		
		return NULL;
	}
	
	public static function getFaqDetails($id)
	{
		$query = '
				SELECT
				f.*,
				fu.`file_name`,
				fu.`path`
				FROM
				
				`faqs` f
				LEFT JOIN `file_uploads` as fu on fu.`id` = f.`id_file_upload`
				WHERE f.`id` =?
		
		';
		
		$params = array($id);
		
		return prepareTable($query,$params);
	}
	
	public static function deleteFaq($id)
	{
		$query[] = '
			DELETE FROM `faqs` WHERE `id` = ?
		';
		$params[] = array($id);
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	
	public static function uploadFaqImages($data,$fileName,$path,$originalFileName)
	{
			
			// Dev::pvx($data);
		$query[]="
			set @`fileID`:=(SELECT `AUTO_INCREMENT`
				FROM  INFORMATION_SCHEMA.TABLES
				WHERE TABLE_SCHEMA = 'pos_framework'
				AND   TABLE_NAME   = 'file_uploads');
				";
		
		$params[] = array();
		
		$query[] = '
				UPDATE `faqs` SET `id_file_upload`= (select @`fileID`) WHERE `id`=?;
		';
		$params[] =array($data['id_faq']);
		
		
		$query[] = '
				Insert into `file_uploads` (`file_name`,`path`,`original_file_name`) VALUES(?,?,?);
		';
		$params[] =array(
			$fileName,
			$path,
			$originalFileName,
		);
		
		
		
		
		if(transactStatement($query,$params)){
			
			return true;
			
		}else{
			
			return false;
			
		}
	}
	
	public static function getAllFaqAdmin()
	{
		$query = '
				SELECT
				f.*,
				fu.`file_name`,
				fu.`path`
				FROM
				
				`faqs` f
				LEFT JOIN `file_uploads` as fu on fu.`id` = f.`id_file_upload`
				WHERE f.`category` =?
		';
		
		$params = array(self::CATEGORY_ADMIN);
		
		return prepareTable($query,$params);
	}
	
	public static function getAllFaqCustomer()
	{
		$query = '
				SELECT
				f.*,
				fu.`file_name`,
				fu.`path`
				FROM
				
				`faqs` f
				LEFT JOIN `file_uploads` as fu on fu.`id` = f.`id_file_upload`
				WHERE f.`category` =?
		';
		
		$params = array(self::CATEGORY_CUSTOMER);
		
		return prepareTable($query,$params);
	}
	public static function getAllFaqContract()
	{
		$query = '
				SELECT
				f.*,
				fu.`file_name`,
				fu.`path`
				FROM
				
				`faqs` f
				LEFT JOIN `file_uploads` as fu on fu.`id` = f.`id_file_upload`
				WHERE f.`category` =?
		';
		
		$params = array(self::CATEGORY_CONTRACT);
		
		return prepareTable($query,$params);
	}
}
