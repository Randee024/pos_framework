<?php

class FileuploadModel{

    function __construct() {
        
    }
	
	public static function uploadPageFiles($data,$fileName,$path,$originalFileName)
	{
			
		$query[]="
			set @`fileID`:=(SELECT `AUTO_INCREMENT`
				FROM  INFORMATION_SCHEMA.TABLES
				WHERE TABLE_SCHEMA = 'pos_framework'
				AND   TABLE_NAME   = 'file_uploads');
				";
		
		$params[] = array();
		
		$query[] = '
				UPDATE `page_has_edits` SET `id_file_upload`= (select @`fileID`) WHERE `id`=?;
		';
		$params[] =array($data['page_edit_id']);
		
		
		$query[] = '
				Insert into `file_uploads` (`file_name`,`path`,`original_file_name`) VALUES(?,?,?);
		';
		$params[] =array(
			$fileName,
			$path,
			$originalFileName,
		);
		
		
		
		
		if(transactStatement($query,$params)){
			
			return true;
			
		}else{
			
			return false;
			
		}
	}
	
	public static function accountFileUpload($data,$fileName,$path,$originalFileName)
	{
			
		$query[]="
			set @`fileID`:=(SELECT `AUTO_INCREMENT`
				FROM  INFORMATION_SCHEMA.TABLES
				WHERE TABLE_SCHEMA = 'pos_framework'
				AND   TABLE_NAME   = 'file_uploads');
				";
		
		$params[] = array();
		
		$query[] = '
				UPDATE `accounts` SET `id_file_upload`= (select @`fileID`) WHERE `id`=?;
		';
		$params[] =array($data['account_id']);
		
		
		$query[] = '
				Insert into `file_uploads` (`file_name`,`path`,`original_file_name`) VALUES(?,?,?);
		';
		$params[] =array(
			$fileName,
			$path,
			$originalFileName,
		);
		
		
		
		
		if(transactStatement($query,$params)){
			unset($_SESSION['user_account']);
				
			$_SESSION['user_account'] = AccountModel::getAccountDetails($data['account_id']);
				
			return true;
			
		}else{
			
			return false;
			
		}
	}
	
	public static function clientPictureUpload($data,$fileName,$path,$originalFileName)
	{
			
		$query[]="
			set @`fileID`:=(SELECT `AUTO_INCREMENT`
				FROM  INFORMATION_SCHEMA.TABLES
				WHERE TABLE_SCHEMA = 'pos_framework'
				AND   TABLE_NAME   = 'file_uploads');
				";
		
		$params[] = array();
		
		$query[] = '
				UPDATE `testimonys` SET `id_file_upload`= (select @`fileID`) WHERE `id`=?;
		';
		$params[] =array($data['id_testimony']);
		
		
		$query[] = '
				Insert into `file_uploads` (`file_name`,`path`,`original_file_name`) VALUES(?,?,?);
		';
		$params[] =array(
			$fileName,
			$path,
			$originalFileName,
		);
		
		
		
		
		if(transactStatement($query,$params)){
			
			return true;
			
		}else{
			
			return false;
			
		}
	}
	
	
	public static function deleteFile($id)
	{
		
		
		$query[]='
			
			Delete from `account_has_files` where `id_file` = ?
		
		';
		$params[] = array($id);
		
		$query[]='
			
			Delete from `files` where `id` = ?
		
		';
		$params[] = array($id);
		
		
		
		if(transactStatement($query,$params)){
			
			return true;
		}else{
			return false;
		}
		
	}
	
	public static function getFileDetails($id)
	{
		
		$query = '
			SELECT
			*
			FROM
			`file_uploads`
			WHERE
			`id` = ?
		';
		
		$params = array($id);
		
		$result= prepareTable($query,$params);
		
		return $result[0];
	}
}
