<?php

class MapModel{

	
	const STATUS_ACTIVE = 0;
	const STATUS_INACTIVE = 1;
	
    function __construct() {
        
    }
	
	function getAllMap() {
		
        $aColumns = array('`id`','`latitude`','`longitude`','`status`');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";
        if ($_GET['sSearch'] != "") {
            $sWhere = " where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aColumns[$i] !== "" && $_GET["bSearchable_$i"] == "true") {
                    $sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch'] ) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }
        include './lib/language.php';
        $query = "select * from `maps`";
        $arr = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));
        $farray = array();
		
        foreach ($arr as $rows) {
			
			$status = '<span class="label label-success">Active</span>';
			$button ='';
			
			if($rows['status'] == self::STATUS_INACTIVE){
				$status = '<span class="label label-danger">Inactive</span>';
				$button ='
					<a href="javascript:void(0)" class="btn btn-sm btn-success btn-activation-map" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Activate"><i class="glyphicon glyphicon-ok-sign"></i></a>
				';
			}else{
				$button ='
					<a href="javascript:void(0)" class="btn btn-sm btn-warning btn-activation-map" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Deactivate"><i class="glyphicon glyphicon-remove-sign"></i></a>
				';
			}
			
			
            $button.= '
				<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-edit-map" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
				<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-delete-map" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
			';
           
            array_push(
				$farray, 
				array(
					$rows['id'],
					$rows['latitude'],
					$rows['longitude'],
					$status,
					$button
				)
			);
        }


        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $count, "iTotalDisplayRecords" => $count, "aaData" => $farray);
        return json_encode($jsonarray);
    }
	
	public static function addMap($data)
	{
		$query =array();
		$params =array();
		
		if(isset($data['id_map']) && !empty($data['id_map'])){
			$query[] = '
				UPDATE `maps` SET `longitude`=?,`latitude`=? WHERE `id` = ?
			';
			
			$params[] = array(
				$data['longitude'],
				$data['latitude'],
				$data['id_map']
			);
			
		}else{
			$query[] = '
				INSERT INTO `maps` (`longitude`,`latitude`) VALUES(?,?)
			';
			
			$params[] = array(
				$data['longitude'],
				$data['latitude'],
			);
			
		}
		
		if(transactStatement($query,$params)){
			
			return true;
			
		}else{
			return false;
		}
	}
	
	public static function deleteMap($id)
	{
		$query[] = '
			DELETE FROM `maps` WHERE `id` = ?
		';
		$params[] = array($id);
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	
	public static function getMapDetails($id)
	{
		$query = '
			SELECT * FROM `maps` WHERE `id` =? ;
		';
		$params = array($id);
		
		return prepareTable($query,$params);
	}
	
	public static function activationMap($id)
	{
		$query[]='
			UPDATE `maps` SET `status` = '.self::STATUS_INACTIVE.' WHERE `status` ='.self::STATUS_ACTIVE.'  
		';
		
		$params[] = array();
		
		$query[]='
			UPDATE `maps` SET `status` = (CASE WHEN status = '.self::STATUS_ACTIVE.' THEN '.self::STATUS_INACTIVE.' ELSE '.self::STATUS_ACTIVE.' END )  WHERE `id` =?  
		';
		
		$params[] = array($id);
		
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	
	public static function getMapCoordinates()
	{
		$query = '
			SELECT * FROM `maps` WHERE `status` = ?;
		';
		$params = array(self::STATUS_ACTIVE);
		
		$result = prepareTable($query,$params);
		
		if(!empty($result)){
			return $result[0];
		}
		
		return NULL;
		
	}
}
