<?php

class PageModel{

	const PAGE_HOME = 1;
	const PAGE_SERVICES = 2;
	const PAGE_ABOUT_US = 3;
	const PAGE_FAQ = 4;
	const PAGE_CONTACT_US = 5;
	
	const HEADING = 0;
	const CONTAINER_1 = 1;
	const CONTAINER_2 = 2;
	const CONTAINER_3 = 3;
	const CONTAINER_4 = 4;
	const TESTIMONY = 5;
	
	const STATUS_ACTIVE = 0;
	const STATUS_INACTIVE = 1;
	
    function __construct() {
        
    }

	public static function getHomePageDetails()
	{
		
		$query = '
		
			SELECT
			
			*
			FROM
			`page_has_edits`
			WHERE
			`id_page` = ?
			AND `status` = ?
 		';
		
		$params = array(self::PAGE_HOME,self::STATUS_ACTIVE);
		
		
		return prepareTable($query,$params);
	}
	
	public static function getServicesPageDetails()
	{
		
		$query = '
		
			SELECT
			
			*
			FROM
			`page_has_edits`
			WHERE
			`id_page` = ?
			AND `status` = ?
 		';
		
		$params = array(self::PAGE_SERVICES,self::STATUS_ACTIVE);
		
		
		return prepareTable($query,$params);
	}
	public static function getAboutUsPageDetails()
	{
		
		$query = '
		
			SELECT
			
			*
			FROM
			`page_has_edits`
			WHERE
			`id_page` = ?
			AND `status` = ?
 		';
		
		$params = array(self::PAGE_ABOUT_US,self::STATUS_ACTIVE);
		
		
		return prepareTable($query,$params);
	}
	public static function getFaqPageDetails()
	{
		
		$query = '
		
			SELECT
			
			*
			FROM
			`page_has_edits`
			WHERE
			`id_page` = ?
			AND `status` = ?
 		';
		
		$params = array(self::PAGE_FAQ,self::STATUS_ACTIVE);
		
		
		return prepareTable($query,$params);
	}
	
	public static function getAllContactUsEdits()
	{
		
		$query = '
		
			SELECT
			
			*
			FROM
			`page_has_edits`
			WHERE
			`id_page` = ?
			AND `status` = ?
 		';
		
		$params = array(self::PAGE_CONTACT_US,self::STATUS_ACTIVE);
		
		
		return prepareTable($query,$params);
	}
	
	public static function UpdateFixPage($data)
	{
		
		if(isset($data['id_edit']) && !empty($data['id_edit'])){
			
			$query = array();
			$params = array();
			
			if(isset($data['bg_color'])){
				$query[] = '
					UPDATE `page_has_edits` SET `title` = ? ,`sub_title` = ? , `bg_color` = ? WHERE `id` = ?;
				';
				
				$params[] = array(
						$data['title'],
						$data['sub_title'],
						$data['bg_color'],
						$data['id_edit'],
						
					);
			}else if(isset($data['button_color'])){
				
				$query[] = '
					UPDATE `page_has_edits` SET `title` = ? ,`sub_title` = ? , `button_color` = ? , `button_hover_color` = ? WHERE `id` = ?;
				';
				$params[] = array(
						$data['title'],
						$data['sub_title'],
						$data['button_color'],
						$data['button_hover_color'],
						$data['id_edit'],
					);
					
			}else{
				$query[] = '
					UPDATE `page_has_edits` SET `title` = ? ,`sub_title` = ? WHERE `id` = ?;
				';
				
				$params[] = array(
						$data['title'],
						$data['sub_title'],
						$data['id_edit'],
					);
			}
			if(transactStatement($query,$params)){
				
				return $data['id_edit'];
				
			}else{
				return NULL;
			}
			
		}else{
			
			$query = '';
			$params = array();
			
			
			if(isset($data['bg_color'])){
				$query = '
					INSERT INTO `page_has_edits` (`id_page`,`id_child`,`title`,`sub_title`,`bg_color`) VALUES(?,?,?,?,?);
				';
				
				$params = array(
						$data['id_page'],
						$data['id_child'],
						$data['title'],
						$data['sub_title'],
						$data['bg_color'],
						
					);
			}else if(isset($data['button_color'])){
				
				$query = '
					INSERT INTO `page_has_edits` (`id_page`,`id_child`,`title`,`sub_title`,`button_color`,`button_hover_color`) VALUES(?,?,?,?,?,?);
				';
				$params = array(
						$data['id_page'],
						$data['id_child'],
						$data['title'],
						$data['sub_title'],
						$data['button_color'],
						$data['button_hover_color'],
					);
					
			}else{
				$query='
					INSERT INTO `page_has_edits` (`id_page`,`id_child`,`title`,`sub_title`) VALUES(?,?,?,?);
				';
				
				$params = array(
						$data['id_page'],
						$data['id_child'],
						$data['title'],
						$data['sub_title'],
						
					);
			}
				
			return prepareTable($query,$params);
		}
		
		
	}
	
	
	public static function UpdateContactUsContent($data)
	{
		$query =array();
		$params =array();
		
		if(isset($data['id_page_edit']) && !empty($data['id_page_edit'])){
			
			$query[]= '
				UPDATE `page_has_edits` SET `sub_title`=? WHERE `id` =?;
			';
			
			$params[] = array(
				$data['content'],
				$data['id_page_edit'],
			);
			
		}else{
			
			$query[]= '
				INSERT INTO `page_has_edits` (`id_page`,`id_child`,`sub_title`) VALUES(?,?,?);
			';
			
			$params[] = array(
				$data['id_page'],
				$data['id_child'],
				$data['content'],
			);
			
		}
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	public static function addLogoTitle($data)
	{
		$query =array();
		$params =array();
		
		if(isset($data['id_page_edit']) && !empty($data['id_page_edit'])){
			
			$query[]= '
				UPDATE `page_has_edits` SET `title`=? WHERE `id` =?;
			';
			
			$params[] = array(
				$data['title'],
				$data['id_page_edit'],
			);
			
		}else{
			
			$query[]= '
				INSERT INTO `page_has_edits` (`id_page`,`id_child`,`title`) VALUES(?,?,?);
			';
			
			$params[] = array(
				$data['id_page'],
				$data['id_child'],
				$data['title'],
			);
			
		}
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	public static function getAllHomeEdits()
	{
		$query= '
				
			SELECT
			*
			FROM
			`page_has_edits`
			
			Where `id_page` = ?
		';
		
		$params = array(self::PAGE_HOME);
		
		return prepareTable($query,$params);
	}
	public static function getAllServicesEdits()
	{
		$query= '
				
			SELECT
			*
			FROM
			`page_has_edits`
			
			Where `id_page` = ?
		';
		
		$params = array(self::PAGE_SERVICES);
		
		return prepareTable($query,$params);
	}
	public static function getAllAboutUsEdits()
	{
		$query= '
				
			SELECT
			*
			FROM
			`page_has_edits`
			
			Where `id_page` = ?
		';
		
		$params = array(self::PAGE_ABOUT_US);
		
		return prepareTable($query,$params);
	}
	
	public static function getAllFaqEdits()
	{
		$query= '
				
			SELECT
			*
			FROM
			`page_has_edits`
			
			Where `id_page` = ?
		';
		
		$params = array(self::PAGE_FAQ);
		
		return prepareTable($query,$params);
	}

}
