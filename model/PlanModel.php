<?php

class PlanModel{

	
	const STATUS_ACTIVE = 0;
	const STATUS_INACTIVE = 1;
	
    function __construct() {
        
    }
	public static function getAllPricingList()
	{
		
		$query = '
			SELECT
			*
			FROM
			`plans`
			WHERE `status` = ?
		';
		
		$params = array(self::STATUS_ACTIVE);
		
		return prepareTable($query,$params);
	}
	
	function getPricingDetails() {
		
        $aColumns = array('`id`','`title`','`description`','`pricing`','`status`');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";
        if ($_GET['sSearch'] != "") {
            $sWhere = " where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aColumns[$i] !== "" && $_GET["bSearchable_$i"] == "true") {
                    $sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch'] ) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }
        include './lib/language.php';
        $query = "select * from `plans`";
        $arr = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));
        $farray = array();
		
        foreach ($arr as $rows) {
			
			$status = '<span class="label label-success">Active</span>';
			
			if($rows['status'] == self::STATUS_INACTIVE){
				$status = '<span class="label label-danger">Inactive</span>';
				$button ='
					<a href="javascript:void(0)" class="btn btn-sm btn-success btn-activation-plan" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Activate"><i class="glyphicon glyphicon-ok-sign"></i></a>
				';
			}else{
				$button ='
					<a href="javascript:void(0)" class="btn btn-sm btn-warning btn-activation-plan" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Deactivate"><i class="glyphicon glyphicon-remove-sign"></i></a>
				';
			}
			
			
            $button.= '
				<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-edit-plan" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
				<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-delete-plan" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
			';
           
            array_push(
				$farray, 
				array(
					$rows['id'],
					$rows['title'],
					$rows['description'],
					$rows['pricing'],
					$status,
					$button
				)
			);
        }


        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $count, "iTotalDisplayRecords" => $count, "aaData" => $farray);
        return json_encode($jsonarray);
    }
	
	public static function addPlan($data)
	{
		$query=array();
		$params=array();
		
		if(isset($data['id_plan']) && !empty($data['id_plan'])){
			$query[]='
				UPDATE `plans` SET `title`=? , `description`=? ,`pricing`=? WHERE `id`=?;
			';
			$params[]=array(
					$data['title'],
					$data['description'],
					$data['pricing'],
					$data['id_plan'],
			
			);
		}else{
			$query[]='
				INSERT INTO `plans` (`title`,`description`,`pricing`)VALUES(?,?,?);
			';
			$params[]=array(
					$data['title'],
					$data['description'],
					$data['pricing'],
			
			);
		}
		
		
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	
	public static function deletePlan($id)
	{
		$query[]='
			DELETE FROM `plans` WHERE `id`=?;
		';
		
		$params[] = array($id);
		
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	
	public static function activationPlan($id)
	{
		$query[]='
			UPDATE `plans` SET `status` = (CASE WHEN status = '.self::STATUS_ACTIVE.' THEN '.self::STATUS_INACTIVE.' ELSE '.self::STATUS_ACTIVE.' END )  WHERE `id` =?  
		';
		
		$params[] = array($id);
		
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
	
	public static function getPlanDetails($id)
	{
		$query = '
			SELECT * FROM `plans` WHERE `id`=?;
		';
		$params = array($id);
		
		return prepareTable($query,$params);
		
	}
}
