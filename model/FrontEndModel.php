<?php

class FrontEndModel{

    function __construct() {
        
    }
	public static function getAllSampleFrontEnd()
	{
		$aColumns = array('`id`','`original_file_name`');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        $sWhere = "";
        if ($_GET['sSearch'] != "") {
            $sWhere = " where (";
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aColumns[$i] !== "" && $_GET["bSearchable_$i"] == "true") {
                    $sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch'] ) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }
        include './lib/language.php';
        $query = "select * from `front_ends`";
        $arr = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));
        $farray = array();
		
        foreach ($arr as $rows) {
			
            $button= '
				<a href="'.$rows['path'].$rows['file_name'].'" class="btn btn-sm btn-info btn-view-front-end" data-toggle="tooltip" title="View" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a>
				<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-delete-front-end" data-id="'.$rows['id'].'" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
			';
           
            array_push(
				$farray, 
				array(
					$rows['id'],
					$rows['original_file_name'],
					$button
				)
			);
        }


        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $count, "iTotalDisplayRecords" => $count, "aaData" => $farray);
        return json_encode($jsonarray);
	}
	public static function uploadFrontEndDesign($data,$fileName,$path,$originalFileName)
	{

		$query[] = '
				Insert into `front_ends` (`file_name`,`path`,`original_file_name`) VALUES(?,?,?);
		';
		$params[] =array(
			$fileName,
			$path,
			$originalFileName,
		);
		
		if(transactStatement($query,$params)){
			
			return true;
			
		}else{
			
			return false;
			
		}
	}
	
	public static function deleteFrontEndDesign($id)
	{
		$query[] = '
			Delete FROM `front_ends` WHERE `id` = ?
		';
		$params[] = array($id);
		
		if(transactStatement($query,$params)){
			return true;
		}
		
		return false;
	}
}
