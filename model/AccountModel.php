<?php

class AccountModel {

	const TYPE_SUPER_ADMIN = 0;
	const TYPE_ADMIN = 0;
	
    function __construct() 
	{
        
    }

    public function createAccount($data) 
	{
		
		if(isset($data['userID']) && !empty($data['userID'])){
			
			if(isset($data['first_name']) && !empty($data['first_name'])){

				$query[] ='
					UPDATE `accounts` SET `first_name`=? , `last_name` =? , `email` =? WHERE `id` = ?;
				';
				
				$params[] = array(
					$data['first_name'],
					$data['last_name'],
					$data['email'],
					$data['userID'],
				);
				
			}else{
				
				$query[] ='
					UPDATE `accounts` SET `username`=? , `password`=? WHERE `id` = ?;
				';
				
				$params[] = array(
					$data['username'],
					$data['password'],
					$data['userID'],
				);
			
			}
			
			if(transactStatement($query,$params)){
				
				unset($_SESSION['user_account']);
				
				$_SESSION['user_account'] = self::getAccountDetails($data['userID']);
				
				return array('status'=>true);
			}
			
			return false;
			
		}else{
			
			$query ='
				INSERT into `accounts` (`first_name`,`last_name`,`email`,`username`,`password`) VALUES (?,?,?,?,?);
			';
			
			$params = array(
				$data['first_name'],
				$data['last_name'],
				$data['email'],
				$data['username'],
				$data['password']
				);
				
			$result = prepareTable($query,$params);
			
			return $result;
		}
		
		
    }
	public function authenticate($data)
	{
				
		$result = prepareTable('
			SELECT * from  accounts Where username = ? AND password = ? ',
			array(
				$data['username'],
				$data['password']
			));


		if(!empty($result)) {
				
				$newArray = array();
				
				foreach($result as $key =>$data){
					
					$newArray = $data;
					
				}
				

				$_SESSION['user_account'] = $newArray;
				$_SESSION['authenticated'] = true;
	
				if(isset($_POST['remember'])){
					
					setcookie ('username',$_POST['username'],time()+3600,'/','localhost');
					setcookie ('password',$_POST['password'],time()+3600,'/','localhost');
					
				}else{
					
					if(isset($_COOKIE['username'])) {
						
						setcookie ('username','');
						
					}
					
					if(isset($_COOKIE['password'])) {
						
						setcookie ('password','');
						
					}
				}
					
			$return['auth'] = true;
			
		} else {
			$return['auth'] = false;
		}

		
	
		echo json_encode($return);
		exit();
		
	}
	
	public function getAccountDetails($id)
	{
		$query = '
			SELECT
			*
			FROM
			`accounts`
			Where `id` =? 
		';
		$params = array($id);
		
		$result = prepareTable($query, $params);
		
		return $result[0];
	}
	
	public function deleteAccount($id)
	{
		$query = '
			DELETE FROM accounts WHERE id=? 
		';
		$params = array($id);
		
		$result = prepareTable($query, $params);
		
		return $result[0];
	}
	
	public function currentUser()
	{
		return $_SESSION['user_account'];
	}
	
	public function changePassword($data)
	{
		$query[] = '
			UPDATE
			`accounts`
			set `password`=?
			
			Where `id`=?
			
		';
		$params[] = array($data['new_password'],$data['userID']);
		
		if(transactStatement($query,$params)){
			return true;
		}else{
			return false;
		}
		
	}
	
	public static function checkMatchPassword($data)
	{
		$query = '
			SELECT
			`id`
			from `accounts`
			
			where `id` = ?
			and `password` = ?
		';
		$params = array(
			$data['userID'],
			$data['old_password']
		);
		
		$result = prepareTable($query,$params);
		
		if(!empty($result)){
			return true;
		}
		
		return false;
	}
	
	
	
	
}
