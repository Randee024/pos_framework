<div id="deleteFaq" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content width-100">
			<div class="modal-header alert-danger">
				<h4 class="modal-title">Warning</h4>
			</div>
			<div class="modal-body">
				 <div class="row">
					<div class="col-md-12">
						<form id="delete-faq-form">
							<input type="hidden" id="id_faq" name="id_faq"/>
						</form>
						<div class="row">
							<div class="col-md-7">
								<p>
									<span>Are you sure you want to delete this FAQ?</span>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="javascript:void(0)" class="btn btn-success btn-delete-confirm-faq"><i class="glyphicon glyphicon-ok"></i>Ok</a>
				<a class="btn btn-danger"  data-dismiss="modal" ><i class="glyphicon glyphicon-remove-sign"></i>Cancel</a>
			</div>
		</div>

	</div>
</div>