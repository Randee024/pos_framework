<div id="addSampleFrontEnd" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content width-100">
            <div class="modal-header modal-design">
                <h4 class="modal-title">Add Sample Front End</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="home-front-end-upload">
                            <form id="myDropZone6" class="dropzone front-end-home" method="POST">
                                <input type="hidden" name="new_path" value="sample_front_end" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="pull-right">
                        <a class="btn btn-danger" data-dismiss="modal" ><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                        <a href="javascript:void(0)" class="btn btn-success btn-front-end"><i class="glyphicon glyphicon-floppy-save"></i> Save</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>