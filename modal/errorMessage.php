<div id="errorMessage" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content width-100">
      <div class="modal-header alert-danger">
        <h4 class="modal-title">Warning!</h4>
      </div>
      <div class="modal-body">
		<p><span id="error_message">Error</span></p>
      </div>
      <div class="modal-footer">
        <a href="/edit_home" class="btn btn-primary"><i class="glyphicon glyphicon-ok-sign"></i>Ok</a>
      </div>
    </div>

  </div>
</div>