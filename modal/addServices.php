<div id="addServices" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content width-100">
			<div class="modal-header modal-design">
				<h4 class="modal-title">Add Services</h4>
			</div>
			<div class="modal-body">
				 <div class="row">
					<div class="col-md-12">
						<form class="form form-vertical" id="form-service">
							<input type="hidden" name="id_service" id="id_service"/>
							<div class="control-group">
								<label>Title</label>
								<div class="controls">
									<input type="text" name="title" id="title" class="form-control" placeholder="Enter Title">
								</div>
							</div>
							<div class="control-group">
								<label>Description</label>
								<div class="controls">
									<textarea name="description" id="description" class="form-control" rows="4" style="resize:none;"></textarea>
								</div>
							</div>
						</form>
						</br>
						<div class="row">
							<div class="col-md-5">
								
									<div id="service-upload">
										<form id="myDropZone2" class="dropzone service" method="POST">
											<input type="hidden" id="id_service" name="id_service" value="" />
											<input type="hidden" name="new_path" value="client" />
											
										</form>
										</br>
										<a href="javascript:void(0)" class="btn btn-default cancel-service-image hidden">Cancel</a>
									</div>

									<div id="service-image" class="hidden">
										<div id="service_icon"><img class="img-responsive" alt="" ></div>
										</br>
										<a href="javascript:void(0)" class="btn btn-danger change-service-image">Change Icon</a>
									</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
                <div class="form-actions">
                    <div class="pull-right">
                        <a class="btn btn-danger" data-dismiss="modal" ><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                        <a href="javascript:void(0)" class="btn btn-success btn-save-service"><i class="glyphicon glyphicon-floppy-save"></i>Save</a>
                    </div>
                </div>
            </div>
		</div>

	</div>
</div>