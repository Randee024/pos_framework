<div id="testimonyModal" class="modal fade" data-wow-delay="1s" role="dialog" d>
	<div class="modal-dialog modal-lg">

    <!-- Modal content-->
		<div class="modal-content1">
		  <div class="modal-body">
			  <div class="container">
					<div class="row">
						<div id="carousel-reviews" class="carousel slide" data-ride="carousel">

							<div class="carousel-inner">
								<div class="item active">
									
									<?php 
										if(!empty($testimonials)){
											foreach($testimonials as $key=>$value)
											{
												$clientImage = array();
												
												if(!empty($value) && isset($value['id_file_upload'])){
													$clientImage = $fileModel->getFileDetails($value['id_file_upload']);
												}
												$ratings = $value['rating'];
												$title= 'Great Service!';
												$subTitle= 'Never before has there been a good film portrayal of ancient Greece\'s favourite myth. 
												So why would Hollywood start now? This latest attempt at bringing the son of Zeus to the big screen is brought to us 
												by X-Men: The last Stand director Brett Ratner. If the name of the director wasn\'t enough to dissuade ...';
												
												if(!empty($value['title'])){
													
													$title = $value['title'];
												}
												
												if(!empty($value['content'])){
													
													$subTitle = $value['content'];
												}
												
												$displayRatings = '<span class="rating-input">';
												for($ctr = 0 ; $ctr < 5 ; $ctr++)
												{
													if($ctr < $ratings){
														$displayRatings.= '<span class="glyphicon glyphicon-star"></span>';
													}else{
														$displayRatings.= '<span class="glyphicon glyphicon-star-empty"></span>';
													}
													
												}
												$displayRatings.= '</span>';
												echo '
													<div class="col-md-4 col-sm-6 wow fadeIn" data-wow-delay="1s">
														<div class="block-text rel zmin">
															<a title="" href="#">'.$title.'</a>
															<div class="mark">
																My rating: '.$displayRatings.'
															</div>
															<p>'.$subTitle.'</p>
															<ins class="ab zmin sprite sprite-i-triangle block"></ins>
														</div>
														<div class="person-text rel">
															<img class="img-responsive" src="'.$clientImage['path'].$clientImage['file_name'].'"/>
															<a title="" href="#">'.$value['name'].'</a>
															<i class="text-black">'.$value['company'].'</i>
														</div>
													</div>
												';
											}
											
										
										}else{
									?>
										
										
										<div class="col-md-4 col-sm-6 wow fadeIn" data-wow-delay="1s">
											<div class="block-text rel zmin">
												<a title="" href="#">Great Service!</a>
												<div class="mark">
													My rating: <span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span>
												</div>
												<p>Never before has there been a good film portrayal of ancient Greece\'s favourite myth. 
												So why would Hollywood start now? This latest attempt at bringing the son of Zeus to the big screen is brought to us 
												by X-Men: The last Stand director Brett Ratner. If the name of the director wasn\'t enough to dissuade ...</p>
												<ins class="ab zmin sprite sprite-i-triangle block"></ins>
											</div>
											<div class="person-text rel">
												<img class="img-responsive" src="/images/blank-user.jpg"/>
												<a title="" href="#">Name</a>
												<i class="text-black">Address</i>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 wow fadeIn" data-wow-delay="1s">
											<div class="block-text rel zmin">
												<a title="" href="#">Great Service!</a>
												<div class="mark">
													My rating: <span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span>
												</div>
												<p>Never before has there been a good film portrayal of ancient Greece\'s favourite myth. 
												So why would Hollywood start now? This latest attempt at bringing the son of Zeus to the big screen is brought to us 
												by X-Men: The last Stand director Brett Ratner. If the name of the director wasn\'t enough to dissuade ...</p>
												<ins class="ab zmin sprite sprite-i-triangle block"></ins>
											</div>
											<div class="person-text rel">
												<img class="img-responsive" src="/images/blank-user.jpg"/>
												<a title="" href="#">Name</a>
												<i class="text-black">Address</i>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 wow fadeIn" data-wow-delay="1s">
											<div class="block-text rel zmin">
												<a title="" href="#">Great Service!</a>
												<div class="mark">
													My rating: <span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span>
												</div>
												<p>Never before has there been a good film portrayal of ancient Greece\'s favourite myth. 
												So why would Hollywood start now? This latest attempt at bringing the son of Zeus to the big screen is brought to us 
												by X-Men: The last Stand director Brett Ratner. If the name of the director wasn\'t enough to dissuade ...</p>
												<ins class="ab zmin sprite sprite-i-triangle block"></ins>
											</div>
											<div class="person-text rel">
												<img class="img-responsive" src="/images/blank-user.jpg"/>
												<a title="" href="#">Name</a>
												<i class="text-black">Address</i>
											</div>
										</div>
										
										<?php } ?>
								</div>                  
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
