<div id="addFaq" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content width-100">
			<div class="modal-header modal-design">
				<h4 class="modal-title">Add FAQ</h4>
			</div>
			<div class="modal-body">
				 <div class="row">
					<div class="col-md-12">
						<form class="form form-vertical" id="form-faq">
							<input type="hidden" name="id_faq" id="id_faq"/>
							<div class="control-group">
								<label>Category</label>
								<div class="controls">
									<select class="form-control" name="category" id="category">
										<option value="1">Administrator Settings</option>
										<option value="2">Customer Support</option>
										<option value="3">Contract and Policy</option>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label>Title</label>
								<div class="controls">
									<input type="text" name="title" id="title" class="form-control" placeholder="Enter Title">
								</div>
							</div>
							<div class="control-group">
								<label>Description</label>
								<div class="controls">
									<textarea name="content" id="content" class="form-control" rows="4" style="resize:none;"></textarea>
								</div>
							</div>
						</form>
						</br>
						<div class="row">
							<div class="col-md-5">
								
									<div id="faq-upload">
										<form id="myDropZone2" class="dropzone faq" method="POST">
											<input type="hidden" id="id_faq" name="id_faq" value="" />
											<input type="hidden" name="new_path" value="faq" />
											
										</form>
										</br>
										<a href="javascript:void(0)" class="btn btn-default cancel-faq-image hidden">Cancel</a>
									</div>

									<div id="faq-image" class="hidden">
										<div id="faq_bg_image"><img class="img-responsive" alt="" ></div>
										</br>
										<a href="javascript:void(0)" class="btn btn-danger change-faq-image">Change Image</a>
									</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
                <div class="form-actions">
                    <div class="pull-right">
                        <a class="btn btn-danger" data-dismiss="modal" ><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
						<a href="javascript:void(0)" class="btn btn-success btn-save-faq"><i class="glyphicon glyphicon-floppy-save"></i>Save</a>
                    </div>
                </div>
            </div>
		</div>

	</div>
</div>