<div id="addTestimony" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content width-100">
            <div class="modal-header modal-design">
                <h4 class="modal-title">Add Testimony</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form form-vertical" id="form-testimony-home">
                            <input type="hidden" name="id_testimony" id="id_testimony"/>
                            <div class="control-group">
                                <label>Title</label>
                                <div class="controls">
                                    <input type="text" name="title" id="title" class="form-control" placeholder="Enter Title">
                                </div>
                            </div>
                            <div class="control-group">
                                <label>Content</label>
                                <div class="controls">
                                    <textarea name="content" id="content" class="form-control" rows="4" style="resize:none;"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label>Client Name</label>
                                <div class="controls">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Enter Title">
                                </div>
                            </div>
                            <div class="control-group">
                                <label>Company</label>
                                <div class="controls">
                                    <input type="text" name="company" id="company" class="form-control" placeholder="Enter Company">
                                </div>
                            </div>
                            <div class="control-group">
                                <label>Rating</label>
                            </div>
							<div class="rating">
								<label>
									<input type="radio" name="rating" id="rating_1" value="1" />
									<span class="icon">★</span>
								</label>
								<label>
									<input type="radio" name="rating" id="rating_2" value="2" />
									<span class="icon">★</span>
									<span class="icon">★</span>
								</label>
								<label>
									<input type="radio" name="rating" id="rating_3" value="3" />
									<span class="icon">★</span>
									<span class="icon">★</span>
									<span class="icon">★</span>   
								</label>
								<label>
									<input type="radio" name="rating" id="rating_4" value="4" />
									<span class="icon">★</span>
									<span class="icon">★</span>
									<span class="icon">★</span>
									<span class="icon">★</span>
								</label>
								<label>
									<input type="radio" name="rating" id="rating_5" value="5" />
									<span class="icon">★</span>
									<span class="icon">★</span>
									<span class="icon">★</span>
									<span class="icon">★</span>
									<span class="icon">★</span>
								</label>
							</div>
						</form>
                        
                        <br>
                        <form class="form form-vertical" id="form-testimony-home">
                            <div class="control-group">
                                <label>Upload Client Photo</label>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-5">

                                <div id="home-testimony-upload">
                                    <form id="myDropZone5" class="dropzone testimony-home" method="POST">
                                        <input type="hidden" id="id_testimony" name="id_testimony" value="" />
                                        <input type="hidden" name="new_path" value="client" />

                                    </form>
                                    </br>
                                    <a href="javascript:void(0)" class="btn btn-danger cancel-home-testimony-image hidden">Cancel Change Photo</a>
                                </div>

                                <div id="home-testimony-image" class="hidden">
                                    <div id="client_image"><img class="img-responsive" alt="" ></div>
                                    </br>
                                    <a href="javascript:void(0)" class="btn btn-info change-home-testimony-image">Change Client Picture</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
					<div class="pull-right">
						<a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal" ><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
						<a href="javascript:void(0)" class="btn btn-success btn-save-testimony"><i class="glyphicon glyphicon-floppy-save"></i> Save</a>
					</div>
				</div>
            </div>
        </div>

    </div>
</div>