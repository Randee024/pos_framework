<div id="successTestimonyMessage" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content width-100">
      <div class="modal-header alert-success">
        <h4 class="modal-title">Congratulations!</h4>
      </div>
      <div class="modal-body">
		<p><span id="message_testimony">You have successfully added a new testimony.</span></p>
      </div>
      <div class="modal-footer">
        <a href="/edit_home" class="btn btn-primary"><i class="glyphicon glyphicon-ok-sign"></i>Ok</a>
      </div>
    </div>

  </div>
</div>