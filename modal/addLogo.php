<div id="addLogo" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content width-100">
			<div class="modal-header modal-design">
				<h4 class="modal-title">Add Partner Logo</h4>
			</div>
			<div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="partner-logo-upload">
                            <form id="myDropZone4" class="dropzone about-us-logo" method="POST">
                                <input type="hidden" name="new_path" value="partnerlogo" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal-footer">
                <div class="form-actions">
                    <div class="pull-right">
                        <a class="btn btn-danger" data-dismiss="modal" ><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
						<a href="javascript:void(0)" class="btn btn-success btn-save-logo"><i class="glyphicon glyphicon-floppy-save"></i>Save</a>
                    </div>
                </div>
            </div>
		</div>

	</div>
</div>