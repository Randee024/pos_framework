<div id="logout" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content width-100">
			<div class="modal-header alert-info">
				<h4 class="modal-title">Information</h4>
			</div>
			<div class="modal-body">
				 <div class="row">
					<div class="col-md-12">
						
						<div class="row">
							<div class="col-md-7">
								<p>
									<span>Are you sure you want to logout?</span>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="/logOut" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i>Ok</a>
				<a href="javascript:void(0)" class="btn btn-default"  data-dismiss="modal" ><i class="glyphicon glyphicon-remove-sign"></i>Cancel</a>
			</div>
		</div>

	</div>
</div>