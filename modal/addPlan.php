<div id="addPlan" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content width-100">
            <div class="modal-header modal-design">
                <h4 class="modal-title">Add Plan</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="add-plan-form">
                            <input type="hidden" id="id_plan" name="id_plan"/>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input class="form-control" type="text" id="title" name="title" placeholder="Title" required />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" id="description" name="description" placeholder="Description" required rows="4" style="resize:none;">Lorem ipsum</textarea>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Price</label>
                                        <input class="form-control numOnly" type="number" min="0" id="pricing" name="pricing" placeholder="Price" required />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="pull-right">
                        <a class="btn btn-danger" data-dismiss="modal" ><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                        <a href="javascript:void(0)" class="btn btn-success btn-save-plan"><i class="glyphicon glyphicon-floppy-save"></i> Save</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>