<div id="successAboutUsMessage" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content width-100">
      <div class="modal-header alert-success">
        <h4 class="modal-title">Congratulations!</h4>
      </div>
      <div class="modal-body">
		<p><span id="message_about_us">You have successfully updated about us page.</span></p>
      </div>
      <div class="modal-footer">
        <a href="/edit_about_us" class="btn btn-primary"><i class="glyphicon glyphicon-ok-sign"></i>Ok</a>
      </div>
    </div>

  </div>
</div>