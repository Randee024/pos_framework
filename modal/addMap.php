<div id="addMap" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content width-100">
			<div class="modal-header modal-design">
				<h4 class="modal-title">Add Map</h4>
			</div>
			<div class="modal-body">
				 <div class="row">
					<div class="col-md-12">
						<form class="form form-vertical" id="form-map">
							<input type="hidden" name="id_map" id="id_map"/>
							<div class="control-group">
								<label>Latitude</label>
								<div class="controls">
									<input type="text" name="latitude" id="latitude" class="form-control numOnly" placeholder="Enter Latitude">
								</div>
							</div>
							<div class="control-group">
								<label>Longitude</label>
								<div class="controls">
									<input type="text" name="longitude" id="longitude" class="form-control numOnly" placeholder="Enter Longitude">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
                <div class="form-actions">
                    <div class="pull-right">
                        <a class="btn btn-danger" data-dismiss="modal" ><i class="glyphicon glyphicon-remove-sign"></i> Cancel</a>
                        <a href="javascript:void(0)" class="btn btn-success btn-save-map"><i class="glyphicon glyphicon-floppy-save"></i>Save</a>
                    </div>
                </div>
            </div>
		</div>

	</div>
</div>